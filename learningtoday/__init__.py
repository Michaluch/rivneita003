from pyramid.config import Configurator
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from src.views.securityview import (
    RootFactory,
    CourseFactory,
    TestFactory,
    rolefinder,
)

from learningtoday.loggingconfig import logger


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    authentication_policy = AuthTktAuthenticationPolicy('learn2day',
                                                        hashalg='sha512',
                                                        reissue_time=12000,
                                                        timeout=120000,
                                                        include_ip=True,
                                                        callback=rolefinder,
                                                        )
    authorization_policy = ACLAuthorizationPolicy()
    config = Configurator(authentication_policy=authentication_policy,
                          authorization_policy=authorization_policy,
                          settings=settings,
                          root_factory=RootFactory,
                          )
    config.include('pyramid_chameleon')
    config.include('pyramid_jinja2')
    config.add_jinja2_search_path("learningtoday:src/templates/jinja2")
    config.add_renderer('.html', 'pyramid_jinja2.renderer_factory')

    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')
    config.add_route('signup', '/signup')

    #Course colors management
    config.add_route('category_color', '/category_color')

    # admin panel routes
    config.add_route('admin_panel', '/admin')
    config.add_route('add_user', '/add/user')
    config.add_route('add_user_post', '/add_user_post')
    config.add_route('manage_users', '/manage_users')
    #Edit
    config.add_route('edit_user', '/edit_user')
    config.add_route('edit_user_post', '/edit_user_post')
    #Search
    '''config.add_route('search_user', '/search_user')'''
    config.add_route('change_user_status', '/change_user_status')
    #Courses
    config.add_route('manage_courses', '/manage_courses')
    config.add_route('upload_info_courses', '/upload_info_courses')

    #Category
    config.add_route('manage_categories', '/manage_categories')
    config.add_route('category_courses', '/category/{id}')
    config.add_route('home_search', '/search/{phrase}')

    #Library
    config.add_route('manage_lectures', '/lectures')
    
    # library routes
    config.add_route('library', '/library')
    #Add
    config.add_route('add_book', '/add/book')
    config.add_route('add_book_post', '/add/book/')
    #Edit
    config.add_route('edit_book', '/edit/book/{id}')
    config.add_route('edit_book_post', '/edit/book')
    #Search
    config.add_route('book', 'book')
    #Category
    config.add_route('add_category_library_post', '/add/category_library/')
    config.add_route('add_author', '/add/author/')

    # tests routes
    config.add_route('tests', '/tests', factory=TestFactory)
    config.add_route('test', '/tests/{lecture_id}', factory=TestFactory, traverse='/{lecture_id}')

    config.add_route('course', '/course/{id}')
    config.add_route('courses', '/courses')
    config.add_route('about', '/about')
    config.add_route('terms', '/terms')

    #Profile page
    config.add_route('profile', '/profile')
    config.add_route('upload_info_profile', '/upload_info_profile')
    config.add_route('download_info', '/download_info')
    config.add_route('feedbacks', '/feedbacks')
    config.add_route('subscribe_news', '/subscribe_news')
    #test routs
    #subscribe route
    config.add_route('score_progress', '/score_progress')
    # manage recommendations
    config.add_route('recommendations', '/recommendations')

    try:
        config.scan()
    except Exception as e:
        logger.critical('can not scan project while initing: %s' % (e,))
    try:
        configuration = config.make_wsgi_app()
        return configuration
    except Exception as e:
        logger.critical('can not make WSGI app: %s', (e,))

