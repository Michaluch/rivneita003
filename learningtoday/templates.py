from pyramid.events import subscriber
from pyramid.events import BeforeRender
from pyramid.security import has_permission
from pyramid.security import authenticated_userid
from learningtoday.src.models import CurRole, User


@subscriber(BeforeRender)
def add_global(event):
    event['utils'] = TemplateUtils(event['request'])


class TemplateUtils(object):
    def __init__(self, request):
        self.request = request

    def has_permission(self, permission):
        return has_permission(permission, self.request.context, self.request)

    def authenticated_userid(self):
        return authenticated_userid(self.request)

    def role_is_teacher(self):
        login = User().get_one(login=authenticated_userid(self.request))
        return CurRole().get_one(id=login['role_id'])['name'] == 'teacher'

    def role_is_admin(self):
        login = User().get_one(login=authenticated_userid(self.request))
        return CurRole().get_one(id=login['role_id'])['name'] == 'admin'