# -*- coding: utf-8 -*-

# The Test Data Access Object handles all interactions with the Test collection.
from learningtoday.src.models import mongoorm
import pymongo


class TestDAO:
    
    def __init__(self, database):
        self.db = database
        self.test = self.db.quizes
        
    
    def add_test(self, idcollection, final, quiz):
        """
        creates a new test in the tests collection
        Parameters:
            idcollection - number of lecture (int)
            final - flag for indicate final exam (bool)
            quiz - list of quize (list). Neeb be next format:

                [{"name":"Question 1?",
                  "answers": [{"answer1":"This answer false", "right":"false"},
                              {"answer2":"This answer false", "right":"false"},
                              {"answer2":"This answer true", "right":"true"}]
                 },
                ]
        """
        test = {"lecture_number": idcollection,
                "questions":quiz}
        
        if final != "":
            test["final"] = final
        else:
            test["final"] = False
    
        try:
            self.test.insert(test, safe=True)
        except pymongo.errors.OperationFailure:
            print "oops, mongo error"
            return False
        except pymongo.errors.DuplicateKeyError as e:
            print "oops, test is already taken"
            return False

        return True
        
    def get_test_by_lecture(self, lectureid):
        """ 
            Returns test record or None
        """            
        test = None            
        try:
            test = self.test.find_one({"lecture_number":lectureid})
        except:
            print "Unable to query database for test"

        if test is None:
            print "Test not in database"
            return None
        
        return test
        
if __name__ == "__main__":
    db = mongoorm.connect_to_db("tests")
    newins = TestDAO(db)
         
    tests = [{
             "name":"Question 1?",
             "answers": [{"answer1":"This answer false",
                        "right":"false"},
                        {"answer2":"This answer false",
                        "right":"false"},
                        {"answer2":"This answer true",
                        "right":"true"}]},
             {"name":"Question 2?",
             "answers":[{"answer1":"This answer false",
                        "right":"false"},
                        {"answer2":"This answer false",
                        "right":"false"},
                        {"answer2":"This answer true",
                        "right":"true"}]},
             {"name":"Question 1?",
             "answers":[{"answer1":"This answer false",
                        "right":"false"},
                        {"answer2":"This answer false",
                        "right":"false"},
                        {"answer2":"This answer true",
                        "right":"true"}]}]
    #newins.add_test(1, True, tests)
    newins.get_test_by_lecture(3)