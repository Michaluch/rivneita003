/*
* Just for pages, where inputs checking without "tips" field
* (for example in profile page, when user editing)
*
* by Dima Karacheban
*/
var updateTips = function( title, text ) {
    if(!title) {
        title = "";
    }
    nice_alert(title, text);
}
var checkLength = function( o, n, min, max ) {
    if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( n, "Length of " + n + " must be between " +
            min + " and " + max + "." );
        return false;
    } 
    else { return true; }
}
var checkRegexp = function( o, regexp, n ) {
    if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
    } else { return true; }
}
