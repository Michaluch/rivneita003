$(document).ready(function(){
    var course = $("div.course").attr("id");
    var flag = 0;
    $(".tab[title='feedbacks']").click(function() {
        if (flag == 0) {
            $.ajax({
                    type: "GET",
                    url: "/course/" + course,
                    data: {action: 'course_comments'},
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        // console.log(data);
                        $('#comments_wrapper').append(data)},
                    error: function() {}
                })
            flag++;
        }
    });

    var flag_test = 0;

    $(document).on("click", "#lecture_test", function() {
        var $this = $(this)
        var lecture_id = $(this).closest("div.content").attr("id");
        var course = $("div.course").attr("id");
        if (flag == 0) {

            var c = confirm("do you want to pass now?");
                if (c == true)
                    {
                    $("#lecture_body_" + lecture_id).empty();
                    $("#test_" + lecture_id).removeClass( "display_none" );
                        $.ajax({
                                type: "GET",
                                url: "/course/" + course,
                                data: {'action': 'lecture_test',
                                    'final_id': lecture_id.split("_")[1],
                                    'lecture_id': lecture_id.split("_")[1]},
                                cache: false,
                                success: function(data, textStatus, jqXHR) {

                                    var test = data.test;
                                    var description = test['description'];
                                    var questions = test['questions'];
                                    // alert(test['questions'][0]);
                                    console.log(data);
                                    $('#test_' + lecture_id).append('<p>' + description + '</p>');
                                    for (var i = 0; i < questions.length; i++) {
                                        $('#test_' + lecture_id).append('<form class="test_question" id="test_question_' + i + '"><p>' + (-(-i)+1));
                                        var q = questions[i]['question'];
                                        $('#test_question_' + i).append('<p class="question_title">' + q + '</p>');
                                        
                                        for (var j = 0; j < questions[i]['answers'].length; j++) {
                                            var a = questions[i]['answers'][j]['answer'];
                                            $('#test_question_' + i).append('<input type="checkbox" value="' + a + '">' + a + '<br>' );
                                            }
                                        $('#test_' + lecture_id).append('</p></form>');
                                        }
                                    $('#test_' + lecture_id).append('<a class="ButtonAdmin" id="submit_test">Submit</a>');
                                    },
                                error: function() {}
                            })
                        flag++;
                        $this.closest("div.navi").empty();
                        }
                    }
        else {alert( "you must finish opened test first" )
            }
    });

    $(document).on("click", "#pass_final", function() {
        $this = $(this);
        var parent_content_div = $this.closest('div.content');
        var lecture_id = parent_content_div.prev("div.content").attr("id").split("_")[1];
        var course = $("div.course").attr("id");
        if (flag == 0) {
            $(".final_test").empty();
                $.ajax({
                        type: "GET",
                        url: "/course/" + course,
                        data: {'action': 'lecture_test',
                            'lecture_id': lecture_id,
                            'final_id': lecture_id},
                        cache: false,
                        success: function(data, textStatus, jqXHR) {

                            var test = data.final_test;
                            $('.final_test').attr('id', 'final_' + test['lecture_id']);
                            var description = test['description'];
                            var questions = test['questions'];
                            // alert(test['questions'][0]);
                            console.log(data);
                            $('.final_test').append('<div class="lecture_title">' + test['name'] + '</div>');
                            $('.final_test').append('<p>' + description + '</p>');
                            for (var i = 0; i < questions.length; i++) {
                                $('.final_test').append('<form class="test_question" id="test_question_' + i + '"><p>' + (-(-i)+1));
                                var q = questions[i]['question'];
                                $('#test_question_' + i).append('<p class="question_title">' + q + '</p>');
                                
                                for (var j = 0; j < questions[i]['answers'].length; j++) {
                                    var a = questions[i]['answers'][j]['answer'];
                                    $('#test_question_' + i).append('<input type="checkbox" value="' + a + '">' + a + '<br>' );
                                    }
                                $('.final_test').append('</p></form>');
                                }
                            $('.final_test').append('<a class="ButtonAdmin" id="submit_test">Submit</a>');
                            },
                        error: function() {}
                    })
                flag++;   
            }
        else {alert( "you must finish opened test first" )
            }
    });

    $(document).on("click", "#feedback_link", function() {
        $(".tab[title='feedbacks']").click()
    });

    $(document).on("click", "#test_link", function() {
        $(".tab[title='final_test']").click()
    });

    $(document).on("click", "#first_lecture", function() {
        $(".tab[title='description']").click()
    });

    $(document).on("click", "#final_lecture", function() {
        $(".tab[title='final_test']").click()
    });

    $(document).on("click", "#previous_lecture", function() {
        var lecture_id = $(this).closest("div.content").attr("id").split("_")[1]
        var prev = -(-lecture_id) - 1
        $(".tab[title='lecture_" + prev + "']").click()
    });

    $(document).on("click", "#next_lecture", function() {
        var lecture_id = $(this).closest("div.content").attr("id").split("_")[1]
        var next = -(-lecture_id) + 1
        $(".tab[title='lecture_" + next + "']").click()
    });

    $(document).on("click", "#submit_comment", function() {

        var tips = $("#tip");

        function updateTips( t ) {
        tips.text( t ).addClass( "ui-state-highlight" );
        setTimeout(function() {
            tips.removeClass( "ui-state-highlight", 1500 );
        }, 500 );
        }
     
        function checkLength( o, n, min, max ) {
            if ( o.val().length > max || o.val().length < min ) {
                o.addClass( "ui-state-error" );
                updateTips( "Length of " + n + " must be between " +
                  min + " and " + max + "." );
                return false;
            } 
            else { return true; }
        }
        function checkRegexp( o, regexp, n ) {
            if ( !( regexp.test( o.val() ) ) ) {
              o.addClass( "ui-state-error" );
              updateTips( n );
              return false;
            } else { return true; }
        }
            var bValid = true;

        var title = $('#title');
        var body = $('#body');  
        // alert(title + body);
            $("#tip").empty().removeClass('ui-state-error');

            bValid = bValid && checkLength( title, 'title', 4, 70 );
            bValid = bValid && checkRegexp( title, /^[a-z]([0-9a-z_])+$/i,
                'Title may consist of a-z, 0-9, underscores, begin with a letter.' );

            bValid = bValid && checkLength( body, 'body', 10, 400 );
            bValid = bValid && checkRegexp( body, /^[a-z]([0-9a-z_])+$/i,
                'Comment may consist of a-z, 0-9, underscores, begin with a letter.' );

        if(bValid) {
            $.ajax({
                type:'POST',
                url: '/feedbacks',
                data: { 'title': title.val().trim(),
                        'body': body.val().trim(),
                        'lecture_id': $('#selectedLecture').find(":selected").val(),
                        'action': 'add'
                        },
                cache: false,
                success: function(data, textStatus, jqXHR) {
                    $("#tip").empty().removeClass( "ui-state-highlight" );
                    $('#comments_wrapper').empty();
                        $.ajax({
                            type: "GET",
                            url: "/course/" + course,
                            data: {action: 'course_comments'},
                            cache: false,
                            success: function(data, textStatus, jqXHR) {
                                // console.log(data);
                                $('#comments_wrapper').append(data)},
                            error: function() {}
                        })
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Server error');
                }
            });
        }
    });
    
    var nice_alert_reload = function(title, mess) {
    //Create form
        $("body").append("<div id='alert_dialog' title=" + title + ">" +
            "<p><span style='float: left; margin: 0 7px 50px 0;'></span>" +
                mess + "</p>" +
            "</div>")

        $( "#alert_dialog" ).dialog({
            resizable: false,
            modal: true,
            show: {
                effect: "fade",
                duration: 300
            },
            hide: {
                effect: "fade",
                duration: 300
            },
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                    $("#alert_dialog").remove();
                    window.location.reload();
                }
            }
        });
    }

    var user_result = '';

    $(document).on('click', '#submit_test', function(event) {
        $this = $(this);
        var test_div = $this.closest('.content');
        var lecture_id = test_div.attr('id').split('_')[1];
        var test_answers = {'lecture_id': lecture_id};
        var questions = [];
        test_div.find('.test_question').each(function(i) {
            $this = $(this)
            var question = {'question': $this.find('.question_title').html()};
            var answers = [];
            $this.find('input').each(function(j) {
                $this = $(this);
                answers.push({'answer': $this.val(), 'correct': $this.prop('checked')});
            });
            question['answers'] = answers;
            questions.push(question);
        });
        test_answers['questions'] = questions;
        $.ajax({
                  url: '/score_progress',
                  type: 'PUT',
                  dataType: 'json',
                  data: {'action': 'complete_test', 'test_answers': JSON.stringify(test_answers)},
              })
              .done(function(json) {
                  if (json.result == 'success') {
                    user_result += 'Grade for test: ' + json.grade + '<br/>';
                    if (test_div.attr('id').split('_')[0] == 'final') {
                        show_course_score(1);
                    } else {
                        show_course_score(0);
                    }
                  }
                  else if (json.result == 'error') {
                    nice_alert_reload('error', json.error);
                    //flag = 0;
                  }
              })
              .fail(function() {
                  nice_alert_reload('!', 'fail');
                  //flag = 0;
              });
                    
    });

    function show_course_score(finished){
        var course_id = $(document).find('.course').attr('id');
        $.ajax({
            url: '/score_progress',
            type: 'GET',
            dataType: 'json',
            data: {
                   'action': 'last_score_result', 
                   'finished': finished,
                   'course_id': course_id
                  },
        })
        .done(function(json) {
            if (json.result == 'success') {
                if (!finished) { 
                    user_result += 'Current course score: ' + json.score_result + '<br/>';
                    //flag = 0;
                    nice_alert_reload('User results:', user_result);
                    //window.location.reload();
                } else {
                    user_result += 'Final course score: ' + json.score_result + '<br/>';
                    show_recomendations(course_id, json.score_result);
                }
            }else if (json.result == 'error') {
                nice_alert_reload('error', json.error);
                //flag = 0;
            }
        })
        .fail(function() {
            nice_alert_reload('!', 'fail');
            //flag = 0;
        });
        
    }

    function show_recomendations(course_id, score) {
        if (score < 100) {
            $.ajax({
                url: '/recommendations',
                type: 'GET',
                dataType: 'json',
                data:  {
                        'action': 'get_user_recommendations',
                        'course_id': course_id,
                        'grade': score },
            })
            .done(function(json) {
                if (json.result == 'success') {
                    user_result += 'Books to read:<br/>';
                    for (i=0; i<json.books_list.length; i++) {
                        var book = json.books_list[i];
                        user_result +=  '&nbsp;&nbsp;&nbsp;&nbsp;' + book[1] + ' (' + book[2] + ')<br/>';
                    }
                    //flag = 0;
                    nice_alert_reload('User results:', user_result);
                    //window.location.reload();
                }else if (json.result == 'error') {
                    //flag = 0;
                    nice_alert_reload('error', json.error);
                }
            })
            .fail(function() {
                nice_alert_reload('!', 'fail');
                //flag = 0;
            });
        } else if (score == 100) {
            nice_alert_reload('Perfect!', 'You are perfect! No books for you!');
            //flag = 0;
            //window.location.reload();
        }
        
    }

});
