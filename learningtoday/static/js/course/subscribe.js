$(document).ready(function() {

    $(document).on('click', '#subscribe', function(event) {
        $('#subscribe').remove();
        $('.right').append('<p id="remove_me" class="subscribed">............</p>');
        var course_id = $('.course').attr('id');
        $.ajax({
            url: '/score_progress',
            type: 'POST',
            dataType: 'json',
            data: {action: 'subscribe', course_id: course_id},
        })
        .done(function(json) {
            if (json.result == 'error') {
                nice_alert('error', json.error);
            }
            if (json.result == 'success') {
                $('#remove_me').remove();
                $('.right').append('<p class="subscribed"> You are subscribed for this course</p>');
            }
        })
        .fail(function() {
            nice_alert('!', 'fail');
        });
        
    });

   
});