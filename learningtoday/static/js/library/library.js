$(function book_status_change() {
    $(document).on('click', '.book_status_change', function () {
            var book_id = $(this).closest('.book_row').find("td[name=id]");
            var book_status = $(this).closest('.book_row').find("td[name=deleted]");
            var book_status_button = $(this).closest(".book_status_change");
            $.ajax({
                type: "PUT",
                url: "/book",
                data: { id: book_id.html(), action: "change_status" },
                cache: false,
                success: function (data, textStatus, jqXHR) {
                    if (data.status_change) {
                        if (book_status.html() == 'no') {
                            book_status.html('yes');
                            book_status_button.html('Activate');
                        }
                        else if (book_status.html() == 'yes') {
                            book_status.html('no');
                            book_status_button.html('Deactivate');
                        }
                    } else {
                        alert('Error occurred, please fix it ASAP!!1');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('ERROR');
                }
            });
        }
    );
});

$(document).on('click', '#add-book', function () {

    $.ajax({
        type: "GET",
        url: "/library",
        data: {
            action: 'add_book'
        },
        success: function (data) {
            $('#book-wrapper').remove()
            $('#books').append($('<div id="book-wrapper" />').append(data));

        },
        error: function () {
            alert('ERROR');
        }
    });
});


$(document).on('click', '#add-book-submit', function () {
    var category_id = $('#add-book-category').find("option:selected").val();
    var author_id = $('#add-book-author').find("option:selected").val();
    var title = $('#add-book-title').val();
    var amount = $('#add-book-amount').val();

    $.ajax({
        type: "POST",
        url: "/library",
        data: {
            category_id: category_id,
            author_id: author_id,
            title: title,
            amount: amount,
            action: 'add_book_submit'
        },
        success: function (data) {
            if (data.status == true) {
                $("#message").find("span").text("Book was successfully added")
                $("#message").fadeIn("slow");
                setTimeout(function () {
                    $("#message").fadeOut("slow");
                }, 5000);
                $("#message").find("span").text();


                $('#add-book-title').val("")
                $('#add-book-amount').val("")
            }


        },
        error: function () {
            alert('ERROR');
        }
    });
});


$(document).on('click', '#add-author', function () {
    $.ajax({
        type: "GET",
        url: "/library",
        data: {
            action: 'add_author'
        },
        success: function (data) {
            $('#book-wrapper').remove()

            $('#books').append($('<div id="book-wrapper" />').append(data));

        },
        error: function () {
            alert('ERROR');
        }
    });
});

$(document).on('click', '#add-author-submit', function () {
    var name = $('#add-author-name').val();

    $.ajax({
        type: "POST",
        url: "/library",
        data: {
            name: name,
            action: 'add_author_submit'
        },
        success: function (data) {
            if (data.status == true) {
                $("#message").find("span").text("Author was successfully added")
                $("#message").fadeIn("slow");
                setTimeout(function () {
                    $("#message").fadeOut("slow");
                }, 5000);
                $("#message").find("span").text();


                $('#add-author-name').val("")

            }


        },
        error: function () {
            alert('ERROR');
        }
    });
});


$(document).on('click', '#add-category', function () {
    $.ajax({
        type: "GET",
        url: "/library",
        data: {
            action: 'add_category'
        },
        success: function (data) {
            $('#book-wrapper').remove()

            $('#books').append($('<div id="book-wrapper" />').append(data));

        },
        error: function () {
            alert('ERROR');
        }
    });
});

$(document).on('click', '#add-category-submit', function () {
    var name = $('#add-category-name').val();
    var active = $('#add-category-active').val();

    $.ajax({
        type: "POST",
        url: "/library",
        data: {
            name: name,
            action: 'add_category_submit'
        },
        success: function (data) {
            if (data.status == true) {
                $("#message").find("span").text("Category was successfully added")
                $("#message").fadeIn("slow");
                setTimeout(function () {
                    $("#message").fadeOut("slow");
                }, 5000);
                $("#message").find("span").text();


                $('#add-category-name').val("")

            }


        },
        error: function () {
            alert('ERROR');
        }
    });
});

$(document).on('click', '#search-book-submit', function () {
    var search_key = $('#search-book-key').find("option:selected").val();
    var search_value = $('#search-book-value').val()
    $.ajax({
        type: "GET",
        url: "/library",
        data: {
            search_key: search_key,
            search_value: search_value,
            action: 'search_book'
        },
        success: function (data) {

            $("#message").find("span").text("Search was successful")
            $("#message").fadeIn("slow");
            setTimeout(function () {
                $("#message").fadeOut("slow");
            }, 5000);
            $("#message").find("span").text();

            $('#book-wrapper').remove()

            $('#books').append($('<div id="book-wrapper" />').append(data));

            $('#book-wrapper').find($('.book-container')).each(function () {

                if ($(this).find($('.book-list .book')).length == 0) {
                    $(this.remove())
                }
            });

            $('#search-book-value').val("")


        },
        error: function () {
            alert('ERROR');
        }
    });
});


$(document).ready(function () {
            $('#book-wrapper').find($('.book-container')).each(function () {
                if ($(this).find($('.book-list .book')).length == 0) {
                    $(this.remove())
                }
            });
});


$(document).on('click', '#category ul li', function () {
//    console.log($(this).text());
    var search_key = 'category_id';
    var search_value = $(this).text();

    var new_active = $(this);
//    alert(new_active.html());


    $.ajax({
        type: "GET",
        url: "/library",
        data: {
            search_key: search_key,
            search_value: search_value,
            action: 'search_book'
        },
        success: function (data) {

            var old_active = $('#category').find('#category-active');
            old_active.removeAttr('id');
//            $(this).setAttribute('id', '#category-active');
//            alert($(this).text());
            new_active.attr('id', 'category-active');

            $("#message").find("span").text("Category filtering was successful")
            $("#message").fadeIn("slow");
            setTimeout(function () {
                $("#message").fadeOut("slow");
            }, 5000);
            $("#message").find("span").text();

            $('#book-wrapper').remove()
            $('#books').append($('<div id="book-wrapper" />').append(data));
            $('#book-wrapper').find($('.book-container')).each(function () {
                if ($(this).find($('.book-list .book')).length == 0) {
                    $(this.remove())
                }
            });
            $('#search-book-value').val("")


        },
        error: function () {
            alert('ERROR');
        }
    });
});

$(document).on('click', '#edit-books', function () {
    $.ajax({
        type: "GET",
        url: "/library",
        data: {
            action: 'book_edit_show'
        },
        success: function (data) {

//            $("#message").find("span").text("Search was successful")
//            $("#message").fadeIn("slow");
//            setTimeout(function () {
//                $("#message").fadeOut("slow");
//            }, 5000);
//            $("#message").find("span").text();

            $('#book-wrapper').remove()

            $('#books').append($('<div id="book-wrapper" />').append(data));

//            $('#book-wrapper').find($('.book-container')).each(function () {
//
//                if ($(this).find($('.book-list .book')).length == 0) {
//                    $(this.remove())
//                }
//            });
//
//            $('#search-book-value').val("")


        },
        error: function () {
            alert('ERROR');
        }
    });
});