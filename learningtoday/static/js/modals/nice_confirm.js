var nice_confirm = function(mess, title) {
    if(!title) {
        title = '';
    }

    $("body").append("<div id='confirm_dialog' title=" + title + ">" +
        "<p><span style='float: left; margin: 0 7px 50px 0;'></span>" +
            mess + "</p>" +
        "</div>")

    var out;
    $( "#confirm_dialog" ).dialog({
        resizable: false,
        width: '20%',
        height: 300,
        modal: true,
        show: {
            effect: "fade",
            duration: 300
        },
        hide: {
            effect: "fade",
            duration: 300
        },
        close: function() {
            $("#confirm_dialog").remove();
        },
        buttons: {
        "Ok": function() {
            $( this ).dialog( "close" );
            out = true;
        },
        Cancel: function() {
            $( this ).dialog( "close" );
            out = false;
        }
    });
    return out;
}