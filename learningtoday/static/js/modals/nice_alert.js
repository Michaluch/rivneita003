/*
* This message box looks like alert, but little bit nicer
* Just include to project and write nice_alert("title", "message") instead of alert
* Used JQuery-UI modal library
*
* by Dima Karacheban
*/
var nice_alert = function(title, mess) {
	//Create form
	$("body").append("<div id='alert_dialog' title=" + title + ">" +
		"<p><span style='float: left; margin: 0 7px 50px 0;'></span>" +
			mess + "</p>" +
		"</div>")

    $( "#alert_dialog" ).dialog({
    	resizable: false,
		modal: true,
		show: {
	        effect: "fade",
	        duration: 300
		},
		hide: {
			effect: "fade",
			duration: 300
		},
        close: function() {
            $("#confirm_dialog").remove();
        },
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
			}
		}
    });
}  //End of function