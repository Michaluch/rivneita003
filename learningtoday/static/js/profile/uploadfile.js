$(document).ready( function() {

    var file = $('#file1');
    var upload = $('#_upload');
    var cancel = $('#_cancel');
    var picture = $('#pic');
    var slider = $('.slider_img');
    var slider_wrap = $('.slider_wrap');
    var wrapper = $('#owner_wrap');
    var pop = $('.pop');
    var body = $(document.body);
    var course_photo = $('.avatars');
    var img = $('.img');
    var img2 = $('.img2');
    var time = null;
    var num_class = null;
    var las_id = null;
    var id = null;
    var formData = null;
    var pic = $('.pic_file');

    pop.addClass('hidden');
    picture.addClass('params');
    img2.addClass('hidden');

    wrapper.mouseenter(function(){
           time = setTimeout(function(){
                slider.stop(false,false).animate({'margin-top':'0px'},500);
                slider.stop(false,false).animate({'opacity':.70},500);
           },300);
    });

    wrapper.mouseleave(function(){
       clearTimeout(time);
       slider.stop(false,false).animate({'margin-top':'40px'},600);
       slider.stop(false,false).animate({'opacity':0},600);
     });

    pop.mouseover(function(){
        if (file.val() == ''){
            img.removeClass('hidden');
        }
        else{
            img.addClass('hidden');
            img2.removeClass('hidden')
            $('.wrap2_text').text('Upload ' + file.val().replace('C:\\fakepath\\','') + '?');
        }
    });

    cancel.click(function(){
        file.val('');
        img.removeClass('hidden');
        img2.addClass('hidden');
    });

    slider.click(function(){
        file.val('');
        img2.addClass('hidden');
        img.removeClass('hidden');
        pop.removeClass('hidden');
        body.css({'overflow':'hidden'});
    });

    pop.click(function(event){
       var target = $(event.target);
       if (target.is((pop))){
           file.val('');
           pop.addClass('hidden');
           img.addClass('hidden');
           img2.addClass('hidden');
           body.css({'overflow':'auto'});
       }
    });

    upload.mouseenter(function(){
        upload.css('box-shadow', '0 0 15px 8px skyblue');
    });
    upload.mouseleave(function(){
        upload.css('box-shadow', '');
    });

    cancel.mouseenter(function(){
        cancel.css('box-shadow', '0 0 15px 3px #C53230');
    });
    cancel.mouseleave(function(){
        cancel.css('box-shadow', '');
    });

    pic.mouseenter(function(){
        pic.css('box-shadow', '0 0 25px 3px #1b61d6');
    });
    pic.mouseleave(function(){
        pic.css('box-shadow', '');
    });

    pic.mousedown(function(){
        pic.css('box-shadow', '0 0 15px 5px green');
    });
    pic.mouseup(function(){
        pic.css('box-shadow', '');
    });
    $('.wrap2_text').text('Upload ' + file.val() + '?');

    upload.click(function(){
           img.addClass('hidden');
           img2.addClass('hidden')
        var formData = new FormData()
            body.css({'overflow':'auto'});
            pop.addClass('hidden');
            formData.append('some', $('#file1')[0].files[0]);
            var file_in = $('#file1')[0].files[0]
            var type = file_in.type;
            if ( (type=='image/jpg') || (type=='image/jpeg') ){
//                alert(type);
                $.ajax({
                    type:'POST',
                    url:'/upload_info_profile',
                    data: formData,
                    cache:false,
                    contentType:false,
                    processData:false,
                    beforeSend: function(data, textStatus, jqXHR){

                            $('#pic').attr('src','/static/img/user_img/load3.gif');

                    },
                    success: function(data, textStatus, jqXHR) {
//                     alert(data.filename.length);
                        if (data.check == 1){
                            setTimeout(function(){$('#pic').attr('src','/static/img/user_img/' + data.filename)},2000)
                        }
                        else{
                             if( (data.current_name == '') || (data.current_name == null) ){
                                $('#pic').attr('src','/static/img/user_img/unknown')
                             }
                             else{
                                $('#pic').attr('src','/static/img/user_img/' + data.current_name)
                                file.val('');
                                alert('wrong format');
                             }
                        }

                    },
                    error: function(){
                        alert('error')
                    }
                });

                file.val('');
            }
            else{
                alert('wrong format');
                file.val('');
            }

    });

    $('.edit_course').click(function(){
        var num = $(this).attr('class').split(' ');
        num_class = num[1]
    });

     $("#pass_ajax").click(function(){
        if($('#photo_course_').val() != ''){
        var formData = new FormData();
            formData.append('id_edit', num_class);
            formData.append('form-edit-course', $('#photo_course_')[0].files[0]);

            $.ajax({
                type:'POST',
                url:'/manage_courses',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(data, textStatus, jqXHR) {
                        $('#avatar'+ data.int_id).attr('src','/static/img/courses_img/'+ data.filename)
   //                     $('#photo_course_').val('');
                },
                error: function(){
                    alert('error')
                }
            });
        };
     });

    $('.image_profile').addClass('profile-courses-image');

     $("#pass-ajax-add").click(function(){
//         alert('ok')
       setTimeout(function(){
         if($('#course-img-add').val() != ''){
//           alert('1')
             formData = new FormData();
             formData.append('form-add-course', $('#course-img-add')[0].files[0]);
             formData.append('num', '1');
         }
         else{
//             alert('2')
             formData = new FormData();
             formData.append('form-add-course', $('#course-img-add').val());
             formData.append('num', '0');
         }
//         alert(formData)
             $.ajax({
                 type:'POST',
                 url:'/upload_info_courses',
                 data: formData,
                 dataType:"json",
                 cache:false,
                 contentType:false,
                 processData:false,
                 success: function(data, textStatus, jqXHR, json) {
//                   alert(data.filename)
                     $('#avatar'+ (data.new_id)).attr('src','/static/img/courses_img/'+ data.filename);
                     $('#course-img-add').val('');
                 },
                 error: function(){
                     alert('error')
                 }
             });
       }, 500);
     });
     course_photo.addClass('photo-param');

});


