$(document).ready( function() {
	function updateTips( text, title ) {
	    if(!title) {
	        title = "";
	    }
	    nice_alert(title, text);
	}

	function checkLength( o, n, min, max ) {
	    if ( o.val().length > max || o.val().length < min ) {
	        o.addClass( "ui-state-error" );
	        updateTips( "Length of " + n + " must be between " +
	            min + " and " + max + ".", n );
	        return false;
	    } 
	    else { return true; }
	}

	function checkRegexp( o, regexp, n ) {
	    if ( !( regexp.test( o.val() ) ) ) {
	        o.addClass( "ui-state-error" );
	        updateTips( n );
	        return false;
	    } else { return true; }
	}

    $('#img').addClass('hidden');

    var login = $("#login_info"),
		email = $("#email_info"),
		save_changes = $("#save_changes"),
		allFields = $( [] ).add(login).add(email);

	var	flag = 0;
	$(document).on("click", "#edit_profile", function() {

		if(flag == 0) {
            $('#img').removeClass('hidden');
            
			allFields.removeClass("clear_input");
			allFields.attr('readonly', false);

			$("#cancel_edit_profile").show(100);

			$(this).val("Save changes");
			flag++;
		}
		else
			if(flag == 1) {
                $('#img').addClass('hidden');
				var bValid = true;
		        bValid = bValid && checkLength( login, "Login", 4, 16 );
		        bValid = bValid && checkRegexp( login, /^[a-z]([0-9a-z_])+$/i,
		        	"Username may consist of a-z, 0-9, underscores, begin with a letter." );

		        bValid = bValid && checkLength( email, "E-mail", 6, 80 );
		        // From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
		        bValid = bValid && checkRegexp( email,
		        	/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "John@example.com" );

		        if(bValid) {
					$.ajax ({
						type: "POST",
						url: "profile",
						data: {"action": "edit_profile", "login": login.val(), "email": email.val()},	
						cache: false,
						success: function(data, textStatus, jqXHR) {
							$(login).val(data.login);
							$(email).val(data.email);
							nice_alert("Success", "Profile updated successfully")
						},
						error: function(jqXHR, textStatus, errorThrown) {
							nice_alert("Server error", "Sorry for the inconvenience. We working on it")
						}
					});

					$("#cancel_edit_profile").hide(100);

            		allFields.removeClass('ui-state-error');
					allFields.addClass("clear_input");
					allFields.attr('readonly', true);
					$(this).val("Edit profile");
					flag = 0;
				}//End valid if
			}//End flag if
    });
	
	$(document).on("click", "#cancel_edit_profile", function() {

		$.ajax ({
			type: "GET",
			url: "profile",
			data: {"action": "get_user"},	
			cache: false,
			async: false,
			success: function(data, textStatus, jqXHR) {
				$(login).val(data.login);
				$(email).val(data.email);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				nice_alert("Server error", "Sorry for the inconvenience. We working on it")
			}
		});

		$(this).hide(100);
		allFields.removeClass('ui-state-error');
		allFields.addClass("clear_input");
		allFields.attr('readonly', true);
		$("#edit_profile").val("Edit profile");
		flag = 0;
	});

	$(document).on("click", ".open_recomendations", function() {

		var course_id = $(this).closest(".brick").attr("id");
		var title = $(this).closest(".brick_wrapper").find(".course_name").html();

		$.ajax ({
			type: "GET",
			url: "profile",
			data: {"course_id": course_id, "action": "get_recomends"},	
			cache: false,
			async: false,
			success: function(data, textStatus, jqXHR) {
				$("#recomends_modal").append(data);
				openModal(title, $(document).height())
			},
			error: function(jqXHR, textStatus, errorThrown) {
				nice_alert("Server error", "Sorry for the inconvenience. We working on it")
			}
		});
	});

	function openModal(title, height) {
		var modal = $("#recomends_modal");

		modal.prop("title", title + " recomendations");
		$("#recomends_modal").dialog({
			autoOpen: false,
	        height: 500,
	        width: '80%',
	        resizable: false,
	        dragable: false,
	        modal: true,
			show: {
		        effect: "fade",
		        duration: 350
			},
			hide: {
				effect: "fade",
				duration: 400
			},
			close: function() {
				$("#books_wrapper").remove();
			}
		});
		modal.dialog("open");
	}
});
