$(document).ready(function () {
    //    Functions regrading tests left container where we choose what test to see
    $(document).on('click', '#select_category', function () {
        if ($(this).val() != '') {
            $.ajax({
                type: "GET",
                url: "/tests",
                data: {
                    category_id: $(this).val(),
                    action: 'select_course'
                },
                cache: false,
                success: function (data) {
                    $('#view_test_button').prop("disabled", true);
                    var select_course = $('<option value="">Select Course</option>');
                    $('#select_course').empty();
                    $('#select_course').append(select_course);
                    $('#select_course').prop("disabled", false);
                    var select_lecture = $('<option value="">Select Lecture</option>');
                    $('#select_lecture').empty();
                    $('#select_lecture').append(select_lecture);
                    $('#select_lecture').prop("disabled", true);
                    if (data.courses.length == 0) {
                        $('#select_course').prop("disabled", true);
                    } else {
                        for (var i = 0; i < data.courses.length; i++) {
                            var new_course = $('<option value="' + data.courses[i]['id'] + '">' + data.courses[i]['name'] + '</option>');
                            $('#select_course').append(new_course);
                        }
                    }
                },
                error: function () {
                    alert('ERROR');
                }
            });
        } else {
            $('#view_test_button').prop("disabled", true);
            var select_course = $('<option value="">Select Course</option>');
            $('#select_course').empty();
            $('#select_course').append(select_course);
            $('#select_course').prop("disabled", true);
            var select_lecture = $('<option value="">Select Lecture</option>');
            $('#select_lecture').empty();
            $('#select_lecture').append(select_lecture);
            $('#select_lecture').prop("disabled", true);
        }
    });
    $(document).on('click', '#select_course', function () {
        if ($(this).val() != '') {
            $.ajax({
                type: "GET",
                url: "/tests",
                data: {
                    course_id: $(this).val(),
                    action: 'select_lecture'
                },
                cache: false,
                success: function (data) {
                    var select_lecture = $('<option value="">Select Lecture</option>');
                    $('#select_lecture').empty();
                    $('#select_lecture').append(select_lecture);
                    $('#select_lecture').prop("disabled", false);

                    if (data.lectures.length == 0) {
                        $('#select_lecture').prop("disabled", true);
                        $('#view_test_button').prop("disabled", true);
                    } else {
                        $('#view_test_button').prop("disabled", false);
                        for (var i = 0; i < data.lectures.length; i++) {
                            var new_course = $('<option value="' + data.lectures[i]['id'] + '">' + data.lectures[i]['title'] + '</option>');
                            $('#select_lecture').append(new_course);
                        }
                    }
                },
                error: function () {
                    alert('ERROR');
                }
            });
        } else {
            $('#view_test_button').prop("disabled", true);
            var select_lecture = $('<option value="">Select Lecture</option>');
            $('#select_lecture').empty();
            $('#select_lecture').append(select_lecture);
            $('#select_lecture').prop("disabled", true);
        }
    });
    $(document).on('change', '#select_lecture', function () {
        if ($(this).val() != '') {
            $('#view_test_button').prop("disabled", false);
        }
    });

    $(document).on('click', '#view_test_button', function () {
        if ($('#select_lecture').val() == '') {
            var course = $('#select_course').val();
            $.ajax({
                type: "GET",
                url: "/tests",
                data: {
                    course: course,
                    action: 'view_test'
                },
                cache: false,
                success: function (data) {
                    $('#tests_main_container').empty();
                    $('#tests_main_container').append(data);
                },
                error: function () {
                    alert('ERROR');
                }
            });
        } else {
            var lecture = $('#select_lecture').val();
            $.ajax({
                type: "GET",
                url: "/tests",
                data: {
                    lecture: lecture,
                    action: 'view_test'
                },
                cache: false,
                success: function (data) {
                    $('#tests_main_container').empty();
                    $('#tests_main_container').append(data);
                },
                error: function () {
                    alert('ERROR');
                }
            });
        }

    });

    $(document).on('click', '.remove_lecture', function () {
        var current_test = $(this).closest($(".tests_lecture"));
        var dialog_tests = $('<div id="dialog-confirm" title="Remove test?"> <p><span style="float: left; margin: 0 7px 20px 0;"></span>This item will be removed and cannot be recovered. Are you sure?</p></div>');
        $(dialog_tests).dialog({
            resizable: false,
            height: 240,
            modal: true,
            buttons: {
                "Confirm": function () {
                    $(this).dialog("close");
                    var lecture_id = current_test.find($(".test_lecture_id")).html();
                    $.ajax({
                    type: "DELETE",
                    url: "/tests/" + lecture_id,
                    data: JSON.stringify({
                        lecture_id: lecture_id
                    }),
                    contentType: "application/json; charset=utf-8",
                    success: function () {
                        current_test.remove().fadeOut("slow")
                        $("#message").find("span").text("Test was successfully removed.")
                        $("#message").fadeIn("slow");
                        setTimeout(function () {
                            $("#message").fadeOut("slow");
                        }, 5000);
                        $("#message").find("span").text()
                    },
                    error: function () {
                        alert('ERROR');
                    }

                });

                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    });


});