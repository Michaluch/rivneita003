$(document).ready(function () {
    //    Functions for adding tests
    $(document).on('click', '#add_test_button', function () {
        $.ajax({
            type: "GET",
            url: "/tests",
            data: {
                action: 'add_test'
            },
            cache: false,
            success: function (data) {
                $('#tests_main_container').empty();
                $('#tests_main_container').append(data);
            },
            error: function () {
                alert('ERROR');
            }
        });
    });
    $(document).on('click', '#add_test_category_id', function add_test_category_id() {
        if ($(this).val() != '') {
            $.ajax({
                type: "GET",
                url: "/tests",
                data: {
                    category_id: $(this).val(),
                    action: 'select_course'
                },
                cache: false,
                success: function (data) {
                    var select_course = $('<option value="">Select Course</option>');
                    $('#add_test_course_id').empty();
                    $('#add_test_course_id').append(select_course);
                    $('#add_test_course_id').prop("disabled", false);
                    var select_lecture = $('<option value="">Select Lecture</option>');
                    $('#add_test_lecture_id').empty();
                    $('#add_test_lecture_id').append(select_lecture);
                    $('#add_test_lecture_id').prop("disabled", true);
                    if (data.courses.length == 0) {
                        $('#add_test_course_id').prop("disabled", true);
                        $('#final_yes').prop("disabled", true);
                        $('#final_no').prop("checked", true);
                    } else {
                        $('#final_yes').prop("disabled", true);
                        $('#final_no').prop("checked", true);
                        for (var i = 0; i < data.courses.length; i++) {
                            var new_course = $('<option value="' + data.courses[i]['id'] + '">' + data.courses[i]['name'] + '</option>');
                            $('#add_test_course_id').append(new_course);
                        }
                    }
                },
                error: function () {
                    alert('ERROR');
                }
            });
        } else {
            var select_course = $('<option value="">Select Course</option>');
            $('#add_test_course_id').empty();
            $('#add_test_course_id').append(select_course);
            $('#add_test_course_id').prop("disabled", true);
            var select_lecture = $('<option value="">Select Lecture</option>');
            $('#add_test_lecture_id').empty();
            $('#add_test_lecture_id').append(select_lecture);
            $('#add_test_lecture_id').prop("disabled", true);
            $('#final_yes').prop("disabled", true);
            $('#final_no').prop("checked", true);
        }
    });
    $(document).on('click', '#add_test_course_id', function () {
        if ($(this).val() != '') {
            $.ajax({
                type: "GET",
                url: "/tests",
                data: {
                    course_id: $(this).val(),
                    action: 'select_lecture_for_creation'
                },
                cache: false,
                success: function (data) {
                    var select_lecture = $('<option value="">Select Lecture</option>');
                    $('#add_test_lecture_id').empty();
                    $('#add_test_lecture_id').append(select_lecture);
                    $('#add_test_lecture_id').prop("disabled", false);
                    if (data.lectures.length == 0) {
                        $('#add_test_lecture_id').prop("disabled", true);
                        $('#final_yes').prop("disabled", true);
                        $('#final_no').prop("checked", true);
                    } else {
                        $('#final_yes').prop("disabled", false);
                        for (var i = 0; i < data.lectures.length; i++) {
                            var new_course = $('<option value="' + data.lectures[i]['id'] + '">' + data.lectures[i]['title'] + '</option>');
                            $('#add_test_lecture_id').append(new_course);
                        }
                    }
                },
                error: function () {
                    alert('ERROR');
                }
            });
        } else {
            var select_lecture = $('<option value="">Select Lecture</option>');
            $('#add_test_lecture_id').empty();
            $('#add_test_lecture_id').append(select_lecture);
            $('#add_test_lecture_id').prop("disabled", true);
            $('#final_yes').prop("disabled", true);
            $('#final_no').prop("checked", true);
        }
    });
    // this function will be fired when we click on the add-question button
    $(document).on('click', '#add_question', function () {
        // this var is assigned to div that wraps questions
        // it has unique id, based on previous variable
        var questionContainer = $('<div class="question_container" />');
        // this var is assigned to div that wraps answers
        var answerWrapper = $('<div class="answer_wrapper" />');
        // this var is assigned to div that will have answer wrappers inside
        var answerContainer = $('<div class="answer_container" />');
        // this var is assigned to a input text field, that will be used for questions
        var qName = $('<input type="text" class="question_name" placeholder="Question Name" />');
        // this var is assigned to a button that will be used to remove questions with answers
        var removeQuestion = $('<input type="button" class="remove_question" value="- question" />');
        // this function is used to remove parent div of the question
        removeQuestion.click(function () {
            $(this).parent().remove();
        });
        // this var is assigned to a input text field, that will be used for answers
        var aName = $('<input type="text" class="answer_name" placeholder="Answer Name" />');
        // this var is assigned to a select-option, that will be used with answers
        var aCorrect = $('<select class="answer_correct"> <option value="false">false</option> <option value="true">true</option> </select>');
        // this var is assigned to a button that will be used to remove  answers
        //        var removeAnswer = $('<input type="button" class="remove_answer" value="- answer" />');
        //        // this function is used to remove parent div of the answer
        //        removeAnswer.click(function () {
        //            $(this).parent().remove();
        //        });
        // this var is assigned to a button that will be used to add new answers
        var addAnswer = $('<input type="button" class="add_answer" value="+ answer" />');
        // here we wrap question input field and question remove button,
        // inside question wrapper div
        questionContainer.append(qName);
        questionContainer.append(removeQuestion);
        // here we wrap answers input field, answers select-option and answers remove button,
        // inside answers wrapper div
        answerContainer.append(aName);
        answerContainer.append(aCorrect);
        //        answerContainer.append(removeAnswer);
        // here we insert, already wrapped answers inside answer container div
        answerWrapper.append(answerContainer);
        answerWrapper.append(answerContainer.clone());
        // here we insert, answer container div, inside question wrapper divs
        questionContainer.append(answerWrapper);
        // here we insert, addAnswer button, it will be at the bottom of the questionContainer div
        // and will always be at the bottom of the questions
        questionContainer.append(addAnswer).hide().fadeIn("slow");
        // at the end, we add our complete divs to the bottom of add_test_questions_wrapper div id
        $('#add_test_questions_wrapper').append(questionContainer);
    });
    // this function will be fired when we click on the add-answer button
    $(document).on('click', '.add_answer', function () {
        // this var is assigned to div that wraps answers
        var answerContainer = $('<div class="answer_container" />');
        // this var is assigned to a input text field, that will be used for answers
        var aName = $('<input type="text" class="answer_name" placeholder="Answer Name" />');
        // this var is assigned to a select-option, that will be used with answers
        var aCorrect = $('<select class="answer_correct"> <option value="false">false</option> <option value="true">true</option> </select>');
        // this var is assigned to a button that will be used to remove  answers
        var removeAnswer = $('<input type="button" class="remove_answer" value="- answer" />');
        // this function is used to remove parent div of the answer
        removeAnswer.click(function () {
            $(this).parent().remove();
        });
        // here we wrap answers input field, answers select-option and answers remove button,
        // inside answers wrapper div
        answerContainer.append(aName);
        answerContainer.append(aCorrect);
        answerContainer.append(removeAnswer).hide().fadeIn("slow");
        // here we add our complete answers to the closest answer-container
        $(this).closest('.question_container').find('.answer_wrapper').append(answerContainer);
    });
    // this function will be fired when we click to submit our questions
    $(document).on('click', '#submit_question', function () {
        var error_count = 0;
        if ($('#add_test_lecture_id').val() == '') {
            error_count += 1
            $('#add_test_lecture_id').addClass('highlight_select_error');
            $("#message-error").find("span").text("Please select lecture")
            $("#message-error").fadeIn("slow");
            setTimeout(function () {
                $("#message-error").fadeOut("slow");
            }, 5000);
            $("#message-error").find("span").text()
        } else {
            $('#add_test_lecture_id').removeClass('highlight_select_error');
        }
        if ($('#add_test_name').val() == '') {
            error_count += 1
            $('#add_test_name').addClass('highlight_error');
        } else {
            $('#add_test_name').removeClass('highlight_error');
        }
        if ($('#add_test_description').val() == '') {
            error_count += 1
            $('#add_test_description').addClass('highlight_error');
        } else {
            $('#add_test_description').removeClass('highlight_error');
        }
        $('.question_name').each(function () {
            if ($(this).val() == '') {
                error_count += 1
                $(this).addClass('highlight_error');
            } else {
                $(this).removeClass('highlight_error');
            }
        })
        $('.answer_name').each(function () {
            if ($(this).val() == '') {
                error_count += 1
                $(this).addClass('highlight_error');
            } else {
                $(this).removeClass('highlight_error');
            }
        })
        if (error_count != 0) {
            $("#message-error").find("span").text("Please fill all highlighted fields");
            $("#message-error").fadeIn("slow");
            setTimeout(function () {
                $("#message-error").fadeOut("slow");
            }, 5000);
            $("#message-error").find("span").text()
            return
        }
        var answer_option_error = 0;
        $('.answer_wrapper').each(function () {
            var false_counter = 0;
            //            console.log($(this).find('.answer_container'));
            var number_of_answers = $(this).find('.answer_container').length;
            $(this).find("select.answer_correct").each(function () {
                if ($(this).val() == "false") {
                    false_counter += 1;
                }
            });
            //            console.log(false_counter);
            //            console.log(number_of_answers);
            if (false_counter == number_of_answers) {
                answer_option_error += 1;
                $(this).find("select.answer_correct").each(function () {
                    $(this).addClass('highlight_select_error');
                });
            } else {
                $(this).find("select.answer_correct").each(function () {
                    $(this).removeClass('highlight_select_error');
                });
            }
        });
        if (answer_option_error != 0) {
            $("#message-error").find("span").text("Please select at least one true answer for the highlighted answers");
            $("#message-error").fadeIn("slow");
            setTimeout(function () {
                $("#message-error").fadeOut("slow");
            }, 5000);
            $("#message-error").find("span").text()
            return
        }
        // we assign an empty array to var questions, that will be used
        // to store questions with answers
        var questions = [];
        // we iterate over each question-wrapper class
        $("#add_test_questions_wrapper").find(".question_container").each(function () {
            // create array for answers, that will be rewritten when,
            // iteration with new question will begin
            var answers = [];
            // then we iterate over every answer-wrapper div
            $(this).find('.answer_container').each(function () {
                // create empty object to hold answer text and their correctness
                var temp = {};
                // save current answer-name
                temp['answer'] = $(this).find('.answer_name').val();
                // look inside select-option, to see if current answer is true of false
                // then save it in our temp object, and at the end we push it to the answers array
                switch ($(this).find("select.answer_correct").first().val()) {
                    case "true":
                        temp['correct'] = true;
                        answers.push(temp);
                        break;
                    case "false":
                        temp['correct'] = false;
                        answers.push(temp);
                        break;
                }
            });
            //            console.log(answers);
            // when we finished iterating over answers in current question
            // we add question-name and answers that came with it, to our questions array
            questions.push({
                'question': $(this).find('.question_name').val(),
                'answers': answers
            });
        });
        //        console.log(questions);
        // saving needed fields
        var lecture_id = $('#add_test_lecture_id').first().val();
        var test_name = $('#add_test_name').val();
        var test_description = $('#add_test_description').val();
        var test_final = $("input[type='radio'][name='add_test_final']:checked").val();
//        console.log(test_final);
        test_final = test_final == "true";
//        console.log(test_final);
        var test_active = $("input[type='radio'][name='add_test_active']:checked").val();
        test_active = test_active == "true";
        // when we finished iterating over all questions and saved all needed fields
        // we create an AJAX PUT request, to add those questions and answers to db
        $.ajax({
            type: "POST",
            url: "/tests",
            data: JSON.stringify({
                lecture_id: lecture_id,
                name: test_name,
                description: test_description,
                final: test_final,
                active: test_active,
                questions: questions
            }),
            contentType: "application/json; charset=utf-8",
            cache: false,
            success: function (data) {
                if (data.status == true) {
                    $("#add_test_course_id").trigger("click");
                    $("#message").find("span").text("Test was successfully added")
                    $("#message").fadeIn("slow");
                    setTimeout(function () {
                        $("#message").fadeOut("slow");
                    }, 5000);
                    $("#message").find("span").text()
                } else {
                    $("#message-error").find("span").text("Test wasn't created")
                    $("#message-error").fadeIn("slow");
                    setTimeout(function () {
                        $("#message-error").fadeOut("slow");
                    }, 5000);
                    $("#message-error").find("span").text()
                }
            },
            error: function () {
                alert('ERROR');
            }
        });
    });
    $(document).on('click', '#final_yes', function () {
        var course_id = $('#add_test_course_id').first().val();
        $.ajax({
            type: "GET",
            url: "/tests",
            data: {
                course_id: course_id,
                action: 'has_final'
            },
            cache: false,
            success: function (data) {
                //                console.log(data.status);
                if (data.status == true) {
                    $('#final_yes').prop("checked", false);
                    $('#final_yes').prop("disabled", true);
                    $('#final_no').prop("checked", true);
                    $("#message-error").find("span").text("Course already has final test")
                    $("#message-error").fadeIn("slow");
                    setTimeout(function () {
                        $("#message-error").fadeOut("slow");
                    }, 5000);
                    $("#message-error").find("span").text()
                }
            },
            error: function () {
                alert('ERROR');
            }
        });
    });
});