$(document).ready(function () {
    //    Functions for editing tests

    $(document).on('click', '.cancel_lecture_edit', function () {
        var current_test = $(this).closest($(".tests_lecture"));
        var lecture_id = current_test.find($(".test_lecture_id")).html();
        $.ajax({
            type: "GET",
            url: "/tests",
            data: {
                lecture_id: lecture_id,
                action: 'cancel_edit'
            },
            cache: false,
            success: function (data) {
                current_test.html('');
                current_test.append(data).hide().fadeIn("slow");

            },
            error: function () {
                alert('ERROR');
            }
        });


//        $('div', current_test).each(function () {
//                    if ($(this).attr("class") == "test_name") {
//                        $(this).html("<span>" + data.response.name + "</span>");
//                    }
//                    if ($(this).attr("class") == "test_description") {
//                        $(this).html("<span>" + data.response.description + "</span>");
//                    }
//                    if ($(this).attr("class") == "test_final") {
//                        if (data.response.final) {
//                            $(this).html("True");
//                        } else {
//                            $(this).html("False");
//                        }
//
//                    }
//                    if ($(this).attr("class") == "test_active") {
//                        if (data.response.active) {
//                            $(this).html("True");
//                        } else {
//                            $(this).html("False");
//                        }
//                    }
//
//                });
//        $('div', current_test).each(function () {
//            if ($(this).attr("class") == "test_final" || $(this).attr("class") == "test_active" || $(this).attr("class") == "test_question_answer_correct") {
//                $(this).html($(this).find("option:selected").val());
//            }
//        });

        current_test.find('.save_lecture').hide();
        current_test.find('.edit_lecture').show();
        current_test.find('.cancel_lecture_edit').hide();
        current_test.find('.remove_lecture').show();

        current_test.find('.test_question_remove_question').hide();
        current_test.find('.test_question_answer_remove').hide();
        current_test.find('.test_question_add_answer').hide();
        current_test.find('.test_add_question').hide();
    });


    $(document).on('click', '.save_lecture', function () {
        var current_test = $(this).closest($(".tests_lecture"));
        var lecture_id = current_test.find($(".test_lecture_id")).html();

        var test_name = '';
        var test_description = '';
        var test_final = '';
        var test_active = '';
        var table_questions = '';

        var error_count = 0;

        if (current_test.find($('.test_name')).find($("input")).val() == '') {
            error_count += 1;
            current_test.find($('.test_name')).find($("input")).addClass('highlight_error');
        } else {
            current_test.find($('.test_name')).find($("input")).removeClass('highlight_error');
        }
        if (current_test.find($('.test_description')).find($("input")).val() == '') {
            error_count += 1;
            current_test.find($('.test_description')).find($("input")).addClass('highlight_error');
        } else {
            current_test.find($('.test_description')).find($("input")).removeClass('highlight_error');
        }
        current_test.find($('.test_question_name_text')).find($("input")).each(function () {
            if ($(this).val() == '') {
                error_count += 1;
                $(this).addClass('highlight_error');
            } else {
                $(this).removeClass('highlight_error');
            }
        });
        current_test.find($('.test_question_answer_text')).find($("input")).each(function () {
            if ($(this).val() == '') {
                error_count += 1;
                $(this).addClass('highlight_error');
            } else {
                $(this).removeClass('highlight_error');
            }
        });
        if (error_count != 0) {
            $("#message-error").find("span").text("Please fill all highlighted fields");
            $("#message-error").fadeIn("slow");
            setTimeout(function () {
                $("#message-error").fadeOut("slow");
            }, 5000);
            $("#message-error").find("span").text();
            return
        }


        var answer_option_error = 0;
        current_test.find($('.test_question_answers')).each(function () {
            var false_counter = 0;

            var number_of_answers = $(this).find('.test_question_answer').length;

//            console.log($(this).find('.test_question_answer'));
            $(this).find(".test_question_answer_correct").each(function () {
//                console.log($(this));
//                console.log($(this).find("option:selected").val());

                if ($(this).find("option:selected").val() == "false") {
                    false_counter += 1;
                }
            });
//            console.log(false_counter);
//            console.log(number_of_answers);
            if (false_counter == number_of_answers) {
                answer_option_error += 1;
                $(this).find(".test_question_answer_correct").each(function () {
                    $(this).find("select").addClass('highlight_select_error');
                });
            } else {
                $(this).find(".test_question_answer_correct").each(function () {
                    $(this).find("select").removeClass('highlight_select_error');
                });
            }
        });
        if (answer_option_error != 0) {
            $("#message-error").find("span").text("Please select at least one true answer for the highlighted answers");
            $("#message-error").fadeIn("slow");
            setTimeout(function () {
                $("#message-error").fadeOut("slow");
            }, 5000);
            $("#message-error").find("span").text();
            return
        }


        $('div', current_test).each(function () {
            if ($(this).attr("class") == "test_name") {
//                console.log($(this).find($("input")).val());
                test_name = $(this).find($("input")).val();
            }
            if ($(this).attr("class") == "test_description") {
//                console.log($(this).find($("input")).val());
                test_description = $(this).find($("input")).val();
            }
            if ($(this).attr("class") == "test_final") {
//                console.log($(this).find("option:selected").val());
                test_final = $(this).find("option:selected").val();
                test_final = test_final == "true";
            }
            if ($(this).attr("class") == "test_active") {
//                console.log($(this).find("option:selected").val());
                test_active = $(this).find("option:selected").val();
                test_active = test_active == "true";
            }
        });


        // we assign an empty array to var questions, that will be used
        // to store questions with answers
        var questions = [];
        // we iterate over each question-wrapper class

        var current_questions = current_test.find(".test_questions");
        current_questions.find(".test_question").each(function () {
            // create array for answers, that will be rewritten when,
            // iteration with new question will begin
            var answers = [];
            // then we iterate over every answer-wrapper div
            $(this).find('.test_question_answer').each(function () {
                // create empty object to hold answer text and their correctness
                var temp = {};
                // save current answer-name
                temp['answer'] = $(this).find('.test_question_answer_text :input').val();
                // look inside select-option, to see if current answer is true of false
                // then save it in our temp object, and at the end we push it to the answers array
                switch ($(this).find(".test_question_answer_correct option:selected").val()) {
                    case "true":
                        temp['correct'] = true;
                        answers.push(temp);
                        break;
                    case "false":
                        temp['correct'] = false;
                        answers.push(temp);
                        break;
                }
            });
            //            console.log(answers);
            // when we finished iterating over answers in current question
            // we add question-name and answers that came with it, to our questions array
            questions.push({
                'question': $(this).find('.test_question_name_text :input').val(),
                'answers': answers
            });
//            console.log(questions);
        });


//        console.log(lecture_id, test_name, test_description, test_final, test_active);
//        console.log(questions);


        // when we finished iterating over all questions and saved all needed fields
        // we create an AJAX PUT request, to add those questions and answers to db
        $.ajax({
            type: "PUT",
            url: "/tests/" + lecture_id,
            data: JSON.stringify({
                lecture_id: lecture_id,
                name: test_name,
                description: test_description,
                final: test_final,
                active: test_active,
                questions: questions
            }),
            contentType: "application/json; charset=utf-8",
            cache: false,
            success: function () {
                $('span', current_test).each(function () {
                    $(this).html($(this).find('input').val());
                });

                $('div', current_test).each(function () {
                    if ($(this).attr("class") == "test_final" || $(this).attr("class") == "test_active" || $(this).attr("class") == "test_question_answer_correct") {
                        $(this).html($(this).find("option:selected").val());
                    }
                });

                current_test.find('.save_lecture').hide();
                current_test.find('.edit_lecture').show();
                current_test.find('.cancel_lecture_edit').hide();
                current_test.find('.remove_lecture').show();

                current_test.find('.test_question_remove_question').hide();
                current_test.find('.test_question_answer_remove').hide();
                current_test.find('.test_question_add_answer').hide();
                current_test.find('.test_add_question').hide();

                $("#message").find("span").text("Test was successfully edited.");
                $("#message").fadeIn("slow");
                setTimeout(function () {
                    $("#message").fadeOut("slow");
                }, 5000);
                $("#message").find("span").text()

            },
            error: function () {
                $("#message-error").find("span").text("You don't have permission to edit this test");
                $("#message-error").fadeIn("slow");
                setTimeout(function () {
                    $("#message-error").fadeOut("slow");
                }, 5000);
                $("#message-error").find("span").text()
            }
        });
    });


    $(document).on('click', '.edit_lecture', function () {
        var current_test = $(this).closest($(".tests_lecture"));

        current_test.find('.edit_lecture').hide();
        current_test.find('.save_lecture').show();
        current_test.find('.remove_lecture').hide();
        current_test.find('.cancel_lecture_edit').show();

        current_test.find('.test_question_remove_question').show();
        current_test.find('.test_question_answer_remove').show();
        current_test.find('.test_question_add_answer').show();
        current_test.find('.test_add_question').show();


        $('span', current_test).each(function () {
            $(this).html('<input type="text" value="' + $(this).html() + '" />');
        });

        $('div', current_test).each(function () {
            if ($(this).attr("class") == "test_final" || $(this).attr("class") == "test_active" || $(this).attr("class") == "test_question_answer_correct") {
                if ($(this).html() == 'True') {
                    $(this).html('<select> <option value="true">true</option><option value="false">false</option> </select>');
                }
                else {
                    $(this).html('<select> <option value="false">false</option><option value="true">true</option> </select>');
                }
            }
        });
    });


    $(document).on('click', '.test_question_remove_question', function () {
        $(this).closest($(".test_question")).remove().hide().fadeIn("slow");
    });

    $(document).on('click', '.test_question_answer_remove', function () {
        $(this).closest($(".test_question_answer")).remove().hide().fadeIn("slow");
    });


    $(document).on('click', '.test_question_add_answer', function () {
        // this var is assigned to div that wraps answers
        var answerContainer = $('<div class="test_question_answer" />');
        // this var is assigned to a input text field, that will be used for answers
        var nameDiv = $('<div class="test_question_answer_text" />');
        var aName = $('<span><input type="text" placeholder="Answer Name" /></span>');
        // this var is assigned to a select-option, that will be used with answers
        var correctDiv = $('<div class="test_question_answer_correct" />');
        var aCorrect = $('<select> <option value="false">false</option> <option value="true">true</option> </select>');
        // this var is assigned to a button that will be used to remove  answers
        var removeAnswer = $('<input type="button" class="test_question_answer_remove" value="- answer" />');
        // this function is used to remove parent div of the answer
        removeAnswer.click(function () {
            $(this).parent().remove();
        });
        removeAnswer.show();
        // here we wrap answers input field, answers select-option and answers remove button,
        // inside answers wrapper div
        nameDiv.append(aName);
        correctDiv.append(aCorrect);
        answerContainer.append(nameDiv).hide().fadeIn("slow");
        answerContainer.append(correctDiv);
        answerContainer.append(removeAnswer);
        // here we add our complete answers to the closest answer-container
        $(this).closest('.test_question').find('.test_question_answers').append(answerContainer);
    });


    // this function will be fired when we click on the add-question button
    $(document).on('click', '.test_add_question', function () {
        // this var is assigned to div that wraps questions
        // it has unique id, based on previous variable
        var questionContainer = $('<div class="test_question" />');
        // this var is assigned to div that wraps answers
        var question_nameDiv = $('<div class="test_question_name" />');
        var question_name_textDiv = $('<div class="test_question_name_text" />');

        var answerWrapper = $('<div class="test_question_answers" />');
        // this var is assigned to div that will have answer wrappers inside
        var answerContainer = $('<div class="test_question_answer" />');
        // this var is assigned to a input text field, that will be used for questions
        var qName = $('<span><input type="text" class="test_question_name_text" placeholder="Question Name" /></span>');
        // this var is assigned to a button that will be used to remove questions with answers
        var removeQuestion = $('<input type="button" class="test_question_remove_question" value="- question" />');
        // this function is used to remove parent div of the question
        removeQuestion.click(function () {
            $(this).closest($('.test_question')).remove();
        });
        removeQuestion.show();

        var nameDiv = $('<div class="test_question_answer_text" />');
        var aName = $('<span><input type="text" placeholder="Answer Name" /></span>');
        // this var is assigned to a select-option, that will be used with answers
        var correctDiv = $('<div class="test_question_answer_correct" />');
        var aCorrect = $('<select> <option value="false">false</option> <option value="true">true</option> </select>');
        // this var is assigned to a button that will be used to remove  answers
        var removeAnswer = $('<input type="button" class="test_question_answer_remove" value="- answer" />');
        // this function is used to remove parent div of the answer
        removeAnswer.click(function () {
            $(this).parent().remove();
        });
        removeAnswer.show();
        // here we wrap answers input field, answers select-option and answers remove button,
        // inside answers wrapper div

        // this var is assigned to a button that will be used to add new answers
        var addAnswer = $('<input type="button" class="test_question_add_answer" value="+ answer" />');
        addAnswer.show();
        // here we wrap question input field and question remove button,
        // inside question wrapper div
        question_name_textDiv.append(qName);
        question_nameDiv.append(question_name_textDiv);
        question_nameDiv.append(removeQuestion);
        questionContainer.append(question_nameDiv);

        // here we wrap answers input field, answers select-option and answers remove button,
        // inside answers wrapper div
        nameDiv.append(aName);
        correctDiv.append(aCorrect);
        answerContainer.append(nameDiv);
        answerContainer.append(correctDiv);
        //        answerContainer.append(removeAnswer);
        // here we insert, already wrapped answers inside answer container div
        answerWrapper.append(answerContainer);
        answerWrapper.append(answerContainer.clone());
        // here we insert, answer container div, inside question wrapper divs
        questionContainer.append(answerWrapper);
        // here we insert, addAnswer button, it will be at the bottom of the questionContainer div
        // and will always be at the bottom of the questions
        questionContainer.append(addAnswer).hide().fadeIn("slow");
        // at the end, we add our complete divs to the bottom of add_test_questions_wrapper div id
        $(this).closest($('.test_container')).find($('.test_questions')).append(questionContainer);
    });
});