$(document).ready(function () {
    //    Functions for searching tests
    $(document).on('click', '#search_test_button', function () {
        var search_key = $('#search_test_option').find("option:selected").val();
        var search_value = $('#search_test_field').val();


        $.ajax({
            type: "GET",
            url: "/tests",
            data: {
                search_key: search_key,
                search_value: search_value,
                action: 'search'
            },
            cache: false,
            success: function (data) {
                $('#tests_main_container').empty();
                $('#tests_main_container').append(data).hide().fadeIn("slow");

//                setTimeout(function () {
//                    $("#tests_main_container").fadeIn("slow");
//                }, 500);

                $("#message").find("span").text("Your search returned " + $('.tests_lecture').length + " result(s)")
                $("#message").fadeIn("slow");
                setTimeout(function () {
                    $("#message").fadeOut("slow");
                }, 5000);
                $("#message").find("span").text()

            },
            error: function () {
                $("#message-error").find("span").text("Oopps...Error occurred, we will fix it soon!")
                $("#message-error").fadeIn("slow");
                setTimeout(function () {
                    $("#message-error").fadeOut("slow");
                }, 5000);
                $("#message-error").find("span").text()
            }
        });
    });
});