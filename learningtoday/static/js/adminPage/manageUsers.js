$(document).ready(function() {

$(document).on("click", "#manage_users", function() {
     $.ajax({
          type: "GET",
          url: "/manage_users",
          data: 'action=manage',
          cache: true,
          success: function(data, textStatus, jqXHR) {
	       $('#UsersTable tbody > tr').remove();
	       var status;
	       var deleted;
	       for (var i=0; i<data.search_result.length; i++ ){
		    if (data.search_result[i].status_id==1){
                         status ='yes';
                         deleted = 'Deactivate'
		    }
		    else {status ='no';
                         deleted = 'Activate'
                         };
                    var tr = '<TR class="currentUser"><TD name="id">'+ data.search_result[i].id +'</TD>'+
                            '<TD name="login">'+ data.search_result[i].login +'</TD>'+
                            '<TD name="f_name">'+ data.search_result[i].f_name +'</TD>'+
                            '<TD name="l_name">'+ data.search_result[i].l_name +'</TD>'+
                            '<TD name="email">'+ data.search_result[i].email +'</TD>'+
                            '<TD name="role_id">'+ data.search_result[i].role_id +'</TD>'+
                            '<TD name="status_id">'+ status +'</TD>'+
                            '<TD><button class="editUser">Edit</button></TD>'+                            
                            '<TD><button class="removeUser">'+ deleted + '</button></TD>' +
                        '</TR>';
                        $("#UsersTable tbody").append(tr);
                        }
                         
          },
          error: function(jqXHR, textStatus, errorThrown) {
               alert(textStatus);
          }
          });
     });
})