function checkCommentFields(search_key, search_value){
      var title_regex = new RegExp(/^[\w\s-\":;,]$/);
      var body_regex = new RegExp(/^[\w\s-@\":;,()%]$/);
      var errorMes = '';
      if (search_value=='') {
            return true;
      }
      else {
      switch(search_key) {
            case 'title': {
                if (!title_regex.test(search_value)) {errorMes = "Title is not allowed to contain such symbols! "}
                if  (!((4<search_value.length) && (search_value.length<70))){
                    errorMes = errorMes + "Title lenght must be between 4 and 70! ";}
                return errorMes;
                break;
            }
            case 'body': {
                  if (!body_regex.test(search_value)) {errorMes = "Body is not allowed to contain such symbols! ";}
                if (!((10<search_value.length) && (search_value.length<400))){
                    errorMes = errorMes + "Body lenght must be from 10 to 400! ";}
                          return errorMes
                  break;
            }
            default: {
              return true;
            }
      }
}}

function checkSearchPhraseNew(search_key, search_value){
      var login_regex = new RegExp(/^[0-9a-zA-Z_]$/);
      var title_regex = new RegExp(/^[\w\s-\":;,]$/);
      var body_regex = new RegExp(/^[\w\s-@\":;,()%]$/);
      if (search_value=='') {
            return true;
      }
      else {
      switch(search_key) {
            case 'user_id': {
                if ((login_regex.test(search_value)) || (search_value=='')){
                      return true }
                else {
                      alert("Username may consist of a-z, 0-9, underscores, begin with a letter.");
                      return false}
              break;
            }
            case 'title': {
                  if (title_regex.test(search_value)){
                        return true}
                  else {
                        alert("Title is not allowed to contain such symbols");
                        return false}
                  break;
            }
            case 'body': {
                  if (body_regex.test(search_value)){
                          return true}
                    else {
                          alert("Body is not allowed to contain such symbols");
                          return false}
                  break;
            }
            default: {
              return true;
            }
      }
}}

function onRemoveClick(thisis) {
        var tr = $(thisis).closest('.currentComment');
        var user_id = tr.closest('.currentComment').find('td').eq(4).html();
        var status_id = tr.closest('.currentComment').find('td').eq(6).html();
        var id = tr.closest('.currentComment').find('td').eq(9).html();
        if (status_id=="yes"){
            var c = confirm("Are you sure you want to deactivate this feedback?")
        }
        else {
            c=true}
        if (c==true){
        if(status_id == 'yes') {
            status_id = 1;
        }
        else
            if(status_id == 'no') {
                status_id = 0;
            }
        $.ajax({
            type: "POST",
            url: "/feedbacks",
            data: {"user_id": user_id, "active": status_id, "action":"edit_status", "id": id },
            cache: false,
            success: function(data, textStatus, jqXHR) {
                if(data.new_status == '1') {
                    tr.closest('.currentComment').find('td').eq(6).html('yes');
                    tr.closest('.currentComment').find('.removeComment').html('Deactivate');
                }
                else
                    if(data.new_status == '0') {
                        tr.closest('.currentComment').find('td').eq(6).html('no');
                        tr.closest('.currentComment').find('.removeComment').html('Activate');
                    }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error');
            }
        });
    }
}

function onSearchClick(search_done) {
    var search_key = $('#search_comment_key').find(":selected").val();
        var search_value;
        var search_date_start;
        var search_date_finish;
        if (search_key=='active') {
            search_value = $('#drop_down_active').find(":selected").val();
        }
        else {
            if (search_key=='lecture_id') {
                search_value = $('#drop_down_lectures').find(":selected").val();
            }
            else {
                    if (search_key=='creation_date') {
                            search_date_start = $('#date_search_start').val();
                            search_date_finish = $('#date_search_finish').val();
                    }
                    else {search_value = $('#search_comment').val();}
            }
                        
        }
        if (search_key=='creation_date') {
            if (search_date_start<=search_date_finish) {
            $.ajax({
                type: "POST",
                url: "/feedbacks",
                data: {"search_key": search_key, "action": 'search', 'start': search_date_start, 'finish': search_date_finish, 'page': search_done},
                cache: false,
                success: function(data, textStatus, jqXHR) {
                    $('#comments_table_wrapper').empty();
                    $("#comments_table_wrapper").append(data);     
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error');
                }
        });}
            else {alert('first date should be earlier than second')}
        }
        else {
            if (checkSearchPhraseNew(search_key, search_value)){
            $.ajax({
            type: "POST",
            url: "/feedbacks",
            data: {"search_key": search_key, "action": 'search', 'search_value': search_value, 'page': search_done},
            cache: false,
            success: function(data, textStatus, jqXHR) {
                $('#comments_table_wrapper').empty();
                $("#comments_table_wrapper").append(data);     
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error');
            }
        });}
        }
        
}

function onBanClick(thisis) {
    var tr = $(thisis).closest('.currentComment');
        var user_id = tr.closest('.currentComment').find('td').eq(4).html();
        var status_id = tr.closest('.currentComment').find('td').eq(8).find('button').html().trim();
        if (status_id=="Ban"){
            var c = confirm("Are you sure you want to ban this user?")
        }
        else {
            c=true}
        if (c==true){
        if (status_id =='Ban') {
            status_id = 1;
        }
        else {if (status_id=='Unban'){
            status_id = 2;
            }
        }
        $.ajax({
            type: "POST",
            url: "/manage_users",
            data: {"login": user_id, "status_id": status_id, "user_id":user_id, "action": "change_user_status"},
            cache: false,
            success: function(data, textStatus, jqXHR) {
                if(data.new_status == '1') {
                    tr.closest('.currentComment').find('td').eq(8).find('button').html('Ban');
                    $('.currentComment').each(function(){
                        if ($(this).find('td').eq(4).html()==user_id){
                            $(this).find('td').eq(8).find('button').html('Ban');}})
                }
                else
                    if(data.new_status == '2') {
                        tr.closest('.currentComment').find('td').eq(8).find('button').html('Unban');
                        $('.currentComment').each(function(){
                            if ($(this).find('td').eq(4).html()==user_id){
                            $(this).find('td').eq(8).find('button').html('Unban');}})
                    }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error');
            }
        });}
}

$(document).ready(function(){
    var search_done = 0;
    $(document).on("click", ".removeComment", function() {
        onRemoveClick(this);
    });
    
    $(document).on("click",'.banUser', function(){

        onBanClick(this)
    })
    
    $(document).on("click",'.page', function(){
        number = $(this).html().trim()
        if (search_done==0){
            number = $(this).html().trim();
            $.ajax({
                type: "POST",
                url: "/feedbacks",
                data: {"number": number, "action": "change_page"},
                cache: false,
                success: function(data, textStatus, jqXHR) {
                    $('#comments_table_wrapper').empty();
                    $("#comments_table_wrapper").append(data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error');
                }
            });}
        else {onSearchClick(number)}
    })
    
    $(document).on('click', '#main_check_box', function(){
        $(".check_box").prop('checked', $(this).prop('checked'));
    });
    
    $(document).on('click','#button_action', function(){
        var action = $('#select_action').find(":selected").val();
        switch (action) {
            case 'ban': {
                $('.check_box:checked').each(function(){
                    if ($(this).closest('.currentComment').find('td').eq(8).find('button').html().trim()=='Ban')
                    {onBanClick(this);}
                })
                break;
            }
            case 'unban': {
                $('.check_box:checked').each(function(){
                    if ($(this).closest('.currentComment').find('td').eq(8).find('button').html().trim()=='Unban')
                    {onBanClick(this);}
                })
                break;
            }
            case 'activate': {
                $('.check_box:checked').each(function(){
                    if ($(this).closest('.currentComment').find('td').eq(6).html()=='no')
                    {onRemoveClick(this);}
                })
                break;
            }
            case 'deactivate': {
                $('.check_box:checked').each(function(){
                    if ($(this).closest('.currentComment').find('td').eq(6).html()=='yes')
                    {onRemoveClick(this);}
                })
                break;
            }
        }
        //$(".check_box").prop('checked', false);
        //$("#main_check_box").prop('checked', false);
    })
    
    $(document).on('click', '#button_comment_search', function(){
        search_done = 1;
        var course_id = $('#drop_down_courses').find(":selected").val();
        if (course_id!="default") {
        onSearchClick(search_done);
        }})
    
        
        $(document).on('change', '#drop_down_courses', function() {
            var course_id = $('#drop_down_courses').find(":selected").val();
            if (course_id!="default") {
            $.ajax({
                    type: "POST",
                    url: "/feedbacks",
                    data: {"action": 'lectures', "course_id": course_id},
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $('#drop_down_lectures').empty();
                        for (var i=0; i<data.lectures.length; i++){
                            var op = '<option value="'+ data.lectures[i].id +'">'+ data.lectures[i].name +'</option>'
                            $('#drop_down_lectures').append(op);
                            $('#drop_down_lectures').attr('disabled', false);
                        }
                        
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert('Error');
                    }
            })
        }
        else {
            $('#drop_down_lectures').empty();
            op = '<option value="default">choose course first</option>';
            $('#drop_down_lectures').append(op);
        }
        })
       
        $(document).on('change', '#search_comment_key', function() {
            if ($('#search_comment_key').find(":selected").val()=='lecture_id') {
                $('#select_wrapper').empty();
                $.ajax({
                    type: "GET",
                    url: "/feedbacks",
                    data: {"action": 'courses'},
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $('#select_wrapper').append('<select name="drop_down_courses" id="drop_down_courses" class="drop1"></select>');    
                        var op = '<option value="default">choose course</option>';
                        $('#drop_down_courses').append(op);
                        for (var i=0; i<data.courses.length; i++){
                            op = '<option value="'+ data.courses[i].id +'">'+ data.courses[i].name +'</option>'
                            $('#drop_down_courses').append(op);
                        }
                        $('#select_wrapper').append('<select name="drop_down_lectures" id="drop_down_lectures" class="drop1"></select>')
                        op = '<option value="default">choose lecture</option>';
                        $('#drop_down_lectures').append(op);
                        $('#drop_down_lectures').attr('disabled', true);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert('Error');
                    }
            });
            }
            else{
                if ($('#search_comment_key').find(":selected").val()=='active') {
                    $('#select_wrapper').empty();
                    dropDownActive = '<select name="drop_down_active" id="drop_down_active" class="drop1">'+
                                    '<option value="1">Activated</option>'+
                                    '<option value="0">Deactivated</option>'+
                                              '</select>';
                    $('#select_wrapper').append(dropDownActive);
                }
                else {
                        if ($('#search_comment_key').find(":selected").val()=='creation_date') {
                            $('#select_wrapper').empty();
                            dropDownDateStart = '<input type="text" name="search_value" id="date_search_start" placeholder="from" class="dates">';
                            dropDowndateFinish = '<input type="text" name="search_value" id="date_search_finish" placeholder="to" class="dates">';
                            $('#select_wrapper').append(dropDownDateStart);
                            $('#select_wrapper').append(dropDowndateFinish);
                            $('#date_search_start').datepicker({
                                dateFormat: 'yy-mm-dd'
                            });
                            $('#date_search_start').attr('readOnly', true);
                            $('#date_search_finish').datepicker({
                                dateFormat: 'yy-mm-dd'
                            });
                            $('#date_search_finish').attr('readOnly', true);
                        }
                        else {
                        if ($('#search_comment').length) {
                                  $('#search_comment').val('');
                        } else {
                                  $('#select_wrapper').empty();
                                  var search = '<input type="text" name="search_value" id="search_comment">'
                                  $('#select_wrapper').append(search);
                                
                        }}
                }
            }
        })
        
    $('#manage-comments').click(function() {
        $.ajax({
                type: "GET",
                url: "/feedbacks",
                cache: false,
                success: function(data, textStatus, jqXHR) {
                    $('#feedback_tab').remove();
                    $('#tabs-4').append(data)},
                error: function() {}
            })
        search_done = 0;
    })
	
    
    $(document).on('keypress', '#search_comment', function(event){
        if (event.keyCode == 13) {
            $('#button_comment_search').click();
    }
    })

    $(document).on('click', '#add_comment', function(){
        $.ajax({
            type: "POST",
            url: "/feedbacks",
            data: {"action": 'lectures'},
            cache: false,
            success: function(data, textStatus, jqXHR) {
                $('#selectedLecture').empty();
                for (var i=0; i<data.lectures.length; i++){
                    var op = '<option value="'+ data.lectures[i].id +'">'+ data.lectures[i].name +'</option>'
                    $('#selectedLecture').append(op);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error');
            }
            })
        $('#dialog-comment-add').dialog('open')
        })

    $(document).on('focus','#title', function(){
            $('#title').removeClass("ui-state-error");})

    $(document).on('focus','#body', function(){
            $('#body').removeClass("ui-state-error");})
    
    $('#dialog-comment-add').dialog({
        autoOpen: false,
        height: 560,
        width: 350,
        resizable: false,
        dragable: false,
        modal: true,
        buttons: {'Create comment': function() {
            var title = $('#title').val().trim();
            var body = $('#body').val().trim();
            var errorMessage = '';
            alert(errorMessage);
            errorMessage = checkCommentFields('title', title);
            errorMessage = errorMessage + checkCommentFields('body', body);
alert(errorMessage);
                if (errorMessage==''){
                    $.ajax({
                    type:'POST',
                    url: '/feedbacks',
                    data: { 'title': title,
                            'body': body,
                            'lecture_id': $('#selectedLecture').find(":selected").val(),
                            'action': 'add'
                            },
                    cache: false,
                    success: function(data, textStatus, jqXHR) {
                        $.ajax({
                            type: "POST",
                            url: "/feedbacks",
                            data: {"number": 1, "action": 'change_page'},
                            cache: false,
                            success: function(data, textStatus, jqXHR) {
                                $('#comments_table_wrapper').empty();
                                $("#comments_table_wrapper").append(data);
                                $('#title').val('');
                                $('#body').val('');
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                alert('Error');
                            }
                        });
                        $('#dialog-comment-add').dialog('close');
                        $("#tip").empty().removeClass( "ui-state-highlight" );
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert('Server error');
                    }
                }); }
                //End of ajax
                else {
                    var validateTips = $( "#tip")
                    validateTips.empty().append(errorMessage).addClass( "ui-state-highlight" );
                }
        }  //End of save button
        },  //Close save button
        Cancel: function() {
            //Close dialog and remove all error classes(red inputs, messages, etc) if button 'cancel' was pressed
            $(this).dialog('close');
            $("#tip").empty().removeClass( "ui-state-highlight" );
            $('#title').removeClass("ui-state-error");
            $('#body').removeClass("ui-state-error");
        },
        close: function() {
            $("#tip").empty().removeClass( "ui-state-highlight" );
            $('#title').removeClass("ui-state-error");
            $('#body').removeClass("ui-state-error");//Remove all error classes(red inputs, messages, etc) if button 'close' was pressed
        }}
    )
}
)