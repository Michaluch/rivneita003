function checkSearchPhrase(search_key, search_value){
      var login_regex = new RegExp(/^[0-9a-z_]$/i);
      var name_regex = new RegExp(/^[a-z ,\.'-]$/i);
      var email_regex = new RegExp(/^[a-z0-9_\.@]$/i);
      if (search_value=='') {
            return true;
      }
      else {
      switch(search_key) {
            case 'login': {
                if ((login_regex.test(search_value)) || (search_value=='')){
                      return true }
                else {
                      alert("Username may consist of a-z, 0-9, underscores, begin with a letter.");
                      return false}
              break;
            }
            case 'f_name': {
                  if (name_regex.test(search_value)){
                        return true}
                  else {
                        alert("First name field only allow : a-z");
                        return false}
                  break;
            }
            case 'l_name': {
                  if (name_regex.test(search_value)){
                          return true}
                    else {
                          alert("Last name field only allow : a-z");
                          return false}
                  break;
            }
             case 'email': {
                  if (login_regex.test(search_value)){
                          return true}
                    else {
                          alert("Email sfield only allow: a-z, 0-9 and ._@");
                          return false}
                  break;
            }
             case 'role_id': {
                  return true
                  break;
            }
             case 'status_id': {
                  return true
                  break;
            }
            default: {
              return false
            }
      }
}}

$(document).ready(function() {

      $("#button_search").click( function() {
      var search_key = $('#search_key').find(":selected").val();
      var search_value
      if (search_key=='status_id') {
            search_value = $('#drop_down_status').find(":selected").val();
            }
      else {
            if (search_key=='role_id') {
                  search_value = $('#drop_down_role').find(":selected").text();
            }
            else {search_value = $('#search').val();}
      }
            
      if (checkSearchPhrase(search_key, search_value)) {
            $.ajax({
                  type: "POST",
                  url: "/manage_users",
                  data: {'search_key': search_key,
                        'search_value': search_value,
                        'action': 'search'},
                  cache: false,
                  dataType: 'json',
                  success: function(data, textStatus, jqXHR) {
                        
                  $("#UsersTable").empty();
		          var tr = '<thead><tr><TH>Id</TH><TH>Username</TH>+<TH>First name</TH><TH>Last name</TH><TH>E-mail</TH><TH>Group</TH>' +
                  '<TH>Active</TH>'+
                  '<TH>Edit</TH>'+
                  '<TH>Change Status</TH>'+
                  '</tr>'+
                  '</thead>';
                  $("#UsersTable").append(tr);
                  var status;
                  var deleted;
                  for (var i=0; i<data.search_result.length; i++ ){
                        if (data.search_result[i].status_id==1){
                              status ='yes';
                              deleted = 'Deactivate'
                        }
                        else {status ='no';
                              deleted = 'Activate'};
                  var tr = '<TR class="currentUser"><TD name="id">'+ data.search_result[i].id +'</TD>'+
                            '<TD name="login">'+ data.search_result[i].login +'</TD>'+
                            '<TD name="f_name">'+ data.search_result[i].f_name +'</TD>'+
                            '<TD name="l_name">'+ data.search_result[i].l_name +'</TD>'+
                            '<TD name="email">'+ data.search_result[i].email +'</TD>'+
                            '<TD name="role_id">'+ data.search_result[i].role_id +'</TD>'+
                            '<TD name="status_id">'+ status +'</TD>'+
                            '<TD><button class="editUser">Edit</button></TD>'+                            
                            '<TD><button class="removeUser">'+ deleted + '</button></TD>' +
                        '</TR>';
                  $("#UsersTable").append(tr);}            
                },
            error: function(jqXHR, textStatus, errorThrown) {
                  alert(textStatus);
            }
      });}
      });
             
      $('#search_key').change(function() {
            if ($('#search_key').find(":selected").val()=='role_id') {
                  $('#search').remove();
                  $('#drop_down_status').remove();
                  var dropDownRole = '<select name="drop_down_role" id="drop_down_role" class="drop">'+
                                    '<option value="1">Admin</option>'+
                                    '<option value="2">Teacher</option>'+
                                    '<option value="3">Student</option>'+
                                '</select>';
                  $('#search_key').after(dropDownRole);
            }
            else{
                  if ($('#search_key').find(":selected").val()=='status_id') {
                        if ($('#search').length) {$('#search').remove();}
                        if ($('#drop_down_role').length)$('#drop_down_role').remove();
                        dropDownStatus = '<select name="drop_down_status" id="drop_down_status" class="drop">'+
                                          '<option value="1">Activated</option>'+
                                          '<option value="2">Deactivated</option>'+
                                      '</select>';
                        $('#search_key').after(dropDownStatus);
                  }
                  else {
                        if ($('#search').length) {
                              $('#search').val('');
                        } else {
                              $('#drop_down_role').remove();
                              $('#drop_down_status').remove();
                              var search = '<input type="text" name="search_value" id="search">'
                              $('#search_key').after(search);
                              
                              $('#search').keypress(function(event){
                                    if (event.which == 13) {
                                          $('#button_search').click()
                                    }
                              })
                        }
            }
            }
            
      })
                  
      $('#search').keypress(function(event){
            if (event.which == 13) {
                  $('#button_search').click()
            }
      })
        
})

