$(document).ready(function() {
	function get_users() {
		$.ajax({
            type: "GET",
            url: "/manage_users",
            cache: false,
            success: function (data) {
                $('#manage_users_wrapper').remove();
                $("#tabs-1").append(data);
            },
            error: function () {
                alert('Server error');
            }
        });
	}

	$(document).on('click', '#manage_users', function () {
		get_users();
	});

	get_users();
});