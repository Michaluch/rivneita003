$(document).ready(function() {
	$("#search_course").on("click", function() {
		var search_key = $("select[name='course_search_key']").val(),
		search_text = $("#search_course_text").val() || "_",
		courses_table = $("#CoursesTable"),
		active_category = $(".tabbed_box .tabs").find(".active"),
		active_category_title = active_category.attr("title"),
		active_category_id = active_category_title.split("_")[1];
		if (search_text.length > 0){
			$.ajax({
				url: "/manage_courses",
				data: "action=search&category_id=" + active_category_id + "&key" + "=" + search_key + "&value" + "=" + search_text,
				type: "GET",
				dataType: "json",
				success: function(json){
					if (json.result == "success"){
						$("#category_" + active_category_id).find(".course_description").remove();
						for (var i=0; i<json.courses.length; i++) {
							var course = json.courses[i];
							div = '<div class="course_description" id="course_' + course.id + '">';
		                    div += '<div class="course_image"><img class="photo-param" src="static/img/courses_img/' + course.image + '"> </div>';
		                    div += '<div class="course_header">';
		                    div += '<p class="course_name">' + course.name + '</p>';
		                    div += '<p class="course_author">by: ' + course.author + '</p>';
		                    div += '<p class="course_text">' + course.description + '</p></div>';
		                    div += '<div class="right course_right">';
		                    div += '<a href="#" class="edit_course">edit</a>';
		                    div += '<a href="#" class="change_course_status">';
		                    if ( course.deleted == 0 ) {
		                    	div += '  deactivate  ';
		                    } else if ( course.deleted == 1 ) {
		                    	div += '  activate  ';
		                    }
		                    div += '</a></div></div>';
		                    $("#category_" + active_category_id).append(div);
		                }

					}
					if (json.result == "error"){
						$("#category_" + active_category_id).find(".course_description").remove();
					}
				}
			});
		}
		
	});
		
});
	