$(document).ready(function() {
    var parent_elem, tips = $("#course_validate_tips"),
    id = $("#form-edit-course input[name=id]"),
    category = $("#edit-course-category-selector"),
    name = $("#form-edit-course input[name=name]"),
    description = $("#form-edit-course textarea[name=description]"),
    image = $("#form-edit-course input[name=image]"),
    fdialog = $("#courses-dialog-edit"),
    form = $("#form-edit-course"),
    form_inputs = $("#form-edit-course input");
    var num_class = null

        
    fdialog.dialog({
      autoOpen: false,
      height: 440,
      width: 340,
      resizable: true,
      dragable: false,
      modal: true,

      buttons: {"Edit course":{
           text:"Edit course",
           id:'pass_ajax',
           click:function() {
              valid = true;
              remove_class(form_inputs, "ui-state-error");
              remove_class(description, "ui-state-error");

              clear_dialog_form(tips, null);

              valid = valid && checkLength(name, "name", 3, 120, tips)
              valid = valid && checkLength(description, "description", 6, 400, tips)

              valid = valid && checkRegexp(name, /^[a-zA-Z][a-zA-Z0-9- \.,;:\(\)\?!"']+$/, "Name field only allow : A-Z a-z 0-9 - . : ; ' \" ? ! ( )", tips)
              valid = valid && checkRegexp(description, /^[a-zA-Z][a-zA-Z0-9- \.,;:\(\)\?!"']+$/, "Description field only allow : A-Z a-z 0-9 - . : ; ' \" ? ! ( )", tips)
              if (valid) {
                $.ajax({
                url: "/manage_courses",
                data: "action=edit&" + form.serialize(),
                type: "PUT",
                dataType: "json",
                success: function(json){
                    if (json.result == "success") {
                        updateTips( "course edited" , tips);
                        parent_elem.find('p.course_name').text(name.val());
                        parent_elem.find('p.course_text').text(description.val());
                        }
                    if (json.result == "error") {
                        if ($('#photo_course_').val() != ''){
                            updateTips( "course edited" , tips)
                            $('#photo_course_').val('');
                        }
                        else{
                            updateTips( json.error , tips)
                        }
                    }

                },
                error: function( xhr, status ) {
                    updateTips( status , tips)
                }
                });
              }
        }},
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        clear_dialog_form(tips, form_inputs);
        remove_class(form_inputs, "ui-state-error");
        remove_class(description, "ui-state-error");

      }
    });

$(document).on("click", ".edit_course ", function() {
      var $this = $(this);
      parent_elem = $this.closest("div.course_description");
      var parent_id = parent_elem.attr("id");
      id.val(parent_id.split('_')[1]);
      name.val(parent_elem.find('p.course_name').text());
      description.val(parent_elem.find('p.course_text').text());
      var a = '#courses-dialog-edit'+num_class;
      fdialog.dialog( "open" );
    });

});