
$(document).ready(function() {
    var category_name_field = $('#new_category');
    $('.category_dropdown').change(function() {
        $this = $(this);
        category_name_field.removeClass('ui-state-error');
        $('#new_category').val($this.find('option:selected').text());
    });

    $(document).on('click', '#add_category_button', function(event) {
        $this = $(this);
        category_name_field.removeClass('ui-state-error');
        var color = rgb2hex($this.closest('#new').find('.admin-color-pick-add').css( "background-color" ));
        $this.closest('#new').find('.admin-color-pick-add').css("background-color", "#ffffff");
        var id = $this.closest('#new').find('select.category_dropdown').find('option:selected').val().split('_')[2];
        var name = category_name_field.val();
        var tabs_panel = $this.closest('.tabs');
        var tabed_box = $this.closest('#v-nav');
        if (! check_regexp_simple(category_name_field, /^[a-zA-Z][a-zA-Z0-9- \.,;:\(\)\?!"']+$/) || ! check_length_simple(category_name_field, 2, 40) ){
            category_name_field.addClass('ui-state-error');
            return;
        }
        $.ajax({
            url: '/manage_categories',
            type: 'POST',
            dataType: 'json',
            data: {'action': 'add', 'id': id, 'name': name, 'color': color},
        })
        .done(function(json) {
            if (json.result == 'success'){
                tab = '<li><a href="#" title="category_' + id + '" class="tab">' +
                '<input type="text" value="' + name + '" class="category_name clear_input" readonly></a>' +
                '<div class="admin-color-pick-edit" style="background-color: ' + color + '"></div>' +
                '<div class="edit_cat" id="' + id + '"><a href="#" class="edit_category">  edit  </a>' +
                '<a href="#" class="drop_category">  delete  </a>  </div>  </li>';
/*=======
                tab = '<li><a href="#" title="category_' + id + '" class="tab"><input type="text" value="' + name + '" class="category_name clear_input" readonly></a><div class="edit_cat" id="' + id + '"><a href="#" class="edit_category">edit</a></br><a href="#" class="drop_category">delete</a></div></li>';
>>>>>>> 6667b6e201670eaf77bac1fa0686a85598417b90*/
                tabs_panel.append(tab);
                category = '<div id="category_' + id + '" class="content" style="display: none"></div>'
                tabed_box.append(category);
                $this.closest('#new').find('select.category_dropdown').find('option:selected').remove();
                $('#add-course-category-selector').append('<option value="' + id +'">' + name + '</option>');
                category_name_field.val('');
            }
            else if (json.result == 'error'){
                nice_alert('error', json.error);
            }
        })
        .fail(function() {
            nice_alert('!', 'some error adding new category. sorry');
        })
        .always(function() {
            
        });
        
    });
});