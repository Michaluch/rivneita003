$(function(){
    $(document).on("click", ".drop_category", function(){
        var $this = $(this)
        var parent_id = $this.closest("div.edit_cat").attr("id")
        // $this.text("  .......  ");
        var c=confirm("please confirm your action");
            if (c==true)
                {
                $.ajax({
                    url: "/manage_categories",
                    data: "action=change_status&id=" + parent_id,
                    type: "PUT",
                    dataType: "json",
                    success: function(json){
                        if (json.result == "success") {
                            if (json.status == 1) {
                                $this.text("  restore  ");
                            }
                            if (json.status == 0) {
                                $this.text("  delete  ");
                            }
                        }
                        if (json.result == "error") {
                                alert(json.error)
                        }
                    },
                    error: function(xhr, status){
                        alert(status)
                    },
                    });
                }
        });
});