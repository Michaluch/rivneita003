$(document).ready(function() {

var flag = 0;
$(document).on('click', '#selectAll', function() {
    if(flag == 0) {
        $(".select_one").each(function() {
            $(this).prop('checked', true);
            flag++;
        });
    }
    else {
        $(".select_one").each(function() {
            $(this).prop('checked', false);
            flag--;
        });
    }
});

$(document).on('click', '#action_ok', function() {
    var action_text = $("#choosed_action>option:selected").html();

    if(action_text == "Choose action") {
        nice_alert("Choose action and press OK");
        return false;
    }
    if (confirm('Are realy want to ' + action_text + ' those users?')) {
        alert("in check")
        //For each TR
        $(".select_one:checked").each(function() {
            //Takes clicked tr (line)
            var tr = $(this).closest('.currentUser');
            //Gets user_id and status_id from td(row) in clicked tr(line)
            var user_id = tr.closest('.currentUser').attr('id');
            var login = tr.closest('.currentUser').find('td').eq(2).html();
            var status_id = tr.closest('.currentUser').find('td').eq(7).html();


            if($(this).closest('.currentUser').find('td').eq(6).html() == 'admin') {
                nice_alert("You can't deactivate or activate user from admin group");
                return false;
            }

            //Rename status_id from string to number, becouse in db we use numbers
            if(action_text == "Activate") {
                //alert("active")
                if(status_id == 'no') {
                    status_id = 2;
                }
                else {
                    var error_login = tr.closest('.currentUser').find('td').eq(2).html();
                    nice_alert("User '" + error_login +"' is alredy deactivated")
                    return true;
                }
            }
            else
                if(action_text == "Deactivate") {
                    //alert("deactive")
                    if(status_id == 'yes') {
                        status_id = 1;
                    }
                    else {
                        nice_alert("This user is alredy activ")
                        return true;
                    }
                }
            else {
                nice_alert("Some error occurred");
                return false;
            }

            changeStatus(user_id, login, status_id, tr);
        });
    }  //End of confirm dialog
});
    
$(document).on('click', '.removeUser', function() {
    //Takes clicked tr (line)
    var tr = $(this).closest('.currentUser');
    //Gets user_id and status_id from td(row) in clicked tr(line)
    var user_id = tr.closest('.currentUser').attr('id');
    var login = tr.closest('.currentUser').find('td').eq(2).html();
    var status_id = tr.closest('.currentUser').find('td').eq(7).html();
    
    if (confirm( 'Are you realy want to ' + $(this).html() + ' this user: ' + login + '?' )) {
        //Rename status_id from string to number, becouse in db we use numbers
        if(status_id == 'yes') {
            status_id = 1;
        }
        else
            if(status_id == 'no') {
                status_id = 2;
            }

        changeStatus(user_id, login, status_id, tr);
    }//End of confirm dialog
});

function changeStatus(user_id, login, status_id, tr) {
    //Send request
    $.ajax({
        type: 'POST',
        //url: "/aza",
        url: '/manage_users',
        data: {'user_id': user_id,
               'login': login,
               'status_id': status_id,
               'action': 'change_user_status'
            },
        cache: false,
        async: false,
        success: function(data, textStatus, jqXHR) {
            if(data.err_admin) {
                nice_alert("Server: " + data.err_admin);
                return false;
            }
            //Convert numbers from db to text
            var new_status = parseInt(data.new_status);

            if(new_status == 1) {
                tr.closest('.currentUser').find('td').eq(7).html('yes');
                tr.closest('.currentUser').find('.removeUser').html('Deactivate');
            }
            else
                if(new_status == 2) {
                    tr.closest('.currentUser').find('td').eq(7).html('no');
                    tr.closest('.currentUser').find('.removeUser').html('Activate');
                }
                else {
                    nice_alert("Something goes wrong");
                }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            nice_alert("Server error", "Sorry for the inconvenience. We working on it");
        }
    });
}
    
});//The end
