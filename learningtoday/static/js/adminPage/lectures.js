$(document).ready(function(){
	
	$('#manage-lectures').click(function() {
        $.ajax({
                type: "GET",
                url: "/lectures",
                cache: false,
                success: function(data, textStatus, jqXHR) {
                    $('#lectures_tab').remove();
                    $('#tabs-5').append(data);
                    $('.tabbed_box_lectures .tab_course').first().click();
                    },
                error: function() {}
            })
    })

    function checkFields(number, title, course_id, lecture_id){
	var errorMessage = '';
    	if (number=="") {
    		errorMessage = errorMessage + "Number can't be empty!"
    	}
    	else {
    		number = parseInt(number);
	    	if (!isNaN(number)){
	    		if (number>127){
	    			errorMessage = errorMessage + "Number must be from 1 to 127"
	    		}
	    		else {
	    			$.ajax({
			            type: "POST",
			            url: "/lectures",
			            async: false,
			            data: {"action":"check_number", "number": number, "course_id": course_id, "lecture_id": lecture_id},
			            cache: false,
			            success: function(data, textStatus, jqXHR) {
			            	if (data.exists==true){
			            		errorMessage = errorMessage + "Numbers can't repeat!";
			            	}
			            },
			            error: function(jqXHR, textStatus, errorThrown) {
			                alert('Error');
			            }
			    	})
	    		}
	    	}
	    	else{
	    		errorMessage = errorMessage + "Number must contain only digits";
	    	}
    		}
    	if (errorMessage!='') {
    		$('.number_edit').addClass("ui-state-error");
    	}
    	if (title==""){
    		errorMessage = errorMessage + " Lecture title can't be empty! "
    		$('.lecture_name_edit').addClass("ui-state-error");
    	}
    return errorMessage;
}

    $(document).on('click', '.edit_lecture', function () {
    	var current_lecture = $(this).closest(".lecture_description");
    	$(".lecture_description").not("#" + current_lecture.attr('id')).css("visibility", "hidden").css("position", "absolute");
    	var thisis = $(this);
    	if ($(this).text().trim()=='edit'){
    		$(this).text('save');
	    	current_lecture.find('.max_lines').css("visibility", "hidden").css("position", "absolute");
	    	current_lecture.find(".lecture_text").css("visibility", "visible").css("position","relative");
	    	var num = current_lecture.find(".number_saved").val().trim();
	    	current_lecture.find(".number_saved").val(num.substring(0, num.length-1));
	    	current_lecture.find(".number_saved").removeClass('number_saved').addClass("number_edit").attr("readonly", false);
	    	current_lecture.find(".lecture_name_saved").removeClass('lecture_name_saved').addClass("lecture_name_edit").attr("readonly", false);
	    	current_lecture.find(".validation").css("position", "relative");
	    	current_lecture.find(".label_lec").each(function(){
	    		$(this).css("visibility","visible").css("position","relative");
	    	})
	    	new nicEditor({fullPanel : true}).panelInstance(current_lecture.find(".lecture_text").attr('id'));
    	}
    	else {
    		if ($(this).text().trim()=='save') {
    			var course_id = $(this).closest(".content").attr('id').split('_')[2];
    			var body = String(current_lecture.find(".nicEdit-main").html());
    			var title = current_lecture.find(".lecture_name_edit").val();
    			var number_in_course = current_lecture.find(".number_edit").val().trim();
    			var lecture_id = thisis.attr('class').split(' ')[1];
    			errorMessage = checkFields(number_in_course, title, course_id, lecture_id)
    			if (errorMessage==''){
	    			$.ajax({
		                type: "POST",
		                url: "/lectures",
		                data: {"id": lecture_id, "number_in_course": number_in_course, "title": title, "body": body, "action": "edit_lecture"},
		                cache: false,
		                success: function(data, textStatus, jqXHR) {
		                	$("#course_lecture_" + course_id).empty();
			                $("#course_lecture_" + course_id).append(data);
		                },
		                error: function(jqXHR, textStatus, errorThrown) {
		                    alert('Error');
		                }
		            });}
				else {
					current_lecture.find(".validation").empty().append(errorMessage).addClass("ui-state-error").css("visibility", "visible").css("position", "relative");
					}
    			
    		}
    	}
    })

	$(document).on('click', '.change_lecture_status', function(){
        var status_id = $(this).text().trim();
        if (status_id=="deactivate"){
            var c=confirm("Do you want to deactivate this lecture?");}
        else {c=true}

        if (c==true) {
        
		var current_lecture = $(this).closest(".lecture_description");
		var thisis = $(this);
		var id = current_lecture.find(".edit_lecture").attr('class').split(' ')[1];
        var status_id = $(this).text().trim();
        if(status_id == 'activate') {
            status_id = 1;
        }
        else 
            if(status_id == 'deactivate') {
                status_id = 0;
            }
        $.ajax({
            type: "POST",
            url: "/lectures",
            data: {"deleted": status_id, "action":"edit_status", "id": id },
            cache: false,
            success: function(data, textStatus, jqXHR) {
                if(data.new_status == '1') {
                    thisis.text("activate")
                }
                else
                    if(data.new_status == '0') {
                        thisis.text("deactivate")
                    }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error');
            }
        });}
	})

    $(document).on('click', '#add_lecture', function(){
    	
    	$.ajax({
            type: "POST",
            url: "/lectures",
            data: {"action":"get_add_form"},
            cache: false,
            success: function(data, textStatus, jqXHR) {
            	
                $("#" + $("#lectures_tab").find(".active").attr('title')).empty().append(data);
                new nicEditor({fullPanel : true}).panelInstance($("#lecture_area").attr('id'));     
                $(".label_lec").each(function(){
	    		$(this).css("visibility","visible").css("position","relative");

	    	})      
                errorMessage = '';
	    		$(".validation").val(errorMessage).css("position", "relative");
	    		$(".number_edit").attr("readonly", false);
	    		$(".lecture_name_edit").attr("readonly", false);
	    		$(".tabbed_box_lectures .content").css("margin-right","85px");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error');
            }
        });

    })



    $(document).on('click', '#save', function(){
    	$('.number_edit').removeClass("ui-state-error");
		$('.lecture_name_edit').removeClass("ui-state-error");
    	var number = $('.number_edit').val().trim();
    	var course_id = $('#select_courses').find(":selected").val();
    	var body = String($(".nicEdit-main").html());
		var title = $(".lecture_name_edit").val();
		var deleted = $("#radio_buttons input[type='radio']:checked").val();
    	errorMessage = checkFields(number, title, course_id, 0);
    	if (errorMessage==''){
			if (body=="<br>"){
    			var c=confirm("Do you want to save lecture with empty body in course?");}
    		else {c=true}
    		if (c==true){
    			$.ajax({
	                type: "POST",
	                url: "/lectures",
	                data: {"course_id": course_id, "number_in_course": number, "title": title,
	                 "body": body, "action": "add_lecture", "deleted": deleted},
	                cache: false,
	                success: function(data, textStatus, jqXHR) {
	                	$("#" + $("#lectures_tab").find(".active").attr('title')).empty()
	                	$("#course_lecture_" + course_id).empty();
		                $("#course_lecture_" + course_id).append(data);
		                //$("#lectures_tab").find(".active").removeClass('active');
		                $(".tab_course").each(function(){
		                	if ($(this).attr('title').split("_")[2]==course_id){
		                		$(this).click();
		                	}
		                })
    					$(".tabbed_box_lectures .content").css("margin-right","0");
	                },
	                error: function(jqXHR, textStatus, errorThrown) {
	                    alert('Error');
	                }
	            });
        	}
		}
	    else {
	    	$(".validation").empty().append(errorMessage).addClass("ui-state-highlight").css("visibility", "visible").css("position", "relative");
	    	
	    }
	   })

	$(document).on("click", ".tabbed_box_lectures .tab_course", function(){
		$(".tabbed_box_lectures .content").css("margin-right","0");
		course_id = $(this).attr('title').split("_")[2];
		$.ajax({
            type: "POST",
            url: "/lectures",
            data: {"course_id": course_id, "action": "get_lectures"},
            cache: false,
            success: function(data, textStatus, jqXHR) {
            	$("#course_lecture_" + course_id).empty();
                $("#course_lecture_" + course_id).append(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
		                    alert('Error');
		                }
		            });
	})

	$(document).on("click", "#button_lecture_search", function(){
		var course_id = $("#lectures_tab").find(".active").attr('title').split("_")[2];
		var search_key = $('#search_lecture_key').find(":selected").val();
        var search_value;
        if (search_key=='deleted') {
            search_value = $('#drop_down_deleted').find(":selected").val();
        }
        else {
            search_value = $('#search_lecture').val();
            }
        $.ajax({
	            type: "POST",
	            url: "/lectures",
	            data: {"search_key": search_key, "action": 'search', 'search_value': search_value, "course_id": course_id},
	            cache: false,
	            success: function(data, textStatus, jqXHR) {
	            	$("#course_lecture_" + course_id).empty();
			        $("#course_lecture_" + course_id).append(data);    
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	                alert('Error');
	            }
        	});
               
	})

	$(document).on('change', '#search_lecture_key', function() {
            if ($('#search_lecture_key').find(":selected").val()=='deleted') {
            	$('#select_lecture_wrapper').empty();
                    dropDownActive = '<select name="drop_down_deleted" id="drop_down_deleted" class="drop1">'+
                                    '<option value="0">Activated</option>'+
                                    '<option value="1">Deactivated</option>'+
                                              '</select>';
                    $('#select_lecture_wrapper').append(dropDownActive);}
            else {
            	$('#select_lecture_wrapper').empty();
                    dropDownActive = '<input type="text" name="search_value" id="search_lecture">';
                    $('#select_lecture_wrapper').append(dropDownActive);}
            

                })

	$(document).on('keypress', '#search_lecture', function(event){
        if (event.keyCode == 13) {
            $('#button_lecture_search').click();
    }
    })

})