var flag = 0;

$(document).on("click", ".edit_category", function(){
    var $this = $(this);
    var closest_parent = $this.closest("li");
    var parent_id = $this.closest("div.edit_cat").attr("id");
    var color_picker = closest_parent.find('.admin-color-pick-edit');
    var color = rgb2hex(color_picker.css( "background-color" ));
    var name = closest_parent.find(".category_name");
    
    if(flag == 0) {
        color_picker.addClass('clickable');
        closest_parent.find(".category_name").removeClass("clear_input");
        closest_parent.find(".category_name").attr('readonly', false);
        closest_parent.find(".category_name").focus();
        $this.text("save");
        flag++;
    }
    else
        if(flag == 1){
            color_picker.removeClass('clickable');
            if (! check_regexp_simple(name, /^[a-zA-Z][a-zA-Z0-9- \.,;:\(\)\?!"']+$/) || ! check_length_simple(name, 2, 40) ){
                        name.addClass("ui-state-error");
                        name.focus();
                        return;
                    }
            $.ajax({
                type: "PUT",
                url: "/manage_categories",
                data: { cat_id: parent_id, name: name.val(), color: color,  action: "edit"},
                cache: false,
                success: function(data, textStatus, jqXHR){
                    $(name).val(data.name);
                },
                error: function(xhr, status){
                    alert('ERROR')
                },
            });
        closest_parent.find(".category_name").removeClass("ui-state-error");
        closest_parent.find(".category_name").addClass("clear_input");
        closest_parent.find(".category_name").attr('readonly', true);
        $this.text("edit");
        flag = 0;
        }

});

