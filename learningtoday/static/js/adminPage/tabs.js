
        // When the document loads do everything inside here ...  
$(function(){  
              
            // When a link is clicked  
    $(document).on('click', 'a.tab', function () {  
                  
                // switch all tabs off  
        $(".active").removeClass("active");  
                  
                // switch this tab on  
        $(this).addClass("active");  
                  
                // slide all elements with the class 'content' up  
        $(".content").slideUp();
                // Now figure out what the 'title' attribute value is and find the element with that id.  Then slide that down.  
        var content_show = $(this).attr("title");  
        $("#"+ content_show).slideDown();
                
    });  

    $(document).on('click', 'a.tab_course', function () {  
                  
                // switch all tabs off  
        $(".tabs_courses .active").removeClass("active");  
                  
                // switch this tab on  
        $(this).addClass("active");  
                  
                // slide all elements with the class 'content' up  
        $(".tabbed_box_lectures .content").slideUp();
                // Now figure out what the 'title' attribute value is and find the element with that id.  Then slide that down.  
        var content_show = $(this).attr("title");  
        $("#"+ content_show).slideDown();
                
    }); 
          
});  


$(document).ready( function() {
    $("#tabs").tabs();
});

