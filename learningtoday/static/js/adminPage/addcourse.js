$(document).ready(function() {
    var tips = $("#course_validate_tips"),
    name = $("#form-add-course input[name=name]"),
    description = $("#form-add-course textarea[name=description]"),
    image = $("#form-add-course input[name=image]"),
    fdialog = $("#courses-dialog-add"),
    form = $("#form-add-course"),
    form_inputs = $("#form-add-course input")

    $("#v-nav ul li a").on("click", function() {
        //This function sets selected course in add course select
        var category_name = $(this).attr('title');
        var buf = category_name.split('_');
        //category_name = parseInt(buf[1], 10) // 1-string, 2-what type of number

        $("#add-course-category-selector").val(buf[1]);
    });

    fdialog.dialog({
      autoOpen: false,
      height: 530,
      width: 370,
      resizable: false,
      dragable: false,
      modal: true,
      buttons: {"Create course":{
          text:"Create course",
          id:"pass-ajax-add",
          click:function() {

          valid = true;
          remove_class(form_inputs, "ui-state-error");
          remove_class(description, "ui-state-error");

          clear_dialog_form(tips, null);
          
          valid = valid && checkLength(name, "name", 3, 120, tips)
          valid = valid && checkLength(description, "description", 6, 400, tips)
          
          valid = valid && checkRegexp(name, /^[a-zA-Z][a-zA-Z0-9- \.,;:\(\)\?!"']+$/, "Name field only allow : A-Z a-z 0-9 - . : ; ' \" ? ! ( )", tips)
          valid = valid && checkRegexp(description, /^[a-zA-Z][a-zA-Z0-9- \.,;:\(\)\?!"']+$/, "Description field only allow : A-Z a-z 0-9 - . : ; ' \" ? ! ( )", tips)

          if (valid) {
            $.ajax({
            url: "/manage_courses",
            data: "action=add&" + form.serialize(),
            type: "POST",
            dataType: "json",
            success: function(json){
                if (json.result == "success") {
//                    alert(json.result)
                    updateTips( "course added" , tips);
                    div = '<div class="course_description" id="course_' + json.course.id + '">';
                    div += '<div class="course_image"><img class="avatars" id="avatar' + json.course.id+'" src="/static/img/courses_img/"> </div>';
                    div += '<div class="course_header">';
                    div += '<p class="course_name">' + json.course.name + '</p>';
                    div += '<p class="course_author">by: ' + json.course.author + '</p>';
                    div += '<p class="course_text">' + json.course.description + '</p></div>';
                    div += '<div class="right course_right">';
                    div += '<a href="#" class="edit_course">edit</a>';
                    div += '<a href="#" class="change_course_status">  deactivate  </a></div></div>';

                    /*var row = ("<tr id='course." + json.course.id + "'>");
                    row += "<td class='category_id' style='visibility: collapse'>" + json.course.category_id + "</td>";
                    row += "<td class='category'>" + json.course.category + "</td>";
                    row += "<td class='author'>" + json.course.author + "</td>";
                    row += "<td class='name'>" + json.course.name + "</td>";
                    row += "<td class='description'>" + json.course.description + "</td>";
                    if (!json.course.image) { image = "no"; }
                    else {image = json.course.image; }
                    row += "<td class='image'>" + image + "</td>";
                    row += "<td><button class='edit_course'>Edit</button></td>";
                    row += "<td><button class='change_course_status'>Deactivate</button></td>"
                    row += "</tr>";*/
                    $("#category_" + json.course.category_id).append(div);
                    $('.avatars').addClass('photo-param');
                    }
                if (json.result == "error") {
                    updateTips( json.error , tips)
                }
                
            },
            error: function( xhr, status ) {
                updateTips( status , tips)
            }
            });
          }
        }},
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        clear_dialog_form(tips, form_inputs);
        remove_class(form_inputs, "ui-state-error");
        remove_class(description, "ui-state-error");
      }
    });

    $( "#add_course" )
      .button()
      .click(function() {
        fdialog.dialog( "open" );
    });


});