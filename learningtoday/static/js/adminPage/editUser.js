$(document).ready(function() {
        
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }

    function checkLength( o, n, min, max ) {
        if ( o.val().length > max || o.val().length < min ) {
                o.addClass( "ui-state-error" );
                updateTips( "Length of " + n + " must be between " +
                min + " and " + max + "." );
                return false;
            } else {
                return true;
        }
    }

    function checkRegexp( o, regexp, n ) {
        if ( !( regexp.test( o.val() ) ) ) {
                o.addClass( "ui-state-error" );
                updateTips( n );
              return false;
            } else {
                return true;
        }
    }

    var login = $('#username_edit'),
        f_name = $('#fName_edit'),
        l_name = $('#lName_edit'),
        email = $('#email_edit'),
        tips = $('#validateTips_edit'),
        allFields = $( [] ).add(login).add(f_name).add(l_name).add(email).add(tips);

    var tr, login_info;
    $(document).on('click', '.editUser', function() {
        //Gets clicked tr(line)
        tr = $(this).closest('.currentUser');
        //Gets user_id from td(row) in clicked tr(line)
        login_info = $(this).closest('.currentUser').find('td').eq(2).html();

        //Send request to get current user parameters
        $.ajax({
            type: "GET",
            //url: "/aza",
            url: "/manage_users",
            data: {"login": login_info, 'action': 'get_user_info'},
            cache: false,
            async: false,
            success: function(data, textStatus, jqXHR) {
                console.log(data);
                //Sets response values in fields
                login.val(data.login);
                f_name.val(data.f_name);
                l_name.val(data.l_name);
                email.val(data.email);
                $('#selectedRole').val(data.role_id);
                //Open the dialog
                $('#dialog-form-edit').dialog('open');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                nice_alert("Server error", "Sorry for the inconvenience. We working on it")
            }
        });
    })

    $('#dialog-form-edit').dialog({
        autoOpen: false,
        height: 560,
        width: 350,
        modal: true,
        resizable: false,
        dragable: false,
        buttons: {'Save changes': function() {
            //Create valid boolean
            var bValid = true;
            //Remove all error classes(red field, messages, etc.)
            allFields.removeClass('ui-state-error');

            bValid = bValid && checkLength( login, 'username', 4, 16 );
            bValid = bValid && checkRegexp( login, /^[a-z]([0-9a-z_])+$/i,
                'Username may consist of a-z, 0-9, underscores, begin with a letter.' );

            bValid = bValid && checkLength( f_name, 'fName', 2, 32 );
            bValid = bValid && checkRegexp( f_name, /^([a-zA-Z])+$/,
                'First name field only allow : a-z' );

            bValid = bValid && checkLength( l_name, 'lName', 2, 32 );
            bValid = bValid && checkRegexp( l_name, /^([a-zA-Z])+$/,
                'Last name field only allow : a-z' );

            bValid = bValid && checkLength( email, 'email', 6, 80 );
            // From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
            bValid = bValid && checkRegexp( email,
                /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, 'eg. ui@jquery.com');

            //Gets user role and convert in number(for db)
            var role_id = $("option:selected", $("#selectedRole")).val();

            //If all fields are valid
            if ( bValid ) {
                /*console.log(login.val())
                console.log(f_name.val())
                console.log(l_name.val())
                console.log(email.val())
                console.log(role_id)*/
                //Sends request with new params
                $.ajax({
                    type: 'POST',
                    url: '/manage_users',
                    data: {'id': tr.attr('id'),
                           'login': login.val(), 
                           'f_name': f_name.val(),
                           'l_name': l_name.val(), 
                           'email': email.val(),
                           'role_id': role_id,
                           'action': 'edit'
                        },
                    cache: false,
                    async: false,
                    success: function(data, textStatus, jqXHR) {
                        console.log(data)
                        //Take response value with boolean, if some params are changed
                        if(data.messages) {
                            nice_alert(data.message);
                            return false;
                        }
                        if(!data.isChanged) {
                            return false;
                        }
                        //Set new params
                        tr.find('td').eq(2).html(data.details.login);
                        tr.find('td').eq(3).html(data.details.f_name);
                        tr.find('td').eq(4).html(data.details.l_name);
                        tr.find('td').eq(5).html(data.details.email);
                        var new_role = data.role_id;
                        switch(new_role) {
                            case 1:
                                new_role = 'admin';
                                break;
                            case 2: 
                                new_role = 'teacher';
                                break;
                            case 3:
                                new_role = 'user';
                                break;
                        }
                        tr.find('td').eq(6).html(new_role);
                        $('#dialog-form-edit').dialog('close');
                        //Close the dialog
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        nice_alert("Server error", "Sorry for the inconvenience. We working on it")
                    }
                });
            }  //End bValid if statement
        }  //End save button
        },  //close save button
        Cancel: function() {
            //Close dialog and remove all error classes(red inputs, messages, etc) if button 'cancel' was pressed
            $(this).dialog('close');
            allFields.val( '' ).removeClass('ui-state-error');
        },
        close: function() {
            //Remove all error classes(red inputs, messages, etc) if button 'close' was pressed
            allFields.val( '' ).removeClass('ui-state-error');
        }
    });
});
