//converting rgb color to hex
var hexDigits = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 
function rgb2hex(rgb) {
 rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
 return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}
function hex(x) {
  return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
 }

//initialising color picker
var cp = colorPicker;

//oncklick function to pick color
$(document).on('click', '.admin-color-pick-add, .admin-color-pick-edit', function(event) {
  $this = $(this)
  if ($this.hasClass('clickable')) {
    cp.size = 2;
    cp.offsetY = -$this.height() - 64;
    cp.offsetX = $this.width() + 1;
    cp.allowDrag = false;
    cp.allowResize = false;
    cp.mode = 'H';
    cp.difPad = 20;
    var background = $this.css('background-color');
    cp(event);
    cp.cP.style.zIndex = 999;
    cp.importColor(background);
  }
});

function pick_color(obj, height, width) {

}

function updateTips( t , tips) {
  tips
    .text( t )
    .addClass( "ui-state-highlight" );
  setTimeout(function() {
    tips.removeClass( "ui-state-highlight", 1500 );
  }, 500 );
}

function checkLength( o, n, min, max , tips) {
  if ( o.val().length > max || o.val().length < min ) {
    o.addClass( "ui-state-error" );
    updateTips( "Length of " + n + " must be between " +
      min + " and " + max + "." , tips);
    return false;
  } else {
    return true;
  }
}

function isSame(obj1, obj2, tips) {
  if(obj2.val() != obj1.val())
  {
    obj2.addClass( "ui-state-error" );
    updateTips("Passwords are not match", tips);
    return false;
  }
  else
  {
    return true;
  }
}

function checkRegexp( o, regexp, n , tips) {
  if ( !( regexp.test( o.val() ) ) ) {
    o.addClass( "ui-state-error" );
    updateTips( n , tips);
    return false;
  } else {
    return true;
  }
}

function remove_class(o, eclass){
    o.removeClass(eclass);
}

function clear_dialog_form(tips, o) {
    if (o != null) {o.val("");}
    tips.text("");
}

function check_length_simple( o, min, max ) {
  if ( o.val().length > max || o.val().length < min ) {
    o.addClass( "ui-state-error" );
    return false;
  } else {
    return true;
  }
}

function check_regexp_simple( o, regexp ) {
  if ( !( regexp.test( o.val() ) ) ) {
    o.addClass( "ui-state-error" );
    return false;
  } else {
    return true;
  }
}