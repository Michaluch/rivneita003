$(document).ready(function() {
    $(document).on("click", ".change_course_status", function(){
        var $this = $(this)
        var parent_id = $this.closest("div.course_description").attr("id")
        // $this.text("  .......  ");
        var c=confirm("please confirm your action");
            if (c==true)
                {
                $.ajax({
                    url: "/manage_courses",
                    data: "action=change_status&id=" + parent_id.split("_")[1],
                    type: "PUT",
                    dataType: "json",
                    success: function(json){
                        if (json.result == "success") {
                            if (json.status == 1) {
                                $this.text("activate");
                            }
                            if (json.status == 0) {
                                $this.text("  deactivate  ");
                            }
                        }
                        if (json.result == "error") {
                                alert(json.error)
                        }
                    },
                    error: function(xhr, status){
                        alert(status)
                    },
                    });
                }
        });
});