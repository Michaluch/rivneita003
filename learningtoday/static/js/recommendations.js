$(document).ready(function() {
    //on load preparations
    var selected_course_id;
    var books_table_checked = [];
    var recommendation_table = $('#recommendation table');
    var books_table = $('#books_table');
    var select_book_level = $('#select_book_level');
    var selected_level;
    set_checkboxes();

    function set_checkboxes() {
        var first_level = recommendation_table.find('tr:first');
        if (first_level.attr('id') != undefined) {
            var first_level_id = first_level.attr('id').split('_')[1];
            select_book_level.find('option[value=' + first_level_id + ']').prop('selected', true);
            var nested_table = first_level.next('tr').find('td table');
            nested_table.find('tr').each(function() {
                $this = $(this);
                var book_id = $this.attr('id').split('_')[2];
                $('#book_' + book_id).find('td input').prop('checked', true);
            });
        }
    }   

    function clear_checkboxes() {
        books_table.find('tr td input').prop('checked', false);
    }

    function get_checked_books() {
        $('#books_table').find('tr').each(function(index, el) {
            $this = $(this);
            if ($this.find('td input').prop('checked')) {
                var selected_book_title = $this.find('.title').html();
                var selected_book_author = $this.find('.author').html();
                books_table_checked.push([$this.attr('id').split('_')[1], selected_book_title, selected_book_author]);
        }
        });
    }

    function generate_recommend_books_rows(){
        table_rows = '';
        for (var i = 0; i < books_table_checked.length; i++) {
            var book = books_table_checked[i];
            table_rows += '<tr id="recommend_book_' + book[0] + '" class="books_in_level">';
            table_rows += '<td class="recommend_book_title">' + book[1] + ' (' + book[2] + ')</td></tr>';
        }
        return table_rows;
    }

    function update_level(level_obj) {
        var nested_table = level_obj.next('tr').find('td table');
        nested_table.empty();
        nested_table.append(generate_recommend_books_rows());
    }

    function add_level(selected_level, level_obj, place) {
        level_obj = level_obj || undefined;
        var level = '<tr id="level_' + selected_level + '" class="level">';
        level += '<td class="level">Level: ' + selected_level + ' </td></tr>';
        level += '<tr><td td class="table-container"><table cellpadding="6" cellspacing="0">';
        level += generate_recommend_books_rows();
        level += '</table></td></tr>';
        if (place == 1) {
            level_obj.before(level);
        }
        if (place == 2) {
            var level_obj_books_table = level_obj.next('tr'); 
            level_obj_books_table.after(level);
        }
        if (place == 0) {
            $('#recommendation table').append(level);
        }
    }

    function add_update(course_level, book_level, update_add, obj, flag) {
        $.ajax({
            url: '/recommendations',
            type: 'PUT',
            dataType: 'json',
            data: {'action': 'update', 'course_id': course_level, 'books_levels': JSON.stringify(book_level)},
        })
        .done(function(json) {
            if (json.result == 'success') {
                if (update_add == 'update'){
                    update_level(obj);
                    books_table_checked = [];
                }
                else if (update_add == 'add') {
                    add_level(selected_level, obj, flag);
                    books_table_checked = []; 
                }
            }
            else if (json.result == 'error') {
                nice_alert('error', json.error);
                books_table_checked = [];
            }
        })
        .fail(function() {
            nice_alert('!', 'fail');
            books_table_checked = [];
        });
    }

    function sort_obj(arr){
        // Setup Arrays
        var sorted_keys = [];
        var sorted_obj = {};

        // Separate keys and sort them
        for (var i in arr){
            sorted_keys.push(i);
        }
        sorted_keys.sort();

        // Reconstruct sorted obj based on keys
        for (var i in sorted_keys){
            sorted_obj[sorted_keys[i]] = arr[sorted_keys[i]];
        }
        return sorted_obj;
    }

    select_book_level.change(function(event) {
        clear_checkboxes();
        $this = $(this);
        var selected_level = $this.find('option:selected').val();
        var recommendation_level = $('#level_' + selected_level);
        if (recommendation_level.length > 0) {
            var nested_table = recommendation_level.next('tr').find('td table');
            nested_table.find('tr').each(function() {
                $this = $(this);
                var book_id = $this.attr('id').split('_')[2];
                $('#book_' + book_id).find('td input').prop('checked', true);
            });
        }
    });


    $(document).on('click', '#clear_all', function(event) {
        clear_checkboxes();
    });

    $(document).on('click', '#commit_changes', function(event) {
        // creating recommendations map
        get_checked_books();
        var first_iter = true;
        var selected_value_inserted = false;
        var prev, next;
        selected_level = parseInt(select_book_level.find('option:selected').val());
        selected_course_id = $('#select_user_course').find('option:selected').val();
        var levels_in_recommendation = recommendation_table.find('tr.level').length;
        function process_commit_add_update(update_add, obj, flag){
            var books = [];
            for (var i = 0; i < books_table_checked.length; i++) {
                var book = books_table_checked[i];
                books.push(book[0]);
            }
            var book_level = {};
            book_level[selected_level] = books;
            add_update(selected_course_id, book_level, update_add, obj, flag);
            selected_value_inserted = true;
        }
        if (!books_table_checked.length){
            var level_to_remove = $('#level_' + selected_level);
            var level_to_remove_id = level_to_remove.attr('id').split('_')[1];
            $.ajax({
            url: '/recommendations',
            type: 'DELETE',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({'course_id': selected_course_id, 'book_level': level_to_remove_id}),
            })
            .done(function(json) {
                if (json.result == 'success') {
                    level_to_remove.next('tr').remove();
                    level_to_remove.remove();
                }
                else if (json.result == 'error') {
                    nice_alert('error', json.error);
                }
            })
            .fail(function() {
                nice_alert('!', 'fail');
            });
            
        }else{
            if (recommendation_table.find('tr.level').length > 0) {
                recommendation_table.find('tr.level').each(function(index) {
                    $this = $(this);
                    var this_level = $this.attr('id');
                    var this_level_id = this_level.split('_')[1];
                    if (first_iter) {
                        next = -1;
                        first_iter = false;
                    }
                    prev = next;
                    next = parseInt(this_level_id);
                    
                    if (selected_level == next && !selected_value_inserted) {
                        process_commit_add_update('update', $this);
                        
                    }
                    else if (next > selected_level && prev < selected_level && !selected_value_inserted) {
                        process_commit_add_update('add', $this, 1);
                    }
                    if((levels_in_recommendation - 1 == index) && (selected_level > next) && !selected_value_inserted) {
                        process_commit_add_update('add', $this, 2);
                    }
                });
            }else {
                process_commit_add_update('add', undefined, 0);
            }
        }      
        
    });
    

    $('#select_user_course').change(function(event) {
        $this = $(this);
        var selected_course = $this.find('option:selected').val();
        $.ajax({
            url: '/recommendations',
            type: 'GET',
            dataType: 'json',
            data: {action: 'select_course', course_id: selected_course},
        })
        .done(function(json) {
            if (json.result == 'success') {
                books_table.empty();
                recommendation_table.empty();
                books_table.append('<tr><th>Title</th><th>Author</th><th>Select</th></tr>');
                for (var i = 0; i < json.books.length; i++) {
                    var book = json.books[i];
                    var table_row = '<tr id="book_' + book.id + '"><td class="title">' + book.title + '</td>';
                    table_row += '<td class="author">' + book.author + '</td><td class="options"><input type="checkbox" /></td></tr>';
                    books_table.append(table_row);
                }

                var recommendation = sort_obj(json.recommendation);
                recommendation_table.append('<caption>Recommendation:</caption>')
                for (level in recommendation) {
                    var table_level = '<tr id="level_' + level + '" class="level">';
                        table_level += '<td class="level">Level: ' + level + ' </td></tr>';
                        table_level += '<tr><td class="table-container"><table cellpadding="6" cellspacing="0">';
                        for (var i = 0; i < recommendation[level].length; i++){
                            var book = recommendation[level][i];
                            var table_row = '<tr id="recommend_book_' + book[0] + '" class="books_in_level">';
                                table_row += '<td class="recommend_book_title">' + book[1] + ' (' + book[2] + ')</td></tr>';
                            table_level += table_row;
                        }
                        table_level += '</table></td></tr>';
                        recommendation_table.append(table_level);
                }
                recommendation_table = $('#recommendation table');
                books_table = $('#books_table');
                set_checkboxes();

                }
                else if (json.result == 'error') {
                    nice_alert('error', json.error);
                }
        })
        .fail(function() {
            nice_alert('!', 'fail');
        });
        
    });
    

});