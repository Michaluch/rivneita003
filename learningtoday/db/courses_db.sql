
SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(80) NOT NULL,
  `color` varchar(7) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

-- ----------------------------
--  Records of `category`
-- ----------------------------
BEGIN;
INSERT INTO `category` VALUES ('1', 'Python Pro', '#777888', '0'), 
('2', 'MySQL', '#999ddd', '0'), 
('3', 'Rails', '#995ddd', '0'), 
('5', 'Django', '#919ddd', '0'), 
('6', 'JavaScript', '#939ddd', '0');
COMMIT;

-- ----------------------------
--  Table structure for `comments`
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `lecture_id` int(10) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `title` varchar(70) NOT NULL,
  `body` varchar(500) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

INSERT INTO `comments` (`id`, `user_id`, `lecture_id`, `creation_date`, `title`, `body`, `active`) VALUES
(1, 4, 1, '2012-01-01 00:00:00', 'comment title 1', 'comment body 1', 1),
(2, 5, 2, '2012-02-01 00:00:00', 'comment title 2', 'comment body 2', 1),
(3, 6, 3, '2012-03-01 00:00:00', 'comment title 3', 'comment body 3', 1),
(4, 7, 4, '2012-04-01 00:00:00', 'comment title 4', 'comment body 4', 0),
(5, 4, 1, '2012-05-01 00:00:00', 'comment title 5', 'comment body 5', 1),
(6, 5, 2, '2012-06-01 00:00:00', 'comment title 6', 'comment body 6', 0),
(7, 6, 3, '2012-07-01 00:00:00', 'comment title 7', 'comment body 7', 0),
(8, 7, 4, '2012-08-01 00:00:00', 'comment title 8', 'comment body 8', 0),
(50, 7, 2, '2013-07-09 17:32:41', ' Aenean imperdiet.', ' Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.', 1),
(10, 13, 1, '2013-04-10 20:43:23', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.', 1),
(11, 9, 3, '2013-09-03 20:44:26', 'Aenean massa.', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 1),
(12, 6, 3, '2013-06-12 20:45:34', 'Donec quam felis', 'ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.', 1),
(13, 11, 2, '2013-10-10 20:46:14', 'Donec pede justo', ' fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.', 1),
(49, 8, 1, '2013-05-09 08:32:06', 'Aliquam lorem ante', 'dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.', 1),
(47, 10, 3, '2013-08-21 01:29:35', 'Nullam dictum felis', 'eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.', 1),
(48, 6, 2, '2013-10-09 10:31:03', 'Aenean vulputate eleifend tellus. ', ' Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.', 1),
(51, 3, 1, '2013-10-01 20:33:25', 'Nam eget dui. Etiam rhoncus.', 'Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.', 1);
-- ----------------------------
--  Table structure for `courses`
-- ----------------------------
DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `name` varchar(120) NOT NULL,
  `description` varchar(400) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ascii;

-- ----------------------------
--  Records of `courses`
-- ----------------------------
BEGIN;
INSERT INTO `courses` VALUES ('1', '1', '1', 'PyPro', 'Nullam id dolor id nibh ultricies vehicula ut id elit.', 'Nullam id dolor id nibh ultricies vehicula ut id elit.', '0'), ('2', '2', '1', 'MySQL for Dummies', 'Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id elit non mi porta gravida at eget metus. Etiam porta sem malesuada magna mollis euismod. Vestibulum id ligula porta felis euismod semper.', '', '1'), ('3', '1', '1', 'Py for Puppies', 'Wow-wow', null, '0');
COMMIT;

-- ----------------------------
--  Table structure for `lecture_books`
-- ----------------------------
DROP TABLE IF EXISTS `lecture_books`;
CREATE TABLE `lecture_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(10) unsigned NOT NULL,
  `lecture_id` int(10) unsigned NOT NULL,
  `test_progress` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

-- ----------------------------
--  Table structure for `lectures`
-- ----------------------------
DROP TABLE IF EXISTS `lectures`;
CREATE TABLE `lectures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `test_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(200) NOT NULL,
  `body` varchar(10000) DEFAULT NULL,
  `raite` tinyint(3) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=ascii;

-- ----------------------------
--  Records of `lectures`
-- ----------------------------
BEGIN;
INSERT INTO `lectures` VALUES ('1', '1', '1', 'Lecture 1', 'Curabitur blandit tempus porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam quis risus eget urna mollis ornare vel eu leo.', null, '0'), ('2', '1', '1', 'Lecture 2', 'Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas sed diam eget risus varius blandit sit amet non magna.', null, '0'), ('3', '1', '1', 'Lecutre 3', 'Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis euismod semper. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.', null, '0'), (4, 1, 1, "Course 1 lecture 1", "Long long boring lecture 1 at course 1", 0, 1),
                   (5, 1, 2, "Course 1 lecture 2", "Long long boring lecture 2 at course 1", 0, 0),
                   (6, 2, 3, "Course 2 lecture 1", "Long long boring lecture 1 at course 2", 0, 0),
                   (7, 2, 4, "Course 2 lecture 2", "Long long boring lecture 2 at course 2", 0, 0);
COMMIT;

-- ----------------------------
--  Table structure for `progress`
-- ----------------------------
DROP TABLE IF EXISTS `progress`;
CREATE TABLE  `progress` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lecture_id` int(10) unsigned NOT NULL,
  `score_id` int(10) unsigned NOT NULL,
  `passtest` tinyint(3) unsigned NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `finalexam` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=ascii AUTO_INCREMENT=5 ;

--
-- Dumping data for table `progress`
--

INSERT INTO `progress` (`id`, `lecture_id`, `score_id`, `passtest`, `attempts`, `finalexam`) VALUES
(1, 1, 1, 10, 1, 0),
(2, 2, 2, 30, 2, 0),
(3, 3, 3, 10, 1, 1),
(4, 4, 4, 90, 13, 0);
-- ----------------------------
--  Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=ascii;

-- ----------------------------
--  Records of `roles`
-- ----------------------------
BEGIN;
INSERT INTO `roles` (`id`, `name`) VALUES (1, "Admin"), (2, "Teacher"), (3, "Student");
COMMIT;

-- ----------------------------
--  Table structure for `score`
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=ascii AUTO_INCREMENT=5 ;

--
-- Dumping data for table `score`
--

INSERT INTO `score` (`id`, `course_id`, `user_id`) VALUES
(1, 1, 4),
(2, 2, 5),
(3, 1, 6),
(4, 2, 7);
-- ----------------------------
--  Table structure for `status`
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=ascii;

-- ----------------------------
--  Records of `status`
-- ----------------------------
BEGIN;
INSERT INTO `status` VALUES (1, "Active"),
                 (2, "Deleted"),
                 (3, "Not confirmed");
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `f_name` varchar(50) DEFAULT NULL,
  `l_name` varchar(50) DEFAULT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(20) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `groups` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=ascii;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', '1', 'xxx', 'xxx', 'admin', '8cb2237d0679ca88db6464eac60da96345513964', 'xxx@x.com', null, '1', null), ('2', '1', 'User', 'user', 'user', '8cb2237d0679ca88db6464eac60da96345513964', 'user@user.com', null, '1', null), ('3', '1', 'Mister', 'joe', 'joes', '8cb2237d0679ca88db6464eac60da96345513964', 'joe@joe.joe', null, '2', null), ('4', '1', 'Mike', 'Deer', 'mike', '8cb2237d0679ca88db6464eac60da96345513964', 'm@d.co', null, '2', null), ('5', '1', 'John', 'Wayne', 'john', '8cb2237d0679ca88db6464eac60da96345513964', 'h@j.co', null, '2', null), ('6', '1', 'Tony', 'Marko', 'tony', '8cb2237d0679ca88db6464eac60da96345513964', 't@m.ko', null, '2', null), ('7', '1', 'Master', 'Zen', 'kane', '8cb2237d0679ca88db6464eac60da96345513964', 'ka@m.a', null, '2', null), ('8', '1', 'Doon', 'Moon', '1ba', '8cb2237d0679ca88db6464eac60da96345513964', 'asd@asd.c', null, '2', null), ('9', '1', 'Danny', 'Howard', 'coolio', '5cec175b165e3d5e62c9e13ce848ef6feac81bff', 'danny@howard.com', null, '2', null), ('10', '1', 'Tim', 'Berg', 'tberg', '5cec175b165e3d5e62c9e13ce848ef6feac81bff', 't@berg.com', null, '1', null), ('11', '1', 'Mark', 'Dest', 'markk', '5cec175b165e3d5e62c9e13ce848ef6feac81bff', 'ma@d.c', null, '1', null), ('12', '1', 'Bob', 'Mob', 'bmob', '8cb2237d0679ca88db6464eac60da96345513964', 'bo@mo.com', null, '1', null), ('13', '1', 'coloo', 'elcolorita', 'colorita', '5cec175b165e3d5e62c9e13ce848ef6feac81bff', 'aasdas@dasd.com', null, '2', null), ('14', '1', 'ZZZZZ', 'asjkdhjkas', 'YYYYYY', '5cec175b165e3d5e62c9e13ce848ef6feac81bff', 'asdasd@ooooo.p', null, '2', null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
