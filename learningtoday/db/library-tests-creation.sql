create database if not exists library_db
  default character set ascii collate ascii_general_ci;

use library_db;

set foreign_key_checks = 0;

create table if not exists category(
  id int unsigned auto_increment,
  name varchar(80) not null,
  deleted tinyint(1) unsigned not null,
  primary key (id)
);

create table if not exists books_authors(
  id int unsigned auto_increment,
  name varchar(90) not null,
  primary key (id)
);

create table if not exists library_books(
  id int unsigned auto_increment,
  category_id int unsigned not null,
  author_id int unsigned not null,
  title varchar(120) not null,
  amount int unsigned not null,
  deleted tinyint(1) unsigned not null,
  primary key (id)
);

create table if not exists roles(
  id int unsigned auto_increment,
  name varchar(50) not null,
  primary key (id)
);

create table if not exists status(
  id int unsigned auto_increment,
  status varchar(60) not null,
  primary key(id)
);

create table if not exists workers(
  id int unsigned auto_increment,
  role_id int unsigned not null,
  f_name varchar(50),
  l_name varchar(50),
  login varchar(20) not null,
  password varchar(40) not null,
  status_id tinyint(2) unsigned not null,
  primary key (id)
);

create table if not exists readers(
  id int unsigned auto_increment,
  f_name varchar(50) not null,
  l_name varchar(50) not null,
  reg_begin date not null,
  reg_end date not null,
  status_id tinyint(2) unsigned not null,
  primary key (id)
);

create table if not exists books_monitor(
  id int unsigned auto_increment,
  book_id int unsigned not null,
  worker_id int unsigned not null,
  reader_id int unsigned not null,
  issue_date date not null,
  return_date date not null,
  primary key (id)
);

/*------------------------------------------
courses_db
------------------------------------------*/

create database if not exists courses_db
  default character set ascii collate ascii_general_ci;

use courses_db;

set foreign_key_checks = 0;

create table if not exists roles(
  id int unsigned auto_increment,
  name varchar(50) not null,
  primary key (id)
);

create table if not exists status(
  id int unsigned auto_increment,
  status varchar(60) not null,
  primary key(id)
);

create table if not exists users(
  id int unsigned auto_increment,
  role_id int unsigned not null,
  f_name varchar(50),
  l_name varchar(50),
  login varchar(20) not null,
  password varchar(40) not null,
  email varchar(20),
  photo varchar(200),
  status_id int unsigned not null,
  primary key (id)
);

create table if not exists category(
  id int unsigned not null,
  name varchar(80) not null,
  color varchar(7) not null,
  deleted tinyint(1) unsigned not null,
  primary key (id)
);

create table if not exists courses(
  id int unsigned auto_increment,
  category_id int unsigned not null,
  author_id int unsigned not null,
  name varchar(120) not null,
  description varchar(400),
  image varchar(200),
  deleted tinyint(1) unsigned not null,
  primary key (id)
);

create table if not exists lectures(
  id int unsigned auto_increment,
  course_id int unsigned not null,
  test_id int unsigned,
  title varchar(200) not null,
  body varchar(10000),
  raite tinyint unsigned,
  number_in_course tinyint,
  deleted tinyint(1) unsigned not null,
  primary key (id)
);

create table if not exists score(
  id int unsigned auto_increment,
  course_id int unsigned not null,
  user_id int unsigned not null,
  finished tinyint(1),
  finish_date datetime,
  primary key (id)
);

create table if not exists progress(
  id int unsigned auto_increment,
  lecture_id int unsigned not null,
  score_id int unsigned not null,
  passtest tinyint not null,
  finalexam tinyint(1) unsigned not null,
  primary key (id)
);

create table if not exists comments(
  id int unsigned auto_increment,
  user_id int unsigned not null,
  lecture_id int unsigned not null,
  creation_date datetime not null,
  title varchar(70) not null,
  body varchar(500),
  active tinyint(1) unsigned not null,
  primary key (id)
);

/*---------------------
--test variant
-----------------------*/
create table if not exists lecture_books(
id int auto_increment,
book_id int unsigned not null,
lecture_id int unsigned not null,
test_progress tinyint unsigned not null,
primary key (id)
);


create user 'librarian'@'localhost' identified by 'libs';
grant insert, update, delete, select, create view on library_db.* to 'librarian'@'localhost';

create user 'courses_master'@'localhost' identified by 'crss';
grant insert, update, delete, select, create view on courses_db.* to 'courses_master'@'localhost';
grant select on library_db.category to 'courses_master'@'localhost';
grant select on library_db.library_books to 'courses_master'@'localhost';
grant select on library_db.books_monitor to 'courses_master'@'localhost';

