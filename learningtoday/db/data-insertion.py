#__author__ = 'alex'

import sys
import MySQLdb as mdb


class insertPosts:

    def __init__(self):
        pass

    def connect_to_db(self, host, user, password, db_name):
        try:
            self.con = mdb.connect(host, user, password, db_name)
            self.con.set_character_set('utf8')
            self.cur = self.con.cursor()
            self.cur.execute('SET NAMES utf8;')
            self.cur.execute('SET CHARACTER SET utf8;')
            self.cur.execute('SET character_set_connection=utf8;')
        except mdb.Error as e:
            print "Problem while connecting to db. Details below: "
            print "Error {0}: {1}".format(e.args[0], e.args[1])
            if self.con:
                self.con.close()
            #sys.exit(1)

    def exec_sql(self, sql, exec_list):
        try:
            self.cur.executemany(sql, exec_list)
            self.con.commit()
        except mdb.Error as e:
            print "Error {0}: {1}".format(e.args[0], e.args[1])
            print sql
            self.con.rollback()
            if self.con:
                self.con.close()
            #sys.exit(1)

#---------------------------
#working with library_db
#---------------------------

ins = insertPosts()

ins.connect_to_db("localhost", "librarian", "libs", "library_db")

roles_sql = "insert into roles (name) values(%s)"
roles_values = [("Super librarian",),
                ("Librarian",)]
ins.exec_sql(roles_sql, roles_values)

category_sql = "insert into category (name, deleted) values (%s, %s)"
category_values = [("Astronomy", 0),
                   ("Computer sciences", 0)]
ins.exec_sql(category_sql, category_values)

status_sql = "insert into status (status) values (%s)"
status_values = [("Active",),
                 ("Deleted",),
                 ("Not confirmed",)]
ins.exec_sql(status_sql, status_values)

workers_sql = "insert into workers (role_id, f_name, l_name, login, password, status_id) values (%s, %s, %s, %s, %s, %s)"
workers_values = [(1, "Jon", "Collins", "login1", "pass1", 1),
                  (2, "Linda", "Haek", "login2", "pass2", 1),
                  (2, "Incr", "Hulk ", "login3", "pass3", 1),
                  (2, "Capitan", "America", "login4", "pass4", 1)]
ins.exec_sql(workers_sql, workers_values)

books_authors_sql = "insert into books_authors (name) values (%s)"
books_authors_values = [("Eleonora Shah",),
                        ("Ivan Fast",),
                        ("Alan Miln",),
                        ("Alesandr Veona",),
                        ("Agnia Barto",),
                        ("Philp Blah",),
                        ("Ilya Bragin",),
                        ("Grimm bro.",),
                        ("Vctor Sidd",),
                        ("Arcan Vlasov",)]
ins.exec_sql(books_authors_sql, books_authors_values)

library_books_sql = "insert into library_books (category_id, author_id, title, amount, deleted) values (%s, %s, %s, %s, %s)"
library_books_values = [(2, 1, "Python for tanks", 10, 0),
                        (2, 2, "Pyhton for blonds", 12, 0),
                        (2, 3, "Pyhton for wolfs", 8, 0),
                        (2, 4, "OOP for birds", 3, 0),
                        (2, 5, "C-- for tubes", 21, 0),
                        (2, 6, "Crash Windows in 5 sec", 20, 0),
                        (2, 7, "Doomed MS-DOS", 3, 0),
                        (1, 8, "UFO near Venus", 2, 0),
                        (1, 9, "Asteroid belt", 20, 0),
                        (1, 10, "Earth doomed", 1, 0)]
ins.exec_sql(library_books_sql, library_books_values)

readers_sql = "insert into readers (f_name, l_name, reg_begin, reg_end, status_id) values (%s, %s, %s, %s, %s)"
readers_values = [("Nil", "Arnstrong", "1930-10-20", "1931-10-20", 1),
                  ("Baz", "Oldrin", "1931-01-01", "1932-01-01", 1),
                  ("Pete", "Conrad", "1932-02-02", "1933-02-02", 1),
                  ("Alan", "Bin", "1932-04-06", "1933-04-06", 1),
                  ("Alan", "Shepard", "1923-02-02", "1924-02-02", 1),
                  ("Edgar", "Mitchel", "1930-07-12", "1931-07-12", 1),
                  ("Devid", "Scott", "1932-06-17", "1933-06-17", 1),
                  ("James", "Irvin", "1930-12-31", "1931-12-31", 1),
                  ("Jon", "Young", "1930-03-08", "1931-03-08", 1),
                  ("Charls", "Diuk", "1935-05-23", "1936-05-23", 1),
                  ("Udjin", "Sernan", "1934-02-20", "1935-02-20", 1),
                  ("Harisson", "Smith", "1935-11-01", "1936-11-01", 1)]
ins.exec_sql(readers_sql, readers_values)

books_monitor_sql = "insert into books_monitor (book_id, worker_id, reader_id, issue_date, return_date) values \
                    (%s, %s, %s, %s, %s)"
books_monitor_values = [(9, 2, 1, "2005-05-01", "2005-06-01"),
                        (9, 3, 12, "2010-01-12", "2010-02-12"),
                        (6, 4, 5, "2008-03-01", "2008-04-01"),
                        (2, 2, 10, "2011-06-13", "2011-07-13"),
                        (9, 3, 6, "2050-06-04", "2050-07-04"),
                        (1, 1, 2, "2000-01-01", "2000-02-01"),
                        (7, 3, 8, "2011-05-05", "2011-06-05"),
                        (8, 3, 5, "2001-09-11", "2001-10-11"),
                        (4, 2, 1, "2006-03-08", "2006-04-08"),
                        (10, 2, 3, "2007-05-03", "2007-06-03")]
ins.exec_sql(books_monitor_sql, books_monitor_values)
ins.con.close()

#---------------------------
#working with courses_db
#---------------------------

ins.connect_to_db("localhost", "courses_master", "crss", "courses_db")

roles_sql = "insert into roles (name) values (%s)"
roles_values = [("Admin",),
                ("Teacher",),
                ("Student",)]
ins.exec_sql(roles_sql, roles_values)

category_sql = "insert into category (id, name, color, deleted) values (%s, %s, %s, %s)"
category_values = [(1, "Space", '#777888', 0),
                   (2, "IT", '#999ddd', 0)]
ins.exec_sql(category_sql, category_values)

status_sql = "insert into status (status) values (%s)"
status_values = [("Active",),
                 ("Deleted",),
                 ("Not confirmed",)]
ins.exec_sql(status_sql, status_values)

users_sql = "insert into users (role_id, f_name, l_name, login, password, email, photo, status_id) \
            values (%s, %s, %s, %s, %s, %s, %s, %s)"
users_values = [(1, "Admin", "Adminovich", "login10", "pass10", "email1@em.inc", "path/to/photo", 1),
                (2, "Alexander", "Mironenko", "login11", "pass11", "email2@em.inc", "path/to/photo", 1),
                (2, "Ievgen", "Shtikov", "login12", "pass12", "email3@em.inc", "path/to/photo", 1),
                (3, "Putilevskii", "Leonid", "login13", "pass13", "email4@em.inc", "path/to/photo", 1),
                (3, "Alexander", "Sergeev", "login14", "pass14", "email5@em.inc", "path/to/photo", 1),
                (3, "Stupnikov", "Andrei", "login15", "pass15", "email6@em.inc", "path/to/photo", 1),
                (3, "Gerasimenko", "Sergei", "login16", "pass16", "email7@em.inc", "path/to/photo", 1),
                (3, "Botov", "Anton", "login17", "pass17", "email8@em.inc", "path/to/photo", 3),
                (3, "Dmitrii", "Gurkin", "login18", "pass18", "email9@em.inc", "path/to/photo", 3),
                (3, "Alexander", "Lisicin", "login19", "pass19", "email10@em.inc", "path/to/photo", 3),
                (3, "Dmitrii", "Starkov", "login20", "pass20", "email11@em.inc", "path/to/photo", 3),
                (3, "Alexander", "Martenjanenkov", "login21", "pass21", "email12@em.inc", "path/to/photo", 3),
                (3, "Victor", "Philimonenkov", "login22", "pass22", "email13@em.inc", "path/to/photo", 3)]
ins.exec_sql(users_sql, users_values)

courses_sql = "insert into courses (category_id, author_id, name, description, \
               image, deleted) values(%s, %s, %s, %s, %s, %s)"
courses_values = [(2, 2, "Python programming", "For experienced", "path/to/image", 0),
                  (1, 3, "Main asteroid belt", "For nubies", "path/to/image", 0)]
ins.exec_sql(courses_sql, courses_values)

lectures_sql = "insert into lectures (course_id, test_id, title, body, deleted, raite) \
               values (%s, %s, %s, %s, %s, %s)"
lectures_values = [(1, 1, "Course 1 lecture 1", "Long long boring lecture 1 at course 1", 0, 1),
                   (1, 2, "Course 1 lecture 2", "Long long boring lecture 2 at course 1", 0, 1),
                   (2, 3, "Course 2 lecture 1", "Long long boring lecture 1 at course 2", 0, 1),
                   (2, 4, "Course 2 lecture 2", "Long long boring lecture 2 at course 2", 0, 1)]
ins.exec_sql(lectures_sql, lectures_values)

score_sql = "insert into score (course_id, user_id) values (%s, %s)"
score_values = [(1, 4),
                (2, 5),
                (1, 6),
                (2, 7)]
ins.exec_sql(score_sql, score_values)

progress_sql = "insert into progress (lecture_id, score_id, passtest, finalexam) values (%s, %s, %s, %s, %s)"
progress_values = [(1, 1, 10, 0),
                   (2, 2, 30, 0),
                   (3, 3, 10, 1),
                   (4, 4, 90, 0)]
ins.exec_sql(progress_sql, progress_values)

comments_sql = "insert into comments (user_id, lecture_id, creation_date, title, body, active) values \
                (%s, %s, %s, %s, %s, %s)"
comments_values = [(4, 1, "2012-01-01", "comment title 1", "comment body 1", 1),
                   (5, 2, "2012-02-01", "comment title 2", "comment body 2", 0),
                   (6, 3, "2012-03-01", "comment title 3", "comment body 3", 1),
                   (7, 4, "2012-04-01", "comment title 4", "comment body 4", 1),
                   (4, 1, "2012-05-01", "comment title 5", "comment body 5", 1),
                   (5, 2, "2012-06-01", "comment title 6", "comment body 6", 0),
                   (6, 3, "2012-07-01", "comment title 7", "comment body 7", 1),
                   (7, 4, "2012-08-01", "comment title 8", "comment body 8", 1)]
ins.exec_sql(comments_sql, comments_values)

ins.con.close()
