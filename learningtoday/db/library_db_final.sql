-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 11, 2013 at 12:53 AM
-- Server version: 5.1.66
-- PHP Version: 5.3.3-7+squeeze17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `library_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `books_authors`
--

DROP TABLE IF EXISTS `books_authors`;
CREATE TABLE `books_authors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(90) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=15 ;

--
-- Dumping data for table `books_authors`
--

INSERT INTO `books_authors` (`id`, `name`) VALUES
(1, 'John McBane'),
(2, 'asdsad'),
(3, 'Barak Obama'),
(4, 'Barka Obama'),
(5, 'Eleonora Shah'),
(6, 'Ivan Fast'),
(7, 'Alan Miln'),
(8, 'Alesandr Veona'),
(9, 'Agnia Barto'),
(10, 'Philp Blah'),
(11, 'Ilya Bragin'),
(12, 'Grimm bro.'),
(13, 'Vctor Sidd'),
(14, 'Arcan Vlasov');

-- --------------------------------------------------------

--
-- Table structure for table `books_monitor`
--

DROP TABLE IF EXISTS `books_monitor`;
CREATE TABLE `books_monitor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(10) unsigned NOT NULL,
  `worker_id` int(10) unsigned NOT NULL,
  `reader_id` int(10) unsigned NOT NULL,
  `issue_date` date NOT NULL,
  `return_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=11 ;

--
-- Dumping data for table `books_monitor`
--

INSERT INTO `books_monitor` (`id`, `book_id`, `worker_id`, `reader_id`, `issue_date`, `return_date`) VALUES
(1, 9, 2, 1, '2005-05-01', '2005-06-01'),
(2, 9, 3, 12, '2010-01-12', '2010-02-12'),
(3, 6, 4, 5, '2008-03-01', '2008-04-01'),
(4, 2, 2, 10, '2011-06-13', '2011-07-13'),
(5, 9, 3, 6, '2050-06-04', '2050-07-04'),
(6, 1, 1, 2, '2000-01-01', '2000-02-01'),
(7, 7, 3, 8, '2011-05-05', '2011-06-05'),
(8, 8, 3, 5, '2001-09-11', '2001-10-11'),
(9, 4, 2, 1, '2006-03-08', '2006-04-08'),
(10, 10, 2, 3, '2007-05-03', '2007-06-03');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=11 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `deleted`) VALUES
(1, 'Python', 1),
(2, 'MySQL', 2),
(3, 'Ruby', 2),
(4, 'Pyramid', 2),
(5, 'HTML', 2),
(6, 'JS', 2),
(9, 'Django', 1),
(10, 'C#', 1);

-- --------------------------------------------------------

--
-- Table structure for table `library_books`
--

DROP TABLE IF EXISTS `library_books`;
CREATE TABLE `library_books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `title` varchar(120) NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=23 ;

--
-- Dumping data for table `library_books`
--

INSERT INTO `library_books` (`id`, `category_id`, `author_id`, `title`, `amount`, `deleted`) VALUES
(1, 1, 3, 'GroovingPy', 11, 1),
(2, 1, 1, 'Building Machine Learning Systems with Python', 123, 1),
(3, 2, 1, 'Scoolj for school', 19, 2),
(4, 3, 1, 'Rubymine', 3, 2),
(5, 4, 1, 'Mummy', 33, 1),
(6, 1, 1, 'Python in Practice', 21, 1),
(7, 1, 1, 'Python Testing Cookbook', 42, 1),
(8, 3, 4, 'Metaclasses in Ruby', 11, 1),
(9, 1, 1, 'Python for tanks', 10, 0),
(10, 1, 2, 'Pyhton for blonds', 12, 0),
(11, 1, 3, 'Pyhton for wolfs', 8, 0),
(12, 2, 4, ' SQL in a Nutshell: A Desktop Quick Reference', 3, 0),
(13, 2, 5, 'Teach Yourself SQL in 10 Minutes', 21, 0),
(14, 2, 6, ' SQL Bible', 20, 0),
(15, 2, 7, 'SQL Queries for Mere Mortals', 3, 0),
(16, 1, 8, 'UFO near Venus', 2, 0),
(17, 1, 9, 'Learning to Program Using Python', 20, 0),
(18, 3, 10, 'Earth doomed', 1, 0),
(19, 5, 6, 'Head First HTML with CSS & XHTML (Paperback) ', 21, 0),
(20, 5, 10, 'Designing with Web Standards (Paperback) ', 10, 0),
(21, 5, 5, 'HTML, XHTML, and CSS (Visual Quickstart Guide) ', 13, 0),
(22, 5, 12, 'Introducing HTML5 (Paperback) ', 34, 0);

-- --------------------------------------------------------

--
-- Table structure for table `readers`
--

DROP TABLE IF EXISTS `readers`;
CREATE TABLE `readers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `f_name` varchar(50) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `reg_begin` date NOT NULL,
  `reg_end` date NOT NULL,
  `status_id` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=13 ;

--
-- Dumping data for table `readers`
--

INSERT INTO `readers` (`id`, `f_name`, `l_name`, `reg_begin`, `reg_end`, `status_id`) VALUES
(1, 'Nil', 'Arnstrong', '1930-10-20', '1931-10-20', 1),
(2, 'Baz', 'Oldrin', '1931-01-01', '1932-01-01', 1),
(3, 'Pete', 'Conrad', '1932-02-02', '1933-02-02', 1),
(4, 'Alan', 'Bin', '1932-04-06', '1933-04-06', 1),
(5, 'Alan', 'Shepard', '1923-02-02', '1924-02-02', 1),
(6, 'Edgar', 'Mitchel', '1930-07-12', '1931-07-12', 1),
(7, 'Devid', 'Scott', '1932-06-17', '1933-06-17', 1),
(8, 'James', 'Irvin', '1930-12-31', '1931-12-31', 1),
(9, 'Jon', 'Young', '1930-03-08', '1931-03-08', 1),
(10, 'Charls', 'Diuk', '1935-05-23', '1936-05-23', 1),
(11, 'Udjin', 'Sernan', '1934-02-20', '1935-02-20', 1),
(12, 'Harisson', 'Smith', '1935-11-01', '1936-11-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Super librarian'),
(2, 'Librarian');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=4 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`) VALUES
(1, 'Active'),
(2, 'Deleted'),
(3, 'Not confirmed');

-- --------------------------------------------------------

--
-- Table structure for table `workers`
--

DROP TABLE IF EXISTS `workers`;
CREATE TABLE `workers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `f_name` varchar(50) DEFAULT NULL,
  `l_name` varchar(50) DEFAULT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `status_id` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ascii AUTO_INCREMENT=5 ;

--
-- Dumping data for table `workers`
--

INSERT INTO `workers` (`id`, `role_id`, `f_name`, `l_name`, `login`, `password`, `status_id`) VALUES
(1, 1, 'Jon', 'Collins', 'login1', 'pass1', 1),
(2, 2, 'Linda', 'Haek', 'login2', 'pass2', 1),
(3, 2, 'Incr', 'Hulk ', 'login3', 'pass3', 1),
(4, 2, 'Capitan', 'America', 'login4', 'pass4', 1);
