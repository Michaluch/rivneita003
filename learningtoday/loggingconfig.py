'''
	This module create loggers
'''
import logging


#creates logger with rotating file handler
def make_logger_rotate(name, level, file_name, file_mode, size, backup_count):
	'''
		This mehtod creates logger
		Attr:
			name - logger name
			level:
				DEBUG
				INFO
				WARNING
				ERROR
				CRITICAL
			file - path to log file
			file_mode - opening file mode
			size - log file size
			backup_count - count of log file backups
		Returns:
			logger
	'''
	#setting logger
	logger = logging.getLogger(name)
	logger.setLevel(getattr(logging, level))
	logger.propagate = False

	#setting logging handler
	rh = logging.handlers.RotatingFileHandler(file_name, mode=file_mode,
		 maxBytes=size, backupCount=backup_count)
	rh.setLevel(getattr(logging, level))

	#setting formatter for logging handler
	formatter = logging.Formatter('[%(levelname)s %(asctime)s] %(message)s')
	rh.setFormatter(formatter)

	#adding handler to logger
	logger.addHandler(rh)
	return logger


#General logger
logger = make_logger_rotate('gen', 'INFO', 'learningtoday/log/general.log', 'a', 1000*1000*10, 5)

#Security logger
security_logger = make_logger_rotate('sec', 'INFO', 'learningtoday/log/security.log', 'a', 1000*1000*10, 5)

#DB log
db_logger = make_logger_rotate('dbs', 'INFO', 'learningtoday/log/dbs.log', 'a', 1000*1000*10, 5)
