from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    remember,
    forget,
    authenticated_userid,
)
from learningtoday.src.models import User
from learningtoday.src.views.securityview import (
    session_decorator,
    logged_in_users,
)

from learningtoday.loggingconfig import security_logger

@session_decorator
class Login(object):
    def __init__(self, request):
        self.request = request

    #@view_config(
    #    context='pyramid.httpexceptions.HTTPForbidden',
    #    request_method='GET',
    #    renderer='develop_login.jinja2',
    #)
    #def login_page(self):
    #    if authenticated_userid(self.request):
    #        return {'message': 'You are not allowed to access this section'}
    #    return {'message': 'Please login to access this page'}

    @view_config(
        route_name='login',
        request_method='GET',
        renderer='develop_login.jinja2',
    )
    def login(self):
        if authenticated_userid(self.request):
            if authenticated_userid(self.request) == 'admin':
                return HTTPFound(location='/admin')
            return HTTPFound(location=self.request.application_url)
        return {'message': 'Please fill login and password'}

    @view_config(
        request_method="POST",
        route_name='login',
        renderer='develop_login.jinja2',
    )
    def login_post(self):
        security_logger.info('IP: %s, who: %s. user login' % (self.request.remote_addr, 
                                                              self.request.params.get('login', '')))
        login = self.request.params.get("login")
        hashed_password = self.request.params.get("hash")
        if not login:
            security_logger.error('can not login user')
            return {'message': 'Please fill login field'}
        elif not hashed_password:
            security_logger.error('can not login user')
            return {'message': 'Please fill password field'}

        user = User(login=login, password=hashed_password)
        if user.check_exist():
            if logged_in_users.get(login):
                del logged_in_users[login]
            headers = remember(self.request, login)

            # saving session (username and cookie)
            logged_in_users[login] = headers[0][1][headers[0][1].find('"') + 1:headers[0][1].find('";')]
            security_logger.info('user login successful')
            if login == 'admin':
                return HTTPFound(location='/admin', headers=headers)
            return HTTPFound(location=self.request.application_url, headers=headers)
        security_logger.error('can not login user')
        return {'message': 'Wrong credentials'}

    @view_config(route_name='logout')
    def logout(self):
        headers = forget(self.request)
        security_logger.info('IP: %s, who: %s. user logout' % (self.request.remote_addr, 
                                                               authenticated_userid(self.request)))
        return HTTPFound(location=self.request.application_url, headers=headers)
