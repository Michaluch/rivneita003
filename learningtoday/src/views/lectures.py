from pyramid.view import view_config
from learningtoday.src.models import Comment
from learningtoday.src.models import User
from learningtoday.src.models import Lecture
from learningtoday.src.models import Course
from operator import itemgetter
import nltk

from learningtoday.src.views.securityview import (
    session_decorator,
)

from pyramid.security import (
    authenticated_userid,
)
from learningtoday.loggingconfig import logger

@session_decorator
class ManageLecture(object):
    def __init__(self, request):
        self.request = request
        self.lecture_deleted = 1
        self.lecture_active = 0

    @view_config(route_name='manage_lectures',
                 renderer='lectures.jinja2',
                 request_method='GET')
    def get_lecture_view(self):
        courses = Course().get(author_id=User().get_one(login=authenticated_userid(self.request))['id'])
        for course in courses:
            lectures = Lecture().get(course_id=course['id'])
            lectures.sort(key=itemgetter('number_in_course'))
            course['lectures'] = lectures
            course['author_id'] = User().get_one(id=course['author_id'])['login']
        return {'courses': courses}


    @view_config(route_name='manage_lectures',
                 renderer='develop_lectures.jinja2',
                 request_method='POST',
                 request_param = 'action=edit_lecture')
    def edit_lecture_view(self):
        new_details = dict()
        for key in self.request.params:
            if len(self.request.params[key])>=1:
                if key in ('id', 'number_in_course'):
                    new_details[key] = int(self.request.params.get(key))
                elif not key=='action':
                    new_details[key] = self.request.params.get(key)
        lec = Lecture().get_one(id=new_details['id'])
        l = Lecture(**new_details)
        try:
            l = l.update()
        except Exception, e:
            logger.error('can not edit lecture: %s' % (e,))
        logger.info('lecture edited')
        lectures = Lecture().get(course_id=lec['course_id'])
        lectures.sort(key=itemgetter('number_in_course'))
        course = Course().get_one(id=lec['course_id'])
        return {"lectures": lectures}

    @view_config(route_name='manage_lectures',
                 renderer='json',
                 request_method='POST',
                 request_param='action=edit_status')
    def lectures_edit_status_view(self):
        """Returns new status of comment"""
        id = int(self.request.params.get('id'))
        deleted = int(self.request.params.get('deleted'))
        new_status = self.lecture_deleted
        if deleted==self.lecture_active:
            Lecture(id=id, deleted=self.lecture_deleted).update()
            new_status = self.lecture_deleted
        elif deleted==self.lecture_deleted:
            Lecture(id=id, deleted=self.lecture_active).update()
            new_status = self.lecture_active
        return {'new_status': new_status}

    @view_config(route_name='manage_lectures',
                 renderer='lecture_add.jinja2',
                 request_method='POST',
                 request_param='action=get_add_form')
    def lectures_add_view(self):
        courses = Course().get(author_id=User().get_one(login=authenticated_userid(self.request))['id'])
        return {"courses": courses}

    @view_config(route_name='manage_lectures',
                 renderer='json',
                 request_method='POST',
                 request_param='action=check_number')
    def lecture_check_number_view(self):
        number = int(self.request.params.get('number'))
        course_id = int(self.request.params.get('course_id'))
        lecture_id = int(self.request.params.get('lecture_id'))
        exists = False
        lecture = Lecture().get_one(number_in_course=number, course_id=course_id)
        if lecture:
            if lecture_id:
                if lecture['id']!=lecture_id:
                    exists = True
            else:
                exists = True
        return {'exists': exists}

    @view_config(route_name='manage_lectures',
                 renderer='develop_lectures.jinja2',
                 request_method='POST',
                 request_param='action=add_lecture')
    def lecture_add_view(self):
        title = self.request.params.get('title')
        body = self.request.params.get('body')
        course_id = self.request.params.get('course_id')
        deleted = self.request.params.get('deleted')
        number_in_course = self.request.params.get('number_in_course')
        try:
            l = Lecture(title=title, body=body, course_id=course_id,
                        deleted=deleted, number_in_course=number_in_course).add()
            lectures = Lecture().get(course_id=course_id)
            lectures.sort(key=itemgetter('number_in_course'))
            return {'lectures': lectures}
        except:
            return {'status': 'error'}

    @view_config(route_name='manage_lectures',
                 renderer='develop_lectures.jinja2',
                 request_method='POST',
                 request_param='action=get_lectures')
    def lecture_get_view(self):
        course_id = self.request.params.get('course_id')
        lectures = Lecture().get(course_id=course_id)
        lectures.sort(key=itemgetter('number_in_course'))
        return {'lectures': lectures}

    @view_config(
        route_name='manage_lectures',
        renderer='develop_lectures.jinja2',
        request_method='POST',
        request_param='action=search'
    )
    def search_lecture_view(self):
        """Accepts: search_key and search_value;
Returns: CommentsTable with params:
comments - list of dictionaries of comments;
number - number of pages for pagination;
page - number of page to show"""
        result = []
        course_id = int(self.request.params.get('course_id'))
        search_key = self.request.params['search_key']
        search_value = self.request.params['search_value']
        search_for = {search_key: search_value}
        result = Lecture().get(course_id=course_id, **search_for)
        result.sort(key=itemgetter('number_in_course'))
        if search_key=='body':
            for lecture in result:
                lec = nltk.clean_html(lecture['body']).lower()
                if not search_value.lower() in lec:
                    result.remove(lecture)
        return {'lectures': result }
