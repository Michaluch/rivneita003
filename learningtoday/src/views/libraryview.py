from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from securityview import session_decorator

from learningtoday.src.models import Book
from learningtoday.src.models import BookAuthor
from learningtoday.src.models import LibCategory

from pyramid.security import (
    authenticated_userid,
    )
from learningtoday.loggingconfig import logger


@session_decorator
class Library(object):
    def __init__(self, request):
        self.request = request

    @staticmethod
    @view_config(
        route_name='library',
        renderer='develop_library.jinja2',
        permission='edit',
        request_method='GET'
    )
    def library():
        books_list = []
        for book in Book().get():
            book['author_id'] = '{name}'.format(**BookAuthor().get_one(id=book['author_id']))
            book['category_id'] = '{name}'.format(**LibCategory().get_one(id=book['category_id']))
            books_list.append(book)
        categories = LibCategory().get()
        return {
            'books_list': books_list,
            'categories': categories
        }

    @staticmethod
    @view_config(
        route_name='library',
        renderer='develop_add_book.jinja2',
        permission='edit',
        request_method='GET',
        request_param='action=add_book'
    )
    def add_book():
        return {
            'book_categories': LibCategory().get(),
            'book_authors': BookAuthor().get(),
        }

    @view_config(
        route_name='library',
        permission='edit',
        request_method='POST',
        request_param='action=add_book_submit',
        renderer='json',

    )
    def add_book_post(self):
        logger.info('IP: %s, who: %s. adding new book(s) to library. \
            title: %s, author id: %s, amount: %s' % (self.request.remote_addr,
                                                     authenticated_userid(self.request),
                                                     self.request.params.get('title', ''),
                                                     self.request.params.get('author_id', ''),
                                                     self.request.params.get('amount', '')))
        details = {'deleted': 1}
        for key in self.request.params:
            if key in ('category_id', 'author_id', 'amount'):
                details[key] = int(self.request.params[key])
            elif key == 'title':
                details[key] = self.request.params[key]
        try:
            Book(**details).add()
            logger.info('new book added')
            return {'status': True}
        except Exception, e:
            logger.error('can not add new book: %s' % (e,))
            return {'status': False}

    @view_config(
        route_name='edit_book',
        renderer='develop_edit_book.jinja2',
        permission='edit',
    )
    def edit_book_post(self):
        print 'INSIDE DEVELOP EDIT BOOK'
        return {'book_details': Book().get_one(id=int(self.request.matchdict.get('id'))),
                'book_categories': LibCategory().get(),
                'book_authors': BookAuthor().get()}

    @view_config(
        route_name='library',
        renderer='develop_library_edit_books.jinja2',
        permission='edit',
        request_param='action=book_edit_show'
    )
    def edit_book_display(self):
        books_list = []
        for book in Book().get():
            book['author_id'] = '{name}'.format(**BookAuthor().get_one(id=book['author_id']))
            book['category_id'] = '{name}'.format(**LibCategory().get_one(id=book['category_id']))
            books_list.append(book)
        categories = LibCategory().get()
        return {
            'books_list': books_list
        }

    @view_config(
        route_name='edit_book_post',
        permission='edit',
        request_method='POST',
    )
    def edit_book_post(self):
        """Accepts: user id from request parameters
        Returns: right now HTTPFound but probably need to be changed to JSON"""
        logger.info('IP: %s, who: %s. editing book in library. id: %s' % (self.request.remote_addr,
                                                                          authenticated_userid(self.request),
                                                                          self.request.params.get('id', '')))
        new_details = dict()
        for key in self.request.params:
            if len(self.request.params[key]) >= 1:
                if key in ('id', 'author_id', 'category_id', 'amount'):
                    new_details[key] = int(self.request.params[key])
                else:
                    new_details[key] = self.request.params[key]
        try:
            Book(**new_details).update()
        except Exception, e:
            logger.error('can not edit book: %s' % (e,))
        logger.info('book edited')
        return HTTPFound(location='/library')

    @view_config(
        route_name='book',
        permission='edit',
        request_method='PUT',
        request_param='action=change_status',
        renderer='json',
    )
    def book_status(self):
        """This view gets id of the book,
        and changes its status to the opposite from the current,
        if instance update was successful it returns True
        else returns False
        """
        # select book with requested id
        logger.info('IP: %s, who: %s. changing book in library. id: %s' % (self.request.remote_addr,
                                                                           authenticated_userid(self.request),
                                                                           self.request.params.get('id', '')))
        book = Book().get_one(id=int(self.request.params.get('id')))
        book_id = book['id']
        status_id = book['deleted']
        try:
            # depending on its current status, change to opposite
            if status_id == 1:
                Book(id=book_id, deleted=2).update()
            else:
                Book(id=book_id, deleted=1).update()
        except Exception, e:
            logger.error('can not change book status: %s', (e,))
            return {'status_change': False}
        logger.info('book status changed')
        return {'status_change': True}

    @view_config(
        route_name='library',
        permission='edit',
        renderer='develop_library_search.jinja2',
        request_method='GET',
        request_param='action=search_book',
    )
    def search_book(self):
        search_key = self.request.params.get('search_key')
        search_value = self.request.params.get('search_value')
        search_queue = []
        print search_key
        print search_value

        def find_category_id(name=''):
            category = LibCategory().get(name=name)
            print category
            if category:
                for category_item in category:
                    search_queue.append({search_key: int(category_item['id'])})
            return search_queue

        def find_author_id(name=''):
            author = BookAuthor().get(name=name)
            if author:
                for author_item in author:
                    search_queue.append({search_key: int(author_item['id'])})
            return search_queue

        if search_key == 'category_id':
            search_queue = find_category_id(search_value)
        elif search_key == 'author_id':
            search_queue = find_author_id(search_value)
        elif search_key == 'title':
            search_results = []
            books_found = Book().get(title=search_value)
            for item in books_found:
                search_results.append(item)

            for result in search_results:
                result['category_id'] = LibCategory().get_one(id=result['category_id'])['name']
                result['author_id'] = BookAuthor().get_one(id=result['author_id'])['name']

            #print search_results
            return {'search_results': search_results, 'book_categories': LibCategory().get(),}

        search_results = []
        for query in search_queue:
            for item in Book().get(**query):
                search_results.append(item)

        for result in search_results:
            result['category_id'] = LibCategory().get_one(id=result['category_id'])['name']
            result['author_id'] = BookAuthor().get_one(id=result['author_id'])['name']

        print search_results
        return {'search_results': search_results, 'book_categories': LibCategory().get()}

    @staticmethod
    @view_config(
        route_name='library',
        permission='edit',
        renderer='develop_add_category_library.jinja2',
        request_method='GET',
        request_param='action=add_category',
    )
    def add_category_get():
        return {'status': True}

    @view_config(
        route_name='library',
        permission='edit',
        request_method='POST',
        renderer='json',
        request_param='action=add_category_submit'
    )
    def add_category_library_post(self):
        logger.info('IP: %s, who: %s. adding library category. name: %s' % (self.request.remote_addr,
                                                                            authenticated_userid(self.request),
                                                                            self.request.params.get('name', ''),))
        details = {}
        if 'deleted' in self.request.params:
            details['deleted'] = 1
        else:
            details['deleted'] = 2
        if 'name' in self.request.params:
            details['name'] = self.request.params.get('name')
        else:
            logger.error('can not add library category')
            return {'status': False}
        try:
            LibCategory(**details).add()
        except Exception, e:
            logger.error('can not add library category: %s' % (e,))
            return {'status': False}
        logger.info('library category added')
        return {'status': True}

    @staticmethod
    @view_config(
        route_name='library',
        permission='edit',
        renderer='develop_add_author.jinja2',
        request_method='GET',
        request_param='action=add_author',
    )
    def add_author_get():
        return {'status': True}


    @view_config(
        route_name='library',
        permission='edit',
        request_method='POST',
        request_param='action=add_author_submit',
        renderer='json'
    )
    def add_author(self):
        logger.info('IP: %s, who: %s. adding book author. name: %s' % (self.request.remote_addr,
                                                                       authenticated_userid(self.request),
                                                                       self.request.params.get('name', ''),))
        if 'name' in self.request.params:
            try:
                BookAuthor(name=self.request.params.get('name')).add()
                logger.info('book author added')
                return {'status': True}
            except Exception, e:
                logger.error('can not add book author: %s' % (e,))
                return {'status': False}
        else:
            logger.error('can not add book author: parameter "name" not found')
            return {'status': False}
