import os
import shutil
import imghdr

from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.security import (authenticated_userid,)
from learningtoday.src.models.coursemodel import Course
from ..models import User
from securityview import (session_decorator,)
from datetime import datetime


@session_decorator
class UploadFile(object):
    def __init__(self, request):
        self.request = request

    @view_config(
        route_name='upload_info_profile',
        request_method='POST',
        renderer='json',
    )
    def image_mananger_profile(self):
        login = authenticated_userid(self.request)
        user_id = User().get_one(login=login)["id"]
   #    print self.request.params
   #     filename = self.request.POST["some"].filename
   #    print filename
        input_file = self.request.POST["some"].file
   #    print input_file
        if imghdr.what(input_file) == 'jpeg' or imghdr.what(input_file) == 'jpg':
            now = datetime.now()
            current_year = str(now.year)
            current_month = str(now.month)
            current_day = str(now.day)
            current_hour = str(now.hour)
            current_minute = str(now.minute)
            current_microsecond = str(now.microsecond)

            filename = current_year + '-' + current_month + '-' + current_day + '__' + current_hour + ':' + \
                       current_minute + ':' + current_microsecond + '__' + login
            absolute = os.path.dirname(os.path.abspath('learningtoday/static/img/user_img' '/user_img'))
            file_path = os.path.join(absolute, filename)

            with open(file_path, 'wb') as output_file:
                shutil.copyfileobj(input_file, output_file)
            User(id=user_id, photo=filename).update()
            return {"filename": filename, "check": 1}
        else:
            current_name = User().get_one(id=user_id)['photo']
            return{"current_name": current_name, "check": 0}

    @view_config(
        route_name='manage_courses',
        request_method='POST',
        renderer='json',
    )
    def image_insert_courses(self):
        some_list = []
        for key in Course().get():
            s = self.request.params
            some_list.append(s)
            number = len(some_list) + 1
        edit_id = self.request.POST["id_edit"]
        int_id = int(edit_id)
#       print self.request.params
#       filename = self.request.POST["form-edit-course"].filename
        input_file = self.request.POST["form-edit-course"].file
        if imghdr.what(input_file) == 'jpeg' or imghdr.what(input_file) == 'jpg':
            now = datetime.now()
            current_year = str(now.year)
            current_month = str(now.month)
            current_day = str(now.day)
            current_hour = str(now.hour)
            current_minute = str(now.minute)
            current_microsecond = str(now.microsecond)

            filename = current_year + '-' + current_month + '-' + current_day + '__' + current_hour + ':' + \
                       current_minute + ':' + current_microsecond + '__'
            absolute = os.path.dirname(os.path.abspath('learningtoday/static/img/courses_img' '/courses_img'))
            file_path = os.path.join(absolute, filename)
            
            with open(file_path, 'wb') as output_file:
                shutil.copyfileobj(input_file, output_file)
            Course(id=int_id, image=filename).update()
            return {"filename": filename, 'int_id': int_id}
        else:
            pass

    @view_config(
        route_name='upload_info_courses',
        request_method='POST',
        renderer='json',
    )
    def image_add_courses(self):

        if int(self.request.params['num']) != 1:
            last_course = Course().get()[-1]
            new_id = last_course['id']
            filename = "null"
            Course(id=new_id, image=filename).update()
            return {"filename": filename, "new_id": new_id}
        else:
            input_file = self.request.POST["form-add-course"].file
            if imghdr.what(input_file) == 'jpeg' or imghdr.what(input_file) == 'jpg':
                now = datetime.now()
                current_year = str(now.year)
                current_month = str(now.month)
                current_day = str(now.day)
                current_hour = str(now.hour)
                current_minute = str(now.minute)
                current_microsecond = str(now.microsecond)

                filename = current_year + '-' + current_month + '-' + current_day + '__' + current_hour + ':' + \
                           current_minute + ':' + current_microsecond + '__'
                absolute = os.path.dirname(os.path.abspath('learningtoday/static/img/courses_img' '/courses_img'))
                file_path = os.path.join(absolute, filename)

                with open(file_path, 'wb') as output_file:
                    shutil.copyfileobj(input_file, output_file)
                last_course = Course().get()[-1]
                new_id = last_course['id']
                Course(id=new_id, image=filename).update()
                return {"filename": filename, "new_id": new_id}
            else:
                last_course = Course().get()[-1]
                new_id = last_course['id']
                filename = "null"
                Course(id=new_id, image=filename).update()
                return {"filename": filename, "new_id": new_id}