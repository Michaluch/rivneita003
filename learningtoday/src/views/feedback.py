from operator import itemgetter
from pyramid.view import view_config
from learningtoday.src.models import Comment
from learningtoday.src.models import User
from learningtoday.src.models import Lecture
from learningtoday.src.models import Course
from datetime import datetime
from datetime import date

from learningtoday.src.views.securityview import (
    session_decorator,
)

from pyramid.security import (
    authenticated_userid,
)
from learningtoday.loggingconfig import logger

@session_decorator
class Feedback(object):
    def __init__(self, request):
        self.request = request
        self.amount_of_comments = 10
        self.comment_active = 1
        self.comment_deleted = 0

        
    @view_config(route_name='feedbacks',
                 renderer='feedback.jinja2',
                 request_method='GET')
    def feedback_view(self):
        """Returns feedback page with params:
comments - list of dictionaries of comments;
number - number of pages for pagination;
page - number of page to show"""
        comments = Comment().get()
        for comment in comments:
            comment['active_user'] = User().get_one(id=comment['user_id'])['status_id']
            comment['user_id'] = User().get_one(id=comment['user_id'])['login']
            comment['lecture_id'] = Lecture().get_one(id=comment['lecture_id'])['title']
        comments.sort(key=itemgetter('creation_date'), reverse = True)
        if not len(comments)%self.amount_of_comments:
            number = len(comments)/self.amount_of_comments
        else:
            number = len(comments)/self.amount_of_comments + 1
        return {'comments': comments[0:self.amount_of_comments],
                'number': number,
                'page': 1}
    
    @view_config(route_name='feedbacks',
                 renderer='json',
                 request_method='POST',
                 request_param='action=edit_status')
    def feedback_edit_status_view(self):
        """Returns new status of comment"""
        id = int(self.request.params.get('id'))
        status = int(self.request.params.get('active'))
        new_status = self.comment_deleted
        if status==self.comment_active:
            Comment(id=id, active=self.comment_deleted).update()
            new_status = self.comment_deleted
        elif status==self.comment_deleted:
            Comment(id=id, active=self.comment_active).update()
            new_status = self.comment_active
        return {'new_status': new_status}
    
    @view_config(route_name='feedbacks',
                 renderer='develop_comment_table.jinja2',
                 request_method='POST',
                 request_param='action=change_page')
    def feedback_pagination_view(self):
        """Returns CommentsTable with params:
comments - list of dictionaries of comments;
number - number of pages for pagination;
page - number of page to show"""
        page = int(self.request.params.get('number'))
        comments = Comment().get()
        comments.sort(key=itemgetter('creation_date'), reverse = True)
        if not len(comments)%self.amount_of_comments:
            number = len(comments)/self.amount_of_comments
        else:
            number = len(comments)/self.amount_of_comments + 1
        comments = comments[(page-1)*self.amount_of_comments:page*self.amount_of_comments]
        for comment in comments:
            comment['active_user'] = User().get_one(id=comment['user_id'])['status_id']
            comment['user_id'] = User().get_one(id=comment['user_id'])['login']
            comment['lecture_id'] = Lecture().get_one(id=comment['lecture_id'])['title']
        return {'comments': comments,
                'number': number,
                'page': page}
    
    
    @view_config(
        route_name='feedbacks',
        renderer='develop_comment_table.jinja2',
        request_method='POST',
        request_param='action=search'
    )
    def search_comment(self):
        """Accepts: search_key and search_value;
Returns: CommentsTable with params:
comments - list of dictionaries of comments;
number - number of pages for pagination;
page - number of page to show"""
        page = int(self.request.params.get('page'))
        result = []
        search_key = self.request.params['search_key']
        if search_key=='creation_date':
            start = self.request.params.get('start')
            finish = self.request.params.get('finish')
            for com in Comment().get():
                if start<=com['creation_date'].split(' ')[0]<=finish:
                    result.append(com)
        else: 
            search_value = self.request.params['search_value']
            if search_key == 'user_id':
                users = User().get(login=search_value)
                for user in users:
                    search_for = {search_key: user['id']}
                    result.extend(Comment().get(**search_for))
            else:
                search_for = {search_key: search_value}
                result = Comment().get(**search_for)
        for comment in result:
            comment['active_user'] = User().get_one(id=comment['user_id'])['status_id']
            comment['user_id'] = User().get_one(id=comment['user_id'])['login']
            comment['lecture_id'] = Lecture().get_one(id=comment['lecture_id'])['title']
        result.sort(key=itemgetter('creation_date'), reverse = True)
        if not len(result)%self.amount_of_comments:
            number = len(result)/self.amount_of_comments
        else:
            number = len(result)/self.amount_of_comments + 1
        result = result[(page-1)*self.amount_of_comments:page*self.amount_of_comments]
        return {'comments': result,
                'number': number,
                'page': page
                }

    @view_config(
        route_name='feedbacks',
        renderer='json',
        request_method='GET',
        request_param='action=courses'
    )
    def get_courses_view(self):
        """Returns: list of dictionaries {id, name} of courses"""
        courses = []
        lectures = []
        for course in Course().get():
            courses.append({'id': course['id'], 'name': course['name']})
        return {'courses': courses}
    
    @view_config(
        route_name='feedbacks',
        renderer='json',
        request_method='POST',
        request_param='action=lectures'
    )
    def get_lectures_view(self):
        """Returns: list of dictionaries {id, name} of lectures"""
        lectures = []
        if 'course_id' in self.request.params:
            course_id = self.request.params.get('course_id');
            for lecture in Lecture().get(course_id=course_id):
                lectures.append({'id': lecture['id'], 'name': lecture['title']})
        else:
            for lecture in Lecture().get():
                lectures.append({'id': lecture['id'], 'name': lecture['title']})
        return {'lectures': lectures}
    
    @view_config(
        route_name='feedbacks',
        renderer='json',
        request_method='POST',
        request_param='action=add'
    )
    def add_comment_view(self):
        """Accepts: title, body of comment and lecture_id;
Returns: status of adding a new comment"""
        title = self.request.params.get('title')
        body = self.request.params.get('body')
        lecture_id = self.request.params.get('lecture_id')
        user_id = User().get_one(login=authenticated_userid(self.request))['id'];
        try:
            c = Comment(title=title, body=body, lecture_id=lecture_id, user_id=user_id,
                        active=self.comment_active, creation_date=datetime.now().strftime('%Y-%m-%d %H:%M:%S')).add()
            return {'status': 'added'}
        except:
            return {'status': 'error'}
    
 