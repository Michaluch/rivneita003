import os
import shutil
import transaction

from smtplib import SMTPException
from pyramid.view import view_config
from pyramid.security import (authenticated_userid, )
from learningtoday.src.models.coursemodel import Course
from ..models import User
from securityview import (session_decorator)
from email.mime.text import MIMEText
from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message
from pyramid_mailer.message import Attachment
from subprocess import Popen, PIPE


@session_decorator
class SendMessage(object):
    def __init__(self, request):
        self.request = request
        self.mailer = get_mailer(request)

    @view_config(
        route_name='subscribe_news',
        request_method='POST',
        renderer='json',
    )
    def message_email(self):
        for one_user in User().get():
            if one_user['status_id'] == 1:
                info_email = one_user['email']
                user_name = one_user['f_name']
                course_name = self.request.params['course_value']
                str_course = str(course_name)
                message = Message(subject="New course!",
                                  sender="Learning today",
                                  recipients=[info_email],
                                  html="""
                                  Hi {} !<br>We added new course:
                                  <font size='3' color='green'> {} </font></br>.
                                  Please visit our
                                  <a href='http://localhost:6543/'>site</a>
                                   for more details""".format(user_name, str_course))
                try:
                    self.mailer.send(message)
                    transaction.commit()
                    print 'ok'
                except SMTPException:
                    print 'error'

        return {'somee': '111'}