import collections
from pyramid.view import view_config
from operator import itemgetter
import json
import pprint

from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    authenticated_userid,
)
from ..models import User
from ..models import Course
from ..models import CurCategory
from ..models import CurRole
from ..models import Lecture
from ..models import Comment
from ..models import Tests

from ..models import Score
from ..models import Recommendations
from ..models import Book
from ..models import BookAuthor

from securityview import (
    session_decorator,
)


@session_decorator
class CourseView(object):
    def __init__(self, request):
        self.request = request

    @view_config(
        route_name='course',
        request_method='GET',
        renderer='course.jinja2',
        permission='view'

    )
    def course(self):
        course = Course().get_one(id=int(self.request.matchdict.get('id')))
        user = User().get_one(login=authenticated_userid(self.request))
        score = Score().get_one(course_id=course['id'], user_id=user['id'], finished=0)
        if not score:
            subscribe = True
        else:
            subscribe = False

        login = authenticated_userid(self.request)
        user_details = User().get_one(login=login)

        if (user_details['photo'] == '') or (user_details['photo'] is None):
            static_photo = 'unknown'
            user_details['photo'] = static_photo

        del user_details['id']
        del user_details['password']

        lectures = Lecture().get(course_id=course['id'])
        lectures.sort(key=itemgetter('number_in_course'))
        comments = []
        tests = []
        final_exam = []
        for lecture in lectures:
            comments.extend(Comment().get(lecture_id=lecture['id']))
            test = Tests().read(lecture_id=lecture['id'])
            if test:
                if test['final']:
                    final_exam =  test
                else:
                    tests.append(test)
        
        for comment in comments:
            comment['user_name'] = User().get_one(id=comment['user_id'])['login']
            comment['user_photo'] = User().get_one(id=comment['user_id'])['photo']
            comment['lecture_id'] = Lecture().get_one(id=comment['lecture_id'])['title']
        comments.sort(key=itemgetter('creation_date'))
        # pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(tests)
        return {'course': course,
                'lectures': lectures,
                'comments': comments,
                'subscribe': subscribe,
                'user_details':user_details
            }

    @view_config(
        route_name='course',
        request_method='GET',
        renderer='course_comments.jinja2',
        permission='view',
        request_param='action=course_comments'
    )
            
    def comment(self):
        course = Course().get_one(id=int(self.request.matchdict.get('id')))
        user = User().get_one(login=authenticated_userid(self.request))

        login = authenticated_userid(self.request)
        user_details = User().get_one(login=login)

        if (user_details['photo'] == '') or (user_details['photo'] is None):
            static_photo = 'unknown'
            user_details['photo'] = static_photo

        del user_details['id']
        del user_details['password']

        lectures = Lecture().get(course_id=course['id'])
        lectures.sort(key=itemgetter('number_in_course'))
        comments = []
        for lecture in lectures:
            comments.extend(Comment().get(lecture_id=lecture['id']))
        
        for comment in comments:
            comment['user_name'] = User().get_one(id=comment['user_id'])['login']
            comment['user_photo'] = User().get_one(id=comment['user_id'])['photo']
            comment['lecture_id'] = Lecture().get_one(id=comment['lecture_id'])['title']
        comments.sort(key=itemgetter('creation_date'))
        return {'course': course,
                'lectures': lectures,
                'comments': comments,
                'user_details': user_details
            }

    @view_config(
        route_name='course',
        request_method='GET',
        renderer='json',
        permission='view',
        request_param='action=lecture_test'
    )
    
    def lecture_test(self):
        
        course = Course().get_one(id=int(self.request.matchdict.get('id')))
        test = Tests().read(lecture_id=int(self.request.params['lecture_id']))
        # print test
        final_test = Tests().read(lecture_id=int(self.request.params['final_id']))
        # pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(tests)
        return {'test': test,
                'final_test': final_test
            }


@session_decorator
class RecommendationProcessing(object):

    def __init__(self, request):
        self.request = request
        self.user = User()
        self.course = Course()
        self.book = Book()
        self.book_author = BookAuthor()
        self.recommendation = Recommendations()

    def _check_user_course(self, course_id):
        user = self.user.get_one(login=authenticated_userid(self.request))
        return self.course.get_one(id=course_id, author_id=user['id'])


    @view_config(renderer='recommendations.jinja2',
                 request_method='GET',
                 route_name='recommendations',
                 )
    def render_recommendations(self):
        user = self.user.get_one(login=authenticated_userid(self.request))
        user_courses = self.course.get(author_id=user['id'])
        if user_courses:
            first_course = user_courses[0]
        else:
            return {'user_courses': [],
                    'first_course': {},
                    'books': [],
                    'recommendations': {},
                    }
        books = self.book.get(category_id=first_course['category_id'])
        recommendation_dict = {}
        for item in books:
            item['author'] = self.book_author.get_one(id=item['author_id'])['name']
        recommendation = self.recommendation.get_recommendation(first_course['id'])
        if not recommendation:
            recommendation = self.recommendation.add(first_course['id'], {})
        else:
            for level, recommended_books in recommendation['books_levels'].iteritems():
                books_list = []
                for book_id in recommended_books:
                    book_item = self.book.get_one(id=book_id)
                    book_author = self.book_author.get_one(id=book_item['author_id'])['name']
                    book_list = [book_id, book_item['title'], book_author]
                    books_list.append(book_list)
                recommendation_dict[level] = books_list
        return {'user_courses': user_courses,
                'first_course': first_course,
                'books': books,
                'recommendations': recommendation_dict,
                }

    @view_config(renderer='json',
                 request_method='GET',
                 request_param='action=get_user_recommendations',
                 route_name='recommendations')
    def user_recommendations(self):
        course_id = int(self.request.params.get('course_id'))
        grade = int(self.request.params.get('grade'))
        recommendation = self.recommendation.get_recommendation(course_id)
        first_loop = True
        recommendation_found = False
        recommended_books = []
        prev = -1
        books_levels = collections.OrderedDict(
                       sorted(recommendation['books_levels'].items())
                       )
        for level in books_levels:
            if (first_loop and (grade < int(level))) or grade == int(level):
                recommended_books = books_levels[level]
                recommendation_found = True
                break
            if int(prev) < grade < int(level) and not first_loop:
                recommended_books = books_levels[prev]
                recommendation_found = True
                break
            prev = level
            first_loop = False
        if not recommendation_found:
            recommended_books = books_levels[prev]
        books_list = []
        for book_id in recommended_books:
            book_item = self.book.get_one(id=book_id)
            book_author = self.book_author.get_one(id=book_item['author_id'])['name']
            book_list = [book_id, book_item['title'], book_author]
            books_list.append(book_list)
        print books_list
        return {'result': 'success', 'books_list': books_list}




    @view_config(renderer='json',
                 request_method='GET',
                 route_name='recommendations',
                 request_param='action=select_course',
                 )
    def select_course(self):
        course_id = self.request.params.get('course_id')
        if not course_id:
            return {'result': 'error', 'error': 'course id param not found'}
        course = self.course.get_one(id=course_id)
        books = self.book.get(category_id=course['category_id'])
        recommendation_dict = {}
        for item in books:
            item['author'] = self.book_author.get_one(id=item['author_id'])['name']
        recommendation = self.recommendation.get_recommendation(int(course_id))
        if not recommendation:
            recommendation = self.recommendation.add(int(course_id), {})
        else:
            for level, recommended_books in recommendation['books_levels'].iteritems():
                books_list = []
                for book_id in recommended_books:
                    book_item = self.book.get_one(id=book_id)
                    book_author = self.book_author.get_one(id=book_item['author_id'])['name']
                    book_list = [book_id, book_item['title'], book_author]
                    books_list.append(book_list)
                recommendation_dict[level] = books_list
        return {'result': 'success',
                'books': books,
                'recommendation': recommendation_dict,
                }

    @view_config(renderer='json',
                 request_method='PUT',
                 route_name='recommendations',
                 request_param='action=update',
                 )
    def update_recommendation(self):
        course_id = self.request.params.get('course_id')
        books_levels = self.request.params.get('books_levels')
        if not course_id or not books_levels:
            return {'result': 'error', 'error': 'course id or books levels not found'}
        if not self._check_user_course(course_id=int(course_id)):
            return {'result': 'error', 'error': 'wrong user'}
        if self.recommendation.add_update_book_level(int(course_id), json.loads(books_levels)):
            return {'result': 'success'}
        else: 
            return {'result': 'error', 'error': 'can not update recommendation'}

    @view_config(renderer='json',
                 request_method='DELETE',
                 route_name='recommendations',
                )
    def remove_level(self):
        course_id = self.request.json_body.get('course_id')
        book_level = self.request.json_body.get('book_level')
        if not course_id or not book_level:
            return {'result': 'error', 'error': 'course id or book level not found'}
        if not self._check_user_course(course_id=int(course_id)):
            return {'result': 'error', 'error': 'wrong user'}
        if self.recommendation.remove_book_level(int(course_id), book_level):
            return {'result': 'success'}
        else: 
            return {'result': 'error', 'error': 'can not remove book level in recommendation'}

    @view_config(renderer='json',
                 request_method='PUT',
                 route_name='recommendations',
                 request_param='action=remove',
                 )
    def remove(self):
        course_id = self.request.params.get('course_id')
        if not course_id:
            return {'result': 'error', 'error': 'course id not found'}
        if self.recommendation.remove(int(course_id)):
            return {'result': 'success'}
        else:
            return {'result': 'error', 'error': 'can not remove recommendation'}

