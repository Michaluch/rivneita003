import re
import os
import shutil
import imghdr
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    authenticated_userid,
)
from operator import itemgetter

from learningtoday.src.models import User
from learningtoday.src.models import Course
from learningtoday.src.models import CurCategory
from learningtoday.src.models import CurRole
from learningtoday.src.models import LibCategory
from learningtoday.src.models import Comment
from learningtoday.src.models import Lecture
from learningtoday.src.models import Score
from learningtoday.src.models import Recommendations
from learningtoday.src.models import Book
from learningtoday.src.models import BookAuthor

from learningtoday.src.views.securityview import (
    session_decorator,
)
from datetime import datetime
from learningtoday.loggingconfig import logger
from learningtoday.src.views.uploadview import UploadFile
@session_decorator
class AdminPanel(object):
    def __init__(self, request):
        self.request = request

    #@staticmethod
    @view_config(
        route_name='admin_panel',
        renderer='admin.jinja2',
        permission='change',
        request_method='GET'
    )
    def admin_panel(self):
        """This view returns 3 lists with dictionaries:
1) list of users, with modified role_id, to display role name
2) list of courses, with modified author and category,
to display their names instead of id's
3) list of categories
""" 
        active_user = User().get_one(login=authenticated_userid(self.request))
        role_name = CurRole().get_one(id=active_user['role_id'])['name']
        # if teacher -> search course by author_id
        # if administrator -> search all
        def get_courses(**kwargs):
            teacher_not_admin = True if role_name == 'teacher' else False
            if teacher_not_admin:
                courses = Course().get(author_id=active_user['id'], **kwargs)
            else:
                courses = Course().get(**kwargs)
            return courses

        courses_list = []
        for course in get_courses():
            # overwriting id's with names
            course['author'] = '{f_name} {l_name}'.format(**User().get_one(id=course['author_id']))
            course['category'] = '{name}'.format(**CurCategory().get_one(id=course['category_id']))
            courses_list.append(course)

        avilable_library_categories = []
        course_categories_id = []
        if role_name == 'admin':
            for item in CurCategory().get():
                course_categories_id.append(item["id"])
            for lib_category in LibCategory().get():
                if lib_category["id"] not in course_categories_id:
                    avilable_library_categories.append(lib_category)

        recommendation_dict = {}
        user_courses = []
        books = []
        first_course = {}
        if role_name == 'teacher':
            user_courses = Course().get(author_id=active_user['id'])
            if user_courses:
                first_course = user_courses[0]
                books = Book().get(category_id=first_course['category_id'])
                for item in books:
                    item['author'] = BookAuthor().get_one(id=item['author_id'])['name']
                recommendation = Recommendations().get_recommendation(first_course['id'])
                if not recommendation:
                    recommendation = Recommendations().add(first_course['id'], {})
                else:
                    for level, recommended_books in recommendation['books_levels'].iteritems():
                        books_list = []
                        for book_id in recommended_books:
                            book_item = Book().get_one(id=book_id)
                            book_author = BookAuthor().get_one(id=book_item['author_id'])['name']
                            book_list = [book_id, book_item['title'], book_author]
                            books_list.append(book_list)
                        recommendation_dict[level] = books_list

        return {'courses_list': courses_list,
                'categories_list': CurCategory().get(),
                'available_lib_categories': avilable_library_categories,
                'user_courses': user_courses,
                'first_course': first_course,
                'books': books,
                'recommendations': recommendation_dict,
                }

    @staticmethod
    @view_config(
        route_name='manage_users',
        request_method='GET',
        renderer='admin_page_users.jinja2',
        permission='change',
    )
    def get_users():
        users_list = []
        for user in User().get():
            user['role_id'] = '{name}'.format(**CurRole().get_one(id=user['role_id']))
            del user['password']
            users_list.append(user)

        roles_list = CurRole().get()

        return {'users_list': users_list,
                'roles_list': roles_list}

    @view_config(
        route_name='manage_users',
        renderer='json',
        permission='edit',
        request_method='GET',
        request_param='action=manage'
    )
    def manage_users_view(self):
        """This view returns 3 lists with dictionaries:
        1) list of users, with modified role_id, to display role name
        2) list of courses, with modified author and category,
           to display their names instead of id's
        3) list of categories
        """
        users_list = User().get();
        for item in users_list:
            del item['password']
            item['role_id'] = '{name}'.format(**CurRole().get_one(id=item['role_id']))
        return {'search_result': users_list}

    @view_config(
        route_name='manage_users',
        renderer='json',
        permission='edit',
        request_method='POST',
        request_param='action=add'
    )
    def add_user_post(self):
        logger.info('IP: %s, who: %s. adding new user. nickname: %s' % (self.request.remote_addr, 
                                                                        authenticated_userid(self.request), 
                                                                        self.request.params['login']))
        user_details = dict()
        if User().get_one(login=self.request.params['login']):
            logger.error('user with such register parameters exist')
            print "login exists"
            return {'message': 'User with login: {}, already exists.'.format(self.request.params['login'])}
        if User().get_one(email=self.request.params['email']):
            logger.error('user with such register parameters exist')
            print "email exists"
            return {'message': 'User with email: {}, already exists.'.format(self.request.params['email'])}

        login = self.request.params['login']
        user_details['role_id'] = 3
        user_details['status_id'] = 1

        for key in self.request.params:
            if len(self.request.params[key]) >= 1:
                if key == 'password':
                    user_details['password'] = self.request.params[key].encode('latin-1')
                elif key in ('login', 'f_name', 'l_name', 'email'):
                    user_details[key] = self.request.params[key].encode('latin-1')
                elif key in ('id', 'status_id', 'role_id'):
                    user_details[key] = int(self.request.params[key])
            else:
                del user_details['status_id']
                del user_details['role_id']
                logger.info('can not add new user: no parameters recieved')
                return {'message': 'Please fill all the fields',
                        'user_details': user_details}
        try:
            User(**user_details).add()
        except Exception, e:
            logger.error('can not add new user: %s' % (e,))

        user_details = User().get_one(login=login)
        del user_details['password']
        logger.info('new user added')
        return user_details

    @view_config(
        route_name='manage_users',
        renderer='json',
        permission='edit',
        request_method='GET',
        request_param='action=get_user_info'
    )
    def get_user_info(self):
        login = self.request.params['login']
        user_details = User().get_one(login=login)
        return user_details

    @view_config(
        route_name='manage_users',
        renderer='json',
        request_method="POST",
        permission='edit',
        request_param='action=edit'
    )
    def edit_user_post(self):
        logger.info('IP: %s, who: %s. editing user. id: %s' % (self.request.remote_addr, 
                                                               authenticated_userid(self.request),
                                                               self.request.params['login']))
        print self.request.params
        new_details = dict()
        for key in self.request.params:
            if len(self.request.params[key]) >= 1:
                if key in ('id', 'status_id', 'role_id'):
                    new_details[key] = int(self.request.params[key])
                else:
                    new_details[key] = self.request.params[key]

        try:
            a = User(**new_details).update()
        except Exception, e:
            logger.error('can not edit user: %s' % (e,))
            print 'error', e
            return {'message': 'Some error occured. We working on it'}

        logger.info('user edited')
        return {'isChanged': a, 'details': new_details}

    @view_config(
        route_name='manage_users',
        renderer='json',
        request_method='POST',
        permission='edit',
        request_param='action=change_user_status'
    )
    def change_user_status(self):
        logger.info('IP: %s, who: %s. changing user status. id: %s' % (self.request.remote_addr, 
                                                                       authenticated_userid(self.request),
                                                                       self.request.params.get('user_id', '')))
        status_id = int(self.request.params.get('status_id'))
        role_id = User().get_one(login=self.request.params["login"])["role_id"]

        if role_id == 1:
            logger.error("Users from admin group can't be activated or deactivated")
            return {"err_admin": "You can't deactivate or activate user from admin group"}

        try:
            user_id = int(self.request.params['user_id'])
        except:
            user_id = User().get_one(login=self.request.params.get('user_id'))['id']
            
        new_status = 0

        try:
            if status_id == 1:
                User(id=user_id, status_id=2).update()
                new_status = 2
            else:
                User(id=user_id, status_id=1).update()
                new_status = 1
        except Exception, e:
            logger.error('can not change user status: %s', (e,))
        logger.info('user status changed')
        return {'new_status': new_status}

    @view_config(
        route_name='manage_users',
        permission='edit',
        renderer='json',
        request_method='POST',
        request_param='action=search'
    )
    def search_user(self):
        """This view will return result of the search query,
as list with user instances as dictionaries
"""
        search_key = self.request.params['search_key']
        search_value = self.request.params['search_value']

        # in request params we receive role as name,
        # that is why we need to find in table Roles,
        # what id corresponds to this role
        if search_key == 'role_id':
            search_value = int(CurRole().get_one(name=search_value)['id'])

        search_for = {search_key: search_value}
        result = User().get(**search_for)

        # removing hashed passwords from search results
        for item in result:
            del item['password']
            item['role_id'] = '{name}'.format(**CurRole().get_one(id=item['role_id']))
        return {'search_result': result}

    """@staticmethod
@view_config(
route_name='add_course',
renderer='develop_add_course.jinja2',
permission='edit',
request_method='GET',
)
def add_course_display():
return {'category_list': CurCategory().get()}"""

    @view_config(
        route_name='manage_courses',
        permission='change',
        request_method='POST',
        renderer='json',
        request_param='action=add',
    )
    def add_course_post(self):
        logger.info('IP: %s, who: %s. adding new course. name: %s' % (self.request.remote_addr, 
                                                                      authenticated_userid(self.request),
                                                                      self.request.params.get('name', '')))
        details = {'author_id': User().get_one(login=authenticated_userid(self.request).encode('latin-1'))['id'],
                   'deleted': 0}
      #  print details
     #   filename = self.request.POST['course_img'].filename
      #  input_file = self.request.POST['course_img'].file
  #      print self.request.params
        for key in self.request.params:
            if key == 'category_id':
                details[key] = int(self.request.params[key])
            else:
                details[key] = self.request.params.get(key, '')

        try:
            course = Course(**details)
  #          print cours
            if course.add():
                response = details
        #        print response
                response['id'] = course.id
                response['author'] = '{f_name} {l_name}'.format(**User().get_one(id=course.author_id))
                response['category'] = '{name}'.format(**CurCategory().get_one(id=course.category_id))
                response["image"] = course.image
                logger.info('new course added')
                return {'result': 'success', 'course': response}
            else:
                logger.error('can not add course')
                return {'result': 'error', 'error': 'can not add course'}

        except Exception, e:
            logger.error('can not add course: %s' % (e,))
            return {'result': 'error', 'error': 'can not add course'}

    """@view_config(
route_name='manage_courses',
renderer='develop_edit_course.jinja2',
permission='edit',
)
def edit_course_display(self):
return {'course_details': Course().get_one(id=int(self.request.matchdict.get('id'))),
'course_categories': CurCategory().get()}"""

    @view_config(
        route_name='manage_courses',
        permission='change',
        request_method='PUT',
        renderer='json',
        request_param='action=edit',
    )
    def edit_course_post(self):
        logger.info('IP: %s, who: %s. editing course. id: %s' % (self.request.remote_addr, 
                                                                 authenticated_userid(self.request),
                                                                 self.request.params.get('id', '')))
        new_details = dict()
        for key in self.request.params:
            if len(self.request.params[key]) >= 1:
                if key in ('id', 'status_id', 'category_id'):
                    new_details[key] = int(self.request.params[key])
                else:
                    new_details[key] = self.request.params[key]
        try:
            if Course(**new_details).update():
                logger.info('course edited')
                return {'result': 'success'}
            else:
                logger.error('can not edit course')
                return {'result': 'error', 'error': 'error while updating course'}

        except Exception, e:
            logger.error('can not edit course: %s' % (e,))
            return {'result': 'error', 'error': 'error while updating course'}

    @view_config(
        route_name='manage_courses',
        permission='change',
        request_method='GET',
        renderer='json',
        request_param='action=search',
    )
    def search_course(self):
        print '-----------searching course-----------------'
        search_category_id = self.request.params.get('category_id')
        search_key = self.request.params.get('key')
        search_value = self.request.params.get('value')
        search_queue = []
        """def find_category_id(name):
category = CurCategory().get(name=name)
if category:
for category_item in category:
search_queue.append({search_key: int(category_item['id'])})
return search_queue"""
        def find_author_id(full_name=''):
            f_name = re.search('\w+(?= )|\w+', full_name)
            l_name = re.search('(?<= )\w+', full_name)
            if f_name and l_name:
                users = User().get(f_name=f_name.group(0), l_name=l_name.group(0))
                for user_item in users:
                    search_queue.append({search_key: int(user_item['id'])})
                return search_queue
            elif f_name:
                f = User().get(f_name=f_name.group(0))
                #l = User().get(l_name=f_name.group(0))
                for user_item in f: # + l:
                    search_queue.append({search_key: int(user_item['id'])})
                return search_queue

        """if search_key == 'category_id':
search_queue = find_category_id(search_value)"""
        # if teacher -> search course by author_id
        # if administrator -> search all
        def get_courses(**kwargs):
            user = User().get_one(login=authenticated_userid(self.request))
            role_name = CurRole().get_one(id=user['role_id'])['name']
            teacher_not_admin = True if role_name == 'teacher' else False
            if teacher_not_admin:
                courses = Course().get(author_id=user['id'], **kwargs)
            else:
                courses = Course().get(**kwargs)
            return courses


        if search_key == 'author_id':
            search_queue = find_author_id(search_value)
        elif search_key == 'name':
            courses = get_courses(name=search_value, category_id=search_category_id)
            if courses:
                for item in courses:
                    item['author'] = '{f_name} {l_name}'.format(**User().get_one(id=int(item["author_id"])))
                print courses
                return {'result': 'success', 'courses': courses}
            else:
                return {'result': 'error', 'error': 'problem searching course'}
        elif search_key == 'description':
            courses = get_courses(description=search_value, category_id=search_category_id)
            if courses:
                for item in courses:
                    item['author'] = '{f_name} {l_name}'.format(**User().get_one(id=int(item["author_id"])))
                print courses
                return {'result': 'success', 'courses': courses}
            else:
                return {'result': 'error', 'error': 'problem searching course'}
        elif search_value == "_":
            courses = get_courses(category_id=search_category_id)
            if courses:
                for item in courses:
                    item['author'] = '{f_name} {l_name}'.format(**User().get_one(id=int(item["author_id"])))
                return {'result': 'success', 'courses': courses}
            else:
                return {'result': 'error', 'error': 'problem searching course'}
        courses = []
        for query in search_queue:
            for item in get_courses(category_id=search_category_id, **query):
                item['author'] = '{f_name} {l_name}'.format(**User().get_one(id=int(item["author_id"])))
                courses.append(item)
        return {'result': 'success', 'courses': courses}

    @view_config(
        route_name='manage_courses',
        permission='change',
        request_method='PUT',
        renderer='json',
        request_param='action=change_status',
    )
    def change_status_course(self):
        logger.info('IP: %s, who: %s. changing course status. id: %s' % (self.request.remote_addr, 
                                                                         authenticated_userid(self.request),
                                                                         self.request.params.get('id', '')))
        status_id = int(Course().get_one(id=int(self.request.params.get('id')))['deleted'])
        try:
            if status_id == 0:
                if Course(id=int(self.request.params.get('id')), deleted=1).update():
                    logger.info('course status changed')
                    return {'result': 'success', 'status': 1}
                else:
                    logger.error('can not change course status')
                    return {'result': 'error', 'error': 'error while changing course'}
            else:
                if Course(id=int(self.request.params.get('id')), deleted=0).update():
                    logger.info('course status changed')
                    return {'result': 'success', 'status': 0}
                else:
                    logger.error('can not change course status')
                    return {'result': 'error', 'error': 'error while changing course'}
        except Exception, e:
            logger.error('can not change course status: %s' % (e,))
            return {'result': 'error', 'error': 'error while changing course'}



    """@staticmethod
@view_config(
route_name='manage_categories',
renderer='develop_add_category.jinja2',
permission='edit',
request_method='POST',
)
def add_category():
""""""This view, returns list with dictionaries,
of library categories that haven't been used in courses categories
""""""
# saving lists of all categories in courses and libraries
course_categories = CurCategory().get()
library_categories = LibraryCategory().get()

# creating sets from id's of courses
course_set = set(d['id'] for d in course_categories)
library_set = set(d['id'] for d in library_categories)

# saving id's of library categories that are not in courses
unique_set = library_set.symmetric_difference(course_set)

# creating list with categories that haven't been used in courses yet
result = [category for category in library_categories if category['id'] in unique_set]
return {'book_categories': result}"""

    @view_config(
        route_name='manage_categories',
        permission='edit',
        request_method='POST',
        request_param='action=add',
        renderer='json',
    )
    def add_category_post(self):
        logger.info('IP: %s, who: %s. adding new course category. name: %s' % (self.request.remote_addr, 
                                                                               authenticated_userid(self.request),
                                                                               self.request.params.get('name', '')))
        details = {}
        if 'id' in self.request.params:
            details['id'] = int(self.request.params['id'])
        else:
            logger.error('recieving wrong parameters')
            return {'result': 'error', 'error': 'id parameter not found'}
        if 'deleted' in self.request.params:
            details['deleted'] = 1
        else:
            details['deleted'] = 0
        if 'name' in self.request.params:
            details['name'] = self.request.params.get('name')
        else:
            logger.error('recieving wrong parameters')
            return {'result': 'error', 'error': 'wrong parameters adding category'}
        if 'color' in self.request.params:
            details['color'] = self.request.params.get('color')
        else:
            logger.error('recieving wrong parameters')
            return {'result': 'error', 'error': 'wrong parameters adding category'}
        try:
            cur_category = CurCategory(**details).add()
            if cur_category:
                logger.info('new category added')
                return {'result': 'success'}
            else:
                logger.error('can not add new course category')
                return {'result': 'error', 'errror': 'error while adding category'}
        except Exception, e:
            logger.error('can not add new course category: %s' % (e,))
            return {'result': 'error', 'error': 'critical error while adding category'}

    """@view_config(
route_name='manage_categories',
renderer='develop_edit_category.jinja2',
permission='edit',
request_method='PUT',
request_param='action=edit',
renderer='json',
)
def edit_category_display(self):
""""""This view, returns list with dictionaries of library categories,
that haven't been used in courses categories
plus current category
""""""
# saving lists of all categories in courses and libraries
course_categories = CurCategory().get()
library_categories = LibraryCategory().get()

# creating sets from id's of courses
course_set = set(d['id'] for d in course_categories)
library_set = set(d['id'] for d in library_categories)

# saving id's of library categories that are not in courses
unique_set = library_set.symmetric_difference(course_set)

# creating list with categories that haven't been used in courses yet
result = [category for category in library_categories if category['id'] in unique_set]

# adding currently assigned category of course to the list
result.insert(0, CurCategory().get_one(id=self.request.matchdict.get('id')))

return {'book_categories': result,
'category_details': CurCategory().get_one(id=int(self.request.matchdict.get('id'))),
}"""

    @view_config(
        route_name='manage_categories',
        permission='edit',
        request_method='PUT',
        request_param='action=edit',
        renderer='json',
    )
    def edit_category_post(self):
        """This view edits category name and its reference to the category from library"""
        # if name changed we save it,
        # else we take the old one from db

        if self.request.params['name']:
            name = self.request.params['name']
        else:
            name = CurCategory().get_one(id=self.request.params['id'])['name']

        if self.request.params['color']:
            color = self.request.params['color']
        else:
            color = CurCategory().get_one(id=self.request.params['id'])['color']
        category_id = CurCategory().get_one(id=int(self.request.params.get('cat_id')))["id"]

        name = self.request.params["name"]

        CurCategory(id=category_id, name=name, color=color).update()

        return {"name": name}

        # next sting is commented for later usage,
        # and awaiting methods implementation from CurCategory model
        # right now category editing will do nothing
        #CurCategory(id=int(self.request.params['id']), name=name).update(id=library_id)

        # change to JSON after AJAX implementation
        

    @view_config(
        route_name='manage_categories',
        permission='edit',
        request_method='PUT',
        request_param='action=change_status',
        renderer='json',
    )
    def change_status_category(self):
        logger.info('IP: %s, who: %s. changing course category status. id: %s' % (self.request.remote_addr, 
                                                                                  authenticated_userid(self.request),
                                                                                  self.request.params.get('id', '')))
        status_id = int(CurCategory().get_one(id=int(self.request.params.get('id')))['deleted'])
        try:
            if status_id == 0:
                if CurCategory(id=int(self.request.params.get('id')), deleted=1).update():
                    logger.info('course category status changed')
                    return {'result': 'success', 'status': 1}
                else:
                    logger.error('can not change course category status')
                    return {'result': 'error', 'error': 'error while changing category'}
            else:
                if CurCategory(id=int(self.request.params.get('id')), deleted=0).update():
                    logger.info('course category status changed')
                    return {'result': 'success', 'status': 0}
                else:
                    logger.error('can not change course category status')
                    return {'result': 'error', 'error': 'error while changing category'}
        except Exception, e:
            logger.error('can not change course category status: %s' % (e,))
            return {'result': 'error', 'error': 'error while changing category'}
