import collections
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    authenticated_userid,
)

from learningtoday.src.models import User
from learningtoday.src.models import Course
from learningtoday.src.models import CurCategory
from learningtoday.src.models import Score
from learningtoday.src.models import Progress
from learningtoday.src.models import Recommendations
from learningtoday.src.models import Book
from learningtoday.src.models import BookAuthor

from securityview import (
    session_decorator,
)
from learningtoday.loggingconfig import logger

from ..models import Score


@session_decorator
class Profile(object):
    def __init__(self, request):
        self.request = request

    @view_config(
        route_name='profile',
        request_method='GET',
        renderer='profile.jinja2',
    )
    def profile(self):
        """This view receives no parameters, it responds to GET request
        Returns dicts:
            1.User details:
              - login,
              - f_name,
              - l_name,
              - email,
              - status_id,
              - photo
            2.Subscribed courses:
              - name,
              - description,
              - color,
              - deleted
        """
        login = authenticated_userid(self.request)
        user_details = User().get_one(login=login)

        if (user_details['photo'] == '') or (user_details['photo'] is None):
            static_photo = 'unknown'
            user_details['photo'] = static_photo

        score = Score().get(user_id=user_details['id'])

        courses_list = []
        for key in score:
            course = Course().get_one(id=key['course_id'])
            category = CurCategory().get_one(id=course['category_id'])
            if category['id'] is not None:
                category['id'] = course['id']
                category['name'] = course['name']
                category['description'] = course['description']
                category['deleted'] = course['deleted']
                category['finished'] = key['finished']
                courses_list.append(category)

        del user_details['id']
        del user_details['password']
        del user_details['role_id']

        return {"user_details": user_details,
                "courses_list": courses_list}

    @view_config(
        route_name='profile',
        request_method='GET',
        renderer='json',
        request_param='action=get_user'
    )
    def get_user(self):
        login = authenticated_userid(self.request)

        user = User().get_one(login=login)

        del user['id']
        del user['password']
        del user['f_name']
        del user['l_name']
        del user['photo']
        del user['status_id']
        del user['role_id']
        del user['groups']

        return user

    @view_config(
        route_name='profile',
        request_method='POST',
        renderer='json',
        request_param='action=edit_profile'
    )
    def profile_edit_post(self):
        """Gets request profile parameters and updates profile
        This function gets request profile parameters(login, email) from ajax
        and update currently logged user details(login and email)
        """
        login = authenticated_userid(self.request)
        user_id = User().get_one(login=login)["id"]

        login = self.request.params["login"]
        email = self.request.params["email"]
        try:
            User(id=user_id, login=login, email=email).update()
        except Exception as e:
            logger.error('can not edit user profile')
        logger.info('user prodile edited. new login: %s' % (login,))
        return {"login": login, "email": email}

    @view_config(
        route_name='profile',
        request_method='GET',
        renderer='profile_books.jinja2',
        request_param='action=get_recomends'
    )
    def get_profile_recomends(self):

        def _round_score_result(val):
            return int(round(val))

        course_id = self.request.params.get('course_id')
        finished = self.request.params.get('finished', 1)

        if not course_id:
            return {'result': 'error', 'error': 'request param course_id not set'}

        get_user = User().get_one(login=authenticated_userid(self.request))
        last_score = Score().get_one(course_id=course_id, user_id=get_user['id'], finished=finished, order_by='id',
                                        asc_desc='desc', limit=1)
        score_result = 0
        progress_list = Progress().get(score_id=last_score['id'])
        tests_count = len(progress_list) - 1

        for item in progress_list:
            item['passtest'] = 0 if item['passtest'] == -1 else item['passtest']
            if item['finalexam'] == 1:
                score_result += _round_score_result(item['passtest'] * 0.6)
            else:
                score_result += _round_score_result((item['passtest'] * 0.4) / tests_count)

        if score_result > 100:
            score_result = 100
        score_result = 0 if score_result == -1 else score_result

        grade = score_result
        recommendation = Recommendations().get_recommendation(course_id)
        first_loop = True
        recommendation_found = False
        recommended_books = []
        prev = -1
        books_levels = collections.OrderedDict(
                       sorted(recommendation['books_levels'].items())
                       )
        for level in books_levels:
            if (first_loop and (grade < int(level))) or grade == int(level):
                recommended_books = books_levels[level]
                recommendation_found = True
                break
            if int(prev) < grade < int(level) and not first_loop:
                recommended_books = books_levels[prev]
                recommendation_found = True
                break
            prev = level
            first_loop = False
        if not recommendation_found:
            recommended_books = books_levels[prev]

        books_list = []
        for book_id in recommended_books:
            book_item = Book().get_one(id=book_id)
            del book_item['category_id']
            del book_item['deleted']
            del book_item['amount']
            books_list.append(book_item)

        return {'books_list': books_list}