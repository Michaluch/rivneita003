import inspect

from pyramid.view import forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPForbidden
from pyramid.security import (
    forget,
    authenticated_userid,
    Allow,
    ALL_PERMISSIONS,
)

from ..models import User
from ..models import CurRole
from ..models import Course
from ..models import Tests
from ..models import Lecture
from learningtoday.loggingconfig import security_logger


class RootFactory(object):
    __acl__ = [
        (Allow, 'role:admin', ALL_PERMISSIONS),
        (Allow, 'role:teacher', 'change'),
        (Allow, 'role:teacher', 'view'),
        (Allow, 'role:student', 'view'),
    ]

    def __init__(self, request):
        self.request = request


class UserFactory(object):
    __acl__ = [
        (Allow, 'role:admin', ALL_PERMISSIONS),
        (Allow, 'role:teacher', 'view'),
        (Allow, 'role:student', 'view'),
    ]

    def __init__(self, request):
        self.request = request

    def __getitem__(self, key):
        user = User(login=key)
        user.__parent__ = self
        user.__name__ = key
        return user


class CourseFactory(object):
    __acl__ = [
        (Allow, 'role:admin', ALL_PERMISSIONS),
        (Allow, 'role:teacher', 'view'),
        (Allow, 'role:teacher', 'create'),
        (Allow, 'role:student', 'view'),
    ]

    def __init__(self, request):
        self.request = request

    def __getitem__(self, key):
        course_details = Course().get_one(id=key)
        if course_details:
            course = Course(**course_details)
            course.__parent__ = self
            course.__name__ = key
            return course
        return None


class TestFactory(object):
    __acl__ = [
        (Allow, 'role:admin', 'view'),
        (Allow, 'role:admin', 'create'),
        (Allow, 'role:teacher', 'view'),
        (Allow, 'role:teacher', 'create'),
        (Allow, 'role:student', 'view'),
    ]

    def __init__(self, request):
        self.request = request

    def __getitem__(self, key):
        lecture_id = int(self.request.matchdict.get('lecture_id'))
        lecture_details = Lecture().get_one(id=lecture_id)
        author_id = Course().get_one(id=lecture_details.get('course_id')).get('author_id')
        if author_id:
            test = Tests(author_id=author_id)
            test.__parent__ = self
            test.__name__ = key
            return test
        return None


def rolefinder(userid, request):
    user = User().get_one(login=userid)
    if user:
        return ['role:%s' % CurRole().get_one(id=user['role_id'])['name']]


class DecoratedMethod(object):
    def __init__(self, func):
        self.func = func

    def __get__(self, obj, cls=None):
        def wrapper(*args, **kwargs):
            if args:
                ret = self.func(obj, args[1], **kwargs)
            else:
                ret = self.func(obj, *args, **kwargs)
            return ret

        for attr in "__module__", "__name__", "__doc__":
            setattr(wrapper, attr, getattr(self.func, attr))
        return wrapper


class DecoratedClassMethod(object):
    def __init__(self, func):
        self.func = func

    def __get__(self, obj, cls=None):
        def wrapper(*args, **kwargs):
            if check_session(obj.__dict__['request']):
                return HTTPFound(location='/login', headers=forget(obj.__dict__['request']))
            ret = self.func(*args, **kwargs)
            return ret

        for attr in "__module__", "__name__", "__doc__":
            setattr(wrapper, attr, getattr(self.func, attr))
        return wrapper


def session_decorator(cls):
    for name, meth in inspect.getmembers(cls):
        if inspect.ismethod(meth):
            if inspect.isclass(meth.im_self):
                setattr(cls, name, DecoratedClassMethod(meth))
            else:
                setattr(cls, name, DecoratedMethod(meth))
        elif inspect.isfunction(meth):
            setattr(cls, name, DecoratedClassMethod(meth))
    return cls

# dictionary of currently active users
logged_in_users = {}


def check_session(request):
    """Checks if user is the only user currently logged in"""
    if logged_in_users.get(authenticated_userid(request)) is not None and request.cookies.get('auth_tkt') is not None:
        if logged_in_users[authenticated_userid(request)] != request.cookies['auth_tkt']:
            security_logger.warning('IP: %s, who: %s. user login from new place. closing old session\
                ' % (request.remote_addr, authenticated_userid(request)))
            return True
    elif logged_in_users.get(authenticated_userid(request)) is None and request.cookies.get('auth_tkt') is not None:
        logged_in_users[authenticated_userid(request)] = request.cookies['auth_tkt']


@forbidden_view_config()
def forbidden_view(request):
    # do not allow a user to login if they are already logged in
    if authenticated_userid(request):
        return HTTPForbidden()

    loc = request.route_url('login', _query=(('next', request.path),))
    return HTTPFound(location=loc)