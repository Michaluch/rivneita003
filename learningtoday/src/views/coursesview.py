from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    authenticated_userid,
)

from learningtoday.src.models import User
from learningtoday.src.models import Course
from learningtoday.src.models import CurCategory
from securityview import (
    session_decorator,
)
from learningtoday.loggingconfig import logger

from ..models import Score


@session_decorator
class Profile(object):
    def __init__(self, request):
        self.request = request

    @view_config(
        route_name='courses',
        request_method='GET',
        renderer='courses.jinja2',
    )
    def profile(self):
        """Gets loged user profile details from db
        This function gets currently logged user details from db and put it
        into the profile template
        """
        categories_list = CurCategory().get()
        courses_list = []
        for course in Course().get():
            # overwriting id's with names
            course['author'] = '{f_name} {l_name}'.format(**User().get_one(id=course['author_id']))
            course['category'] = '{name}'.format(**CurCategory().get_one(id=course['category_id']))
            course['color'] = '{color}'.format(**CurCategory().get_one(id=course['category_id']))
            courses_list.append(course)

        return {"courses_list": courses_list}

    @view_config(
        route_name='about',
        request_method='GET',
        renderer='about.jinja2',
    )
    def about(self):
        return {}

    @view_config(
        route_name='terms',
        request_method='GET',
        renderer='terms.jinja2',
    )
    def terms(self):
        return{}


