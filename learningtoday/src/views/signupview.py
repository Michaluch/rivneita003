from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.security import (
    remember,
    authenticated_userid,
)
from learningtoday.src.models import User
from learningtoday.src.views.securityview import (
    logged_in_users,
)

from learningtoday.loggingconfig import security_logger

class SignUp(object):
    def __init__(self, request):
        self.request = request

    @view_config(
        route_name='signup',
        renderer='develop_signup.jinja2',
        request_method='GET',
    )
    def signup(self):
        if authenticated_userid(self.request):
            return HTTPFound(location=self.request.application_url)
        return {'message': 'some error in signup_get'}

    @view_config(
        route_name='signup',
        renderer='json',
        request_method='POST',
    )
    def signup_post(self):
        security_logger.info('IP: %s, who: %s. registering new user' % (self.request.remote_addr, 
                                                                        self.request.params.get('login', '')))
        user_details = dict()
        if User().get_one(login=self.request.params['login']):
            security_logger.error('can not register new user')
            print "login"
            return {'login_exists': 'User with login: {}, already exists.'.format(self.request.params['login'])}
        if User().get_one(email=self.request.params['email']):
            security_logger.error('can not register new user')
            print "email"
            return {'email_exists': 'User with email: {}, already exists.'.format(self.request.params['email'])}
        # Default values for role and status

        user_details['role_id'] = 3
        user_details['status_id'] = 1

        for key in self.request.params:
            if len(self.request.params[key]) >= 1:
                if key == 'hash':
                    user_details['password'] = self.request.params[key]
                elif key in ('login', 'f_name', 'l_name', 'email'):
                    user_details[key] = self.request.params[key]
                elif key in ('id', 'status_id', 'role_id'):
                    user_details[key] = int(self.request.params[key])
            else:
                del user_details['status_id']
                del user_details['role_id']
                security_logger.error('can not register new user. recieved data not correct')
                return {'message': 'Please fill all the fields'}

        if User(**user_details).add():
            headers = remember(self.request, self.request.params['login'])
            print 'h'
            print headers
            logged_in_users[self.request.params['login']] = \
                headers[0][1][headers[0][1].find('"') + 1:headers[0][1].find('";')]
            security_logger.info('new user added')

            print self.request.application_url
            self.request.response.headerlist.extend(headers)
            return {'redirect_to': self.request.application_url}
        else:
            security_logger.error('can not add new user')
            return {'err_mess': 'Some unknown error occurred, we will be working on it'}
