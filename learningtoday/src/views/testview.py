from pyramid.view import view_config
from pyramid.security import authenticated_userid
from ..models import (
    Lecture,
    Tests,
    CurCategory,
    Course,
    User
)


class Test(object):
    def __init__(self, request):
        self.request = request

    @staticmethod
    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        renderer='develop_tests.jinja2',
    )
    def base():
        """This view receives no parameters, it responds to GET request
        Returns:
            - available categories @type list
        """
        categories = CurCategory().get()
        return {'categories': categories}

    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        renderer='json',
        request_param='action=select_course',
    )
    def select_course(self):
        """This view receives one parameter:
            category_id @type str
        it responds to GET request
        Returns:
            - available courses @type list
        """
        author_id = User().get_one(login=authenticated_userid(self.request))['id']
        category_id = int(self.request.params.get('category_id'))
        courses = Course().get(category_id=category_id, author_id=author_id)
        return {'courses': courses}

    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        renderer='json',
        request_param='action=select_lecture',
    )
    def select_lecture(self):
        """This view receives one parameter:
            course_id @type str
        it responds to GET request
        Returns:
            - available lectures @type list
        """
        course_id = int(self.request.params.get('course_id'))
        lectures = Lecture().get(course_id=course_id)
        lectures_with_tests = []
        for lecture in lectures:
            if Tests().check_for_lecture(lecture.get('id')):
                lectures_with_tests.append(lecture)
        return {'lectures': lectures_with_tests}

    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        renderer='json',
        request_param='action=select_lecture_for_creation',
    )
    def select_lecture_for_creation(self):
        """This view receives one parameter:
            course_id @type str
        it responds to GET request
        Returns:
            - lectures that doesn't have tests @type list
        """
        course_id = int(self.request.params.get('course_id'))
        lectures = Lecture().get(course_id=course_id)
        lectures_without_tests = []
        for lecture in lectures:
            if not Tests().check_for_lecture(lecture.get('id')):
                lectures_without_tests.append(lecture)
        return {'lectures': lectures_without_tests}

    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        renderer='json',
        request_param='action=has_final',
    )
    def has_final(self):
        """This view receives one parameter:
            course_id @type str
        it responds to GET request
        Returns:
            - boolean True if course already has final exam
        """
        course_id = int(self.request.params.get('course_id'))
        if Tests().has_final(course_id=course_id):
            return {'status': True}
        return {'status': False}

    @staticmethod
    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        renderer='develop_add_test.jinja2',
        request_param='action=add_test',
    )
    def return_add_test_template():
        """This view receives no parameters, it responds to GET request
        Returns:
            - available categories @type list
        """
        categories = CurCategory().get()
        return {'categories': categories}

    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        renderer='develop_view_test.jinja2',
        request_param='action=view_test',
    )
    def return_view_test_template(self):
        """This view receives no parameters, it responds to GET request
        Returns:
            - available categories @type list
        """
        if 'course' in self.request.params:
            course_id = int(self.request.params['course'])
            lectures_in_course = Lecture().get(course_id=course_id)
            lecture_ids = []
            for lecture in lectures_in_course:
                lecture_ids.append(lecture['id'])
            #print many_lectures
            lectures = []
            for lecture_id in lecture_ids:
                #print lecture_id
                test = Tests().read(lecture_id=lecture_id)

                if test:
                    lecture = Lecture().get_one(id=lecture_id)
                    test['lecture_name'] = lecture['title']
                    lectures.append(test)
            #print result
            return {'lectures': lectures}
        elif 'lecture' in self.request.params:
            lecture_id = int(self.request.params['lecture'])
            #print lecture_id
            result = []
            test = Tests().read(lecture_id=lecture_id)
            if test:
                test['lecture_name'] = Lecture().get_one(id=lecture_id)['title']
                result.append(test)
            #print result
            return {'lectures': result, 'status': True}
        else:
            return {'status': False}

    @view_config(
        route_name='tests',
        permission='create',
        request_method='POST',
        renderer='json',
    )
    def create(self):
        """This view responds to POST request to add a test to database,
        Requires in request.json_body @type dict:
            - lecture_id @type str
            - name @type str
            - description @type str
            - active @type boolean
            - final @type boolean
            - questions (list containing dictionaries in format:
                {question: @type string, answers: [{answer: @type string, correct: @type boolean}, {etc}]}
        Returns:
            - True if creation of new Test was successful
            - else False
        """
        data = self.request.json_body
        if Tests().create(
                lecture_id=data.get('lecture_id'),
                name=data.get('name'),
                description=data.get('description'),
                active=data.get('active'),
                final=data.get('final'),
                questions=data.get('questions')
        ):
            return {'status': True}
        return {'status': False}

    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        request_param='action=get_by_lecture',
        renderer='json',
    )
    def read(self):
        """This view responds to GET request with request parameter 'action=get_by_lecture'
        Requires in request.params:
            - lecture_id: @type str

        Makes request to DB and expects list as response

        Returns:
            - list with tests in the following format:
                - lecture_id (str)
                - name (str)
                - description (str)
                - active (boolean)
                - final (boolean)
                - questions (list containing dictionaries in format:
                    {question: @type string, answers: [{answer: @type string, correct: @type boolean}, {etc}]}
        """
        lecture_id = self.request.params.get('lecture_id')
        response = Tests().read(lecture_id=lecture_id)
        return {'response': response}

    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        request_param='action=cancel_edit',
        renderer='develop_test_cancel_edit.jinja2',
    )
    def cancel_edit(self):
        """This view responds to GET request with request parameter 'action=get_by_lecture'
        Requires in request.params:
            - lecture_id: @type str

        Makes request to DB and expects list as response

        Returns:
            - list with tests in the following format:
                - lecture_id (str)
                - name (str)
                - description (str)
                - active (boolean)
                - final (boolean)
                - questions (list containing dictionaries in format:
                    {question: @type string, answers: [{answer: @type string, correct: @type boolean}, {etc}]}
        """
        lecture_id = self.request.params.get('lecture_id')
        test_details = Tests().read(lecture_id=lecture_id)
        test_details['lecture_name'] = Lecture().get_one(id=lecture_id)['title']
        return {'test_details': test_details}

    @view_config(
        route_name='test',
        permission='edit',
        request_method='PUT',
        renderer='json',
    )
    def update(self):
        """This view responds to PUT Request
        Requires in request.json_body @type dict:
            - lecture_id @type str
            - name @type str
            - description @type str
            - active @type boolean
            - final @type boolean
            - questions @type list with dictionaries
        Returns:
            - status: True (on success)
            - status: False (on failure)
        """
        data = self.request.json_body
        if Tests().update(
                lecture_id=data.get('lecture_id'),
                name=data.get('name'),
                description=data.get('description'),
                active=data.get('active'),
                final=data.get('final'),
                questions=data.get('questions')
        ):
            return {'status': True}
        return {'status': False}

    @view_config(
        route_name='test',
        permission='edit',
        request_method='DELETE',
        renderer='json',
    )
    def delete(self):
        """This view responds to DELETE Request
        Requires in request.json_body:
            - lecture_id: @type str

        Returns:
            - status: True (on success)
            - status: False (on failure)
        """
        lecture_id = self.request.json_body.get('lecture_id')
        if Tests().delete(lecture_id=lecture_id):
            return {'status': True}
        return {'status': False}

    @view_config(
        route_name='tests',
        permission='view',
        request_method='GET',
        request_param='action=search',
        renderer='develop_test_search_results.jinja2',
    )
    def search(self):
        """This view responds to GET Request that contains Request parameter action=search
        Requires in request.params:
            - search_key: @type str
            - search_value: @type str

        Makes request to DB and expects list as response

        Returns:
            - list with tests in the following format:
                - lecture_id (str)
                - name (str)
                - description (str)
                - active (boolean)
                - final (boolean)
                - questions (list containing dictionaries in format:
                    {question: @type string, answers: [{answer: @type string, correct: @type boolean}, {etc}]}
        """
        search_key = self.request.params.get('search_key')
        search_value = self.request.params.get('search_value')
        #print search_key
        #print search_value
        search_results = Tests().search(search_key=search_key, search_value=search_value)

        author_id = User().get_one(login=authenticated_userid(self.request))['id']
        courses = Course().get(author_id=author_id)
        authored_courses_id = [course_id['id'] for course_id in courses]

        result = {'category': {}}
        for test in search_results:
            lecture = Lecture().get_one(id=test['lecture_id'])
            search_result_course_id = lecture['course_id']
            lecture_name = lecture['title']
            #print search_result_course_id
            #print result

            if search_result_course_id in authored_courses_id:
                for course in courses:
                    if course['id'] == search_result_course_id:
                        category_id = course['category_id']
                        course_name = course['name']
                        category_name = CurCategory().get_one(id=category_id)['name']

                if category_name in result['category']:
                    if course_name in result['category'][category_name]['course']:
                        course_lecture = result['category'][category_name]['course'][course_name]['lecture']
                        course_lecture.update({lecture_name: test})
                    else:
                        result['category'][category_name]['course'].update({course_name: {'lecture': {lecture_name: test}}})
                else:
                    result['category'].update({category_name: {'course': {course_name: {'lecture': {lecture_name: test}}}}})
                    #result[category_name] = {course_name: [test]}
        #import pprint
        #print pprint.pprint(search_results)
        #print pprint.pprint(result)

        return {'search_results': result}

    @staticmethod
    @view_config(
        route_name='test',
        permission='edit',
        request_method='GET',
        renderer='json',
        request_param='action=check_permission'
    )
    def check_permission():
        return {}