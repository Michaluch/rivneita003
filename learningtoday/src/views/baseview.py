import json
import math
from pyramid.view import view_config
from learningtoday.src.models import Course
from learningtoday.src.models import CurCategory
from learningtoday.src.models import User
from learningtoday.src.models import Lecture
from learningtoday.src.models import Score
from learningtoday.src.models import Progress
from learningtoday.src.models import Tests

from learningtoday.src.views.securityview import (
    session_decorator,
)

from pyramid.security import (
    authenticated_userid,
)
from learningtoday.loggingconfig import logger

@session_decorator
class Base(object):
    def __init__(self, request):
        self.request = request

    @staticmethod
    @view_config(route_name='home',
                 renderer='home.jinja2')
    def my_view():
        return {'boxes': generate_box()}

    @view_config(route_name='home_search',
                 renderer='json',
                 request_method='GET')
    def search_view(self):
        search_value = self.request.matchdict.get('phrase')
        courses = Course().get(name=search_value, deleted=0)
        for c in Course().get(description=search_value, deleted=0):
            if not c in courses:
                courses.append(c)
        for course in courses:
            color = CurCategory().get_one(id=course['category_id'])['color']
            course['description'] = course['description'][0:300] + '...'
            if authenticated_userid(self.request):
                score = Score().get_one(user_id=User().get_one(login=authenticated_userid(self.request))['id'], course_id=c['id'])
                if score:
                    try:
                        if score['finished']:
                            progress = 1
                        else:
                            progress = (1-len(Progress().get(score_id=score['id'], passtest=-1))/float(len(Lecture().get(course_id=c['id']))))*100
                    except ZeroDivisionError:
                        progress = 0
                    course['subscribe'] = 'Your progress is ' + str(int(progress)) + ' %' 
                    course['clas'] = "yes"
                else:
                    course['subscribe'] = 'Subscribe now!'
                    course['clas'] = "no"
            else:
                course['subscribe'] = 'Subscribe now!'
                course['clas'] = "no"
            course['len'] = len(Score().get(course_id=course['id']))
            course['color'] = color
        if len(courses):
            return {'boxes': courses}


    @view_config(route_name='category_courses',
                 renderer='json',
                 request_method='GET')
    def category_view(self):
        search_value = self.request.matchdict.get('id')
        courses = []
        color = CurCategory().get_one(id=search_value)['color']
        for c in Course().get(category_id=search_value, deleted=0):
            c['color'] = color
            c['description'] = c['description'][0:300] + '...'
            if authenticated_userid(self.request):
                score = Score().get_one(user_id=User().get_one(login=authenticated_userid(self.request))['id'], course_id=c['id'])
                if score:
                    try:
                        if score['finished']:
                            progress = 1
                        else:
                            progress = (1-len(Progress().get(score_id=score['id'], passtest=-1))/float(len(Lecture().get(course_id=c['id']))))*100
                    except ZeroDivisionError:
                        progress = 0
                    c['subscribe'] = 'Your progress is ' + str(int(progress)) + ' %' 
                    c['clas'] = "yes"
                else:
                    c['subscribe'] = 'Subscribe now!'
                    c['clas'] = "no"
            else:
                c['subscribe'] = 'Subscribe now!'
                c['clas'] = "no"
            c['len'] = len(Score().get(course_id=c['id']))
            courses.append(c)
        if len(courses):
            return {'boxes': courses}


def generate_box():
    res = []
    for l in CurCategory().get():
        if l['deleted']==0:
            sec_class = ''.join(['brick', str(l['id'])])
            div_id = ''.join(['brick', str(l['id'])])
            link = ''.join(['Link', str(l['id'])])
            course_name = l['name']
            color = l['color']
            res.append({'sec_class': sec_class,
                        'div_id': div_id,
                        'link': link,
                        'name': course_name,
                        'color':color
                        })
    return res


#test views class
#score and progress
@session_decorator
class ScoreProgress(object):
    '''
        This class works with user score and user progress
    '''

    def __init__(self, request):
        self.request = request
        self.lecture = Lecture()
        self.course = Course()
        self.score = Score()
        self.progress = Progress()
        self.user = User()
        self.test = Tests()

    @staticmethod
    @view_config(renderer='t_subscribe.jinja2',
                 request_method='GET',
                 route_name='score_progress')
    def render_subscribe():
        return {}

    @view_config(renderer='json',
                 request_method='POST',
                 route_name='score_progress',
                 request_param='action=subscribe',
                 )
    def subscribe(self):
        # Params:
        #     course_id - course id
        # get course id
        course_id = self.request.params.get('course_id')
        # check user and course id
        if course_id is None or not authenticated_userid(self.request):
            return {'result': 'error', 'error': 'course id not recieved or can not get user login'}
            # get and check user and course
        get_user = self.user.get_one(login=authenticated_userid(self.request))
        get_course = self.course.get_one(id=course_id)
        if not get_user or not get_course:
            return {'result': 'error', 'error': 'can not get user or course in adding score'}
            # check if score exist
        if self.score.get_one(course_id=course_id, user_id=get_user['id'], finished=0):
            return {'result': 'error', 'error': 'user score already exist'}
        self.score.course_id = course_id
        self.score.user_id = get_user['id']
        self.score.finished = 0
        lectures_in_course = self.lecture.get(course_id=course_id)
        has_final_exam = False
        progress_list = []
        for lecture in lectures_in_course:
            progress_item = {}
            test = Tests().read(lecture_id=lecture['id'])
            if test:
                if test['final']:
                    has_final_exam = True
                    progress_item['finalexam'] = 1
                else:
                    progress_item['finalexam'] = 0
                progress_item['lecture_id'] = lecture['id']
                progress_item['passtest'] = -1
                progress_list.append(progress_item)
        if not has_final_exam:
            return {'result': 'error', 'error': 'course do not have final exam'}
        # trying to add new score
        if not self.score.add():
            return {'result': 'error', 'error': 'can not add user score'}
        for item in progress_list:
            self.progress.finalexam = item['finalexam']
            self.progress.lecture_id = item['lecture_id']
            self.progress.score_id = self.score.id;
            self.progress.passtest = item['passtest']
            self.progress.add()
        del(progress_list)
        del(progress_item)
        del(lectures_in_course)   
        return {'result': 'success'}
             
    

    @view_config(renderer='json',
                 request_method='PUT',
                 route_name='score_progress',
                 request_param='action=complete_test',
                 )
    def complete_test(self):
        '''
         Params:
             test_answers - test answers in format:
                 {
                     lecture_id - lecture id (int),
                     questions - questions and answers in format:
                        'questions': [
                            {
                                'question': 'question text',
                                'answers': [
                                    {
                                        'answer': 'answer text',
                                        'correct': 'True/False',
                                    },
                                    ...
                                ]
                            },
                            ...
                        ]                 
                 }
        '''      
        test_answers = json.loads(self.request.params.get('test_answers'))
        lecture_id = int(test_answers['lecture_id'])
        # grade = self.request.params.get('grade', False)
        # if not lecture_id or not grade or (grade > 100 and grade < 0):
        #     return {'result': 'error', 'error': 'request parameters lecture_id or grade not set or grade not in range [0, 100]'}
        lecture = self.lecture.get_one(id=lecture_id)
        course = self.course.get_one(id=lecture['course_id'])
        user = self.user.get_one(login=authenticated_userid(self.request))
        if not lecture or not course or not user or test_answers is None:
            return {'result': 'error', 'error': 'can not found required params'}
        score = self.score.get_one(course_id=course['id'], user_id=user['id'], finished=0)
        if not score:
            return {'result': 'error', 'error': 'can not get score record or score is finished'}
        progress = self.progress.get_one(score_id=score['id'], lecture_id=lecture_id)
        if progress['passtest'] > -1:
            return {'result': 'error', 'error': 'test already passed'}

        # calculating test grade
        grade = Tests().get_test_grade(test_answers)

        self.progress.id = progress['id']
        self.progress.passtest = grade
        self.progress.update()
        if progress['finalexam'] == 1:
            self.score.id = score['id']
            self.score.finished = 1
            self.score.update()
        return {'result': 'success',
                'grade': grade}


    @view_config(renderer='json',
                 request_method='GET',
                 route_name='score_progress',
                 request_param='action=last_score_result'
                 )
    def last_score_result(self):
        '''
            Params:
                course_id - course id
        '''
        def _round_score_result(val):
            return int(round(val))
        finished = self.request.params.get('finished', 1)
        course_id = self.request.params.get('course_id')
        if not course_id:
            return {'result': 'error', 'error': 'request param course_id not set'}
        get_user = self.user.get_one(login=authenticated_userid(self.request))
        last_score = self.score.get_one(course_id=course_id, user_id=get_user['id'], finished=finished, order_by='id',
                                        asc_desc='desc', limit=1)
        score_result = 0
        progress_list = self.progress.get(score_id=last_score['id'])
        tests_count = len(progress_list) - 1
        for item in progress_list:
            item['passtest'] = 0 if item['passtest'] == -1 else item['passtest']
            if item['finalexam'] == 1:
                score_result += _round_score_result(item['passtest'] * 0.6)
            else:
                score_result += _round_score_result((item['passtest'] * 0.4) / tests_count)
        if score_result > 100:
            score_result = 100
        score_result = 0 if score_result == -1 else score_result
        return {'result': 'success', 'score_result': score_result}

    @view_config(renderer='json',
                 request_method='GET',
                 route_name='score_progress',
                 request_param='action=all_scores_result'
                 )
    def all_scores_result(self):
        '''
            Params:
                course_id - course id
        '''
        def _round_score_result(val):
            return int(round(val))

        course_id = self.request.params.get('course_id')
        if not course_id:
            return {'result': 'error', 'error': 'request param course_id not set'}
        get_user = self.user.get_one(login=authenticated_userid(self.request))
        scores_list = self.score.get(course_id=course_id, user_id=get_user['id'], finished=1)
        if not get_user or not scores_list:
            return {'result': 'error', 'error': 'can not get user or user scores list (course must be finished)'}
        scores_result_list = [0]
        for score in scores_list:
            progress_list = self.progress.get(score_id=score['id'])
            tests_count = len(progress_list) - 1
            for item in progress_list:
                if item['finalexam'] == 1:
                    score_result += _round_score_result(item['passtest'] * 0.6)
                else:
                    score_result += _round_score_result((item['passtest'] * 0.4) / tests_count)
            if score_result > 100:
                score_result = 100
            scores_result_list.append({score_id: score['id'], finish_date: score['finish_date'], score_result: score_result})
        return {'result': 'success', 'score_result': scores_result_list}
