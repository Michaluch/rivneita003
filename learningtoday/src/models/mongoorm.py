# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 06:32:24 2013

@author: Mykhailo Ome'lchuk
"""

from pymongo import MongoClient


from learningtoday.loggingconfig import db_logger

class Mongoorm(object):
    def connect_to_db(self, db_name="learningtoday"):
        """(str) -> MongoDB_connector
        Connection to MongoDB database.

        Create a new connection to a single MongoDB instance at host:port.
        The resultant client object has connection-pooling built in.
        Parametr db_name is string, name of databes
        """
        try:
            connection = MongoClient(host="mongodb://localhost", port=27017)
            db = connection[db_name]
            return db
        except Exception as e:
            db_logger.critical('can not operate with mongo DB: %s' % (e,))



    def case_collection(self, db_name, collection_name):
        """(str, str) -> MongoDb_Collection
        return MongoDB Collection cursor
        """
        try:
            return self.connect_to_db(db_name)[collection_name]
        except Exception as e:
            db_logger.critical('can not get mongo collection cursor: %s' % (e,))


def connect_to_db(db_name=''):
    """(str) -> MongoDB_connector
    Connection to MongoDB database.

    Create a new connection to a single MongoDB instance at host:port.
    The resultant client object has connection-pooling built in.
    Parametr db_name is string, name of databes
    """
    if not db_name:
        return 'Please provide db name.'
    try:
        connection = MongoClient(host="mongodb://localhost", port=27017)
        db = connection[db_name]
        return db
    except Exception, e:
        print "Error connecting to MongoDB: %s" % e


def get_cursor(db_name='', collection_name=''):
    """(str, str) -> MongoDb_Collection
    return MongoDB Collection cursor
    """
    error = ''
    if not db_name:
        error += 'Please provide db name.\n'
    if not collection_name:
        error += 'Please provide collection name.\n'
    if error:
        return error

    try:
        cursor = connect_to_db(db_name)[collection_name]
        return cursor
    except Exception, e:
        print "Error creating collection cursor: %s" % e


if __name__ == '__main__':
    pass
