"""
    This module consist MySQL model for user progress
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class Progress(DbObject):
    """
        This is MySQL model for user progress
    """

    def __init__(self, **kwargs):
        """
            Args:
                id - user progress id (int)
                lecture_id - lecture what belongs to progress (int)
                score_id - score of progress (int)
                passtest - course passing percent (int)
                attempts - number attempts of passing course (int)
                finalexam - is finalexam (int)
        """
        self.id = kwargs.get("id")
        self.score_id = kwargs.get('score_id')
        self.lecture_id = kwargs.get("lecture_id")
        self.passtest = kwargs.get("passtest")
        self.finalexam = kwargs.get("finalexam")
        
    @DbOps("courses")
    def add(self, cur):
        """
            This method add new user progress
            id not required
            Constraints:
                lecture_id
                score_id
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_foreign_id(cur, "lectures", self.lecture_id)
        if not ret: 
            return ret
        ret = self._check_foreign_id(cur, "score", self.score_id)
        if not ret: 
            return ret
        return self._add(cur, "progress")
    
    @DbOps("courses")
    def update(self, cur):
        """
            This method update user progress
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.lecture_id is not None:
            ret = self._check_foreign_id(cur, "lectures", self.lecture_id)
            if not ret: 
                return ret
        if self.score_id is not None:
            ret = self._check_foreign_id(cur, "score", self.score_id)
            if not ret: 
                return ret
        return self._update(cur, "progress")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """
            This method get list of users progresses
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "progress"
        return self._get(cur, **kwargs)

    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        """
            This method get one user progress
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "progress"
        return self._get(cur, True, **kwargs)
    
"""def check_progress():
    pg = Progress(id=0, lecture_id=2, passtest=10, attempts=2, finalexam=1)
    print "adding progress"
    print pg.add()
    print "get many"
    print pg.get(finalexam=0)
    print pg.get_one(id=4)
    print "updating"
    pg.passtest = 200
    print pg.update()
    
check_progress()"""