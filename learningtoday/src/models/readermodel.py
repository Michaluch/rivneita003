"""
    This module consist MySQL model for reader
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class Reader(DbObject):
    """
        This is MySQL model for reader
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - reader id (int)
                f_name - first reader name (str)
                l_name - last reader name (str)
                reg_begin - register date (datetime)
                reg_end - date when registration end (datetime)
                status_id - reader status id (int)
        """
        self.id = kwargs.get("id")
        self.f_name = kwargs.get("f_name")
        self.l_name = kwargs.get("l_name")
        self.reg_begin = kwargs.get("reg_begin")
        self.reg_end = kwargs.get("reg_end")
        self.status_id = kwargs.get("status_id")
        
    @DbOps("library")
    def add(self, cur):
        """
            This method add new reader
            id not required
            Constraints:
                name - unique
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_foreign_id(cur, "status", self.status_id)
        if not ret: 
            return ret
        return self._add(cur, "readers")
        
    @DbOps("library")
    def get(self, cur, **kwargs):
        """
            This method get list of readers
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "readers"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        """
            This method get one reader
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "readers"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        """
            This method update reader
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.status_id is not None:
            ret = self._check_foreign_id(cur, "status", self.status_id)
            if not ret: 
                return ret
        return self._update(cur, "readers")
    
"""def check_reader():
    w = Reader(id=0, f_name="name 1", l_name="lname 2", reg_begin=datetime(2011, 10, 1), reg_end=datetime(2011, 12, 1), status_id=1)
    print "adding new"
    print w.add()
    print "get many"
    print w.get()
    print "get one"
    print w.get_one(login="login2")
    w.f_name = "".join([w.f_name, "77"])
    print "updating"
    print w.update()

check_reader()"""