'''
    This moule consist model of books recommendations

    recommendations configuration:
    db: learningtogay
    collection: recommendations

    recommendation = {
        "course_id": "course id (int)",
        "books_levels":{
            "level1 (int)": [book_id1, book_id2, ...],
            "level2 (int)": [book_id3, book_id4, ...],
            ...
        }
    }

'''

from learningtoday.src.models.mongoorm import Mongoorm

class Recommendations(Mongoorm):
    '''
        This class is recommendations model
    '''
    def __init__(self):
        # super(Recommendations, self).__init__()
        db = 'learningtoday'
        collection = 'recommendations'
        self.col_cur = self.case_collection(db, collection)

    def _check_course_existanse(self):
        pass
        # add check course existence here: True/False

    def add(self, course_id, books_levels):
        '''
            This method add new recommendation
            Args:
                course_id - course id (int)
                books_levels - dict of books levels (value as list of books)
        '''
        if self.col_cur.find({'course_id': int(course_id)}).count():
            return False
        if self.col_cur.insert({'course_id': int(course_id), 'books_levels': books_levels}):
            return True
        else: 
            return False

    def add_update_book_level(self, course_id, books_levels):
        '''
            This mehtod add new books levels to recommendation
            Args:
                course_id - course id (int)
                books_levels - dict of books levels (value as list of books)
        '''
        if not self.col_cur.find({'course_id': int(course_id)}).count():
            return False
        add_update_dict = {}
        for key, value in books_levels.iteritems():
            add_update_dict['books_levels.' + key] = value
        print add_update_dict
        if self.col_cur.update({'course_id': int(course_id)}, {'$set': add_update_dict}):
            return True
        else: 
            return False

    def get_recommendation(self, course_id):
        '''
            This method returns one recommendation by course id
            Args:
                course_id - recommendation course id (int)
            Returns:
                recommendation - json object (python dict)
        '''
        return self.col_cur.find_one({'course_id': int(course_id)}) 

    def remove_book_level(self, course_id, book_level):
        '''
            This method removes one book level
            Args:
                course_id - course id (int)
                book_level - book level (str, i.e. '45')
                or '40': [])
        '''
        if not self.col_cur.find({'course_id': int(course_id)}).count():
            return False
        book_level = 'books_levels.' + book_level
        if self.col_cur.update({'course_id': int(course_id)}, { '$unset': {book_level: '',}}):
            return True
        else:
            return False

    def remove(self, course_id):
        '''
            This method removes one recommendation
            Args:
                course_id - removed recommendation course id (int)
        '''
        if self.col_cur.remove({'course_id': int(course_id)}):
            return True
        else:
            return False
                
'''rec = Recommendations()
books_levels = {
    '20': [1, 2, 3],
    '40': [1, 2, 3],
}
print rec.add(1, books_levels)
print rec.get_recommendation(1)
new_books_levels = {
    '50': [1, 2, 3],
    '60': [1, 2],
}
print '--------------------------------------------------------'
print rec.add_update_book_level(1, new_books_levels)
print rec.get_recommendation(1)
new_books_levels = {
    '50': [1, 5],
    '60': [1, 4],
}
print '--------------------------------------------------------'
print rec.add_update_book_level(1, new_books_levels)
print rec.get_recommendation(1)
print '--------------------------------------------------------'
print rec.remove_book_level(1, '60')
print rec.get_recommendation(1)
# print rec.remove(2)'''