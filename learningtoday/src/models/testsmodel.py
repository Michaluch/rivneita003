from mongoorm import connect_to_db

from pyramid.security import (
    Allow,
    ALL_PERMISSIONS,
)
from ..models import User, Lecture

"""""""""""""""""""""
recommendations configuration:
db: learningtogay
collection: recommendations

recommendation = {
    "course_id": "course id (int)"
    "books_level":{
        "level1 (int)": [book_id1, book_id2, ...],
        "level2 (int)": [book_id3, book_id4, ...],
        ...
    }
}
"""""""""""""""""""""

"""""""""""""""""""""
current configuration:
db: learningtogay
collection: tests

tests structure:
test = {
    "lecture_id": "lecture id (int)"
    "name": "test name"
    "description": "test description"
    "final": "yes/no"
    "deleted": "is test deleted (int)"
    "questions": [
        {
            "question": "text of the question 1"
            "answers": [
                {
                    "answer": "text of the answer 1",
                    "correct": "is this correct answer? (True/False) 
                },
                {
                    "answer": "text of the answer 2",
                    "correct": "is this correct answer? (True/False) 
                }
            ]
        },
        {
            "question": "text of the question 2"
            "answers": [
                {
                    "answer": "text of the answer 1",
                    "correct": "is this correct answer? (True/False) 
                },
                {
                    "answer": "text of the answer 2",
                    "correct": "is this correct answer? (True/False) 
                }
            ]
        }
        
    ]
}
"""""""""""""""""""""


class Tests(object):
    @property
    def __acl__(self):
        author = User().get_one(id=self.author_id).get('login')
        return [
            (Allow, author, ALL_PERMISSIONS),
        ]

    def __init__(self, **kwargs):
        """self.connection creates connection with database
        self.collection creates cursor to work with tests collection
        """
        self.__name__ = None
        self.__parent__ = None

        self.connection = connect_to_db(db_name='courses')
        self.collection = self.connection['tests']

        self.lecture_id = kwargs.get('lecture_id')
        self.name = kwargs.get('name')
        self.author_id = kwargs.get('author_id')

    def create(
            self,
            lecture_id=None,
            name=None,
            description=None,
            active=None,
            final=None,
            questions=None
    ):
        """This method must receive following parameters:
            - lecture_id (str)
            - name (str)
            - description (str)
            - active (boolean)
            - final (boolean)
            - questions (list containing dictionaries with questions and answers)

        we try to create Test in DB,
        if it doesn't exist we will create it,
        if it does exist we will return False

        Returns True if creation of new Test was successful
        """
        # Uncomment line below in need to remove collection
        #self.collection.remove()
        incoming_values = {
            'lecture_id': lecture_id,
            'name': name,
            'description': description,
            'active': active,
            'final': final,
            'questions': questions
        }
        #print incoming_values
        for k, v in incoming_values.items():
            if k not in ('active', 'final') and not v:
                print "key: {}, doesn't have a value".format(k)
                return False

        #import pprint
        #pprint.pprint(list(self.collection.find({'name': name})))
        try:
            if not list(self.collection.find(
                {
                    "lecture_id": int(lecture_id),
                }
            )):
                self.collection.insert(
                    {
                        "lecture_id": int(lecture_id),
                        "name": name,
                        "description": description,
                        "active": active,
                        "final": final,
                        "questions": questions
                    }
                )
            else:
                print 'Lecture: {}, already has test with name: {}'.format(lecture_id, name)
                return False

            return True
        except Exception, e:
            print 'Failed to CREATE test: %s' % e
            return False

    def read(self, lecture_id=0):
        """This method must receive lecture_id parameter
        Returns list with tests on success, and False on failure
        """
        if not lecture_id:
            return False
        try:
            result = self.collection.find_one({'lecture_id': int(lecture_id)}, {'_id': 0})
            return result
        except Exception, e:
            print 'Failed to READ tests by lecture_id: %s' % e
            return False

    def _questions_to_dol(self, answers=None):
        '''
            answers - list of answers
            dol - dict of list
        '''
        dol = {}
        if answers is None:
            return dol
        for question in answers:
            # dol - dict of list {'question name 1': ['correct 1', 'correct 2',...],
            #                     'question name 2': ['correct 1', 'correct 2',...]}
            list_of_correct = []
            for answer in question['answers']:
                list_of_correct.append(answer['correct'])
            dol[question['question']] = list_of_correct
        return dol

    def _check_test_answers(self, user_answers=None, correct_answers=None):
        '''
            Returns:
                grade - [0: 100]
        '''
        question_amount = len(user_answers)
        correct_answered = 0
        for question in user_answers:
            user_answers_list = user_answers[question]
            correct_answers_list = correct_answers[question]
            if user_answers_list == correct_answers_list:
                correct_answered += 1
        # calculating persentage of correct answers
        return int(round((correct_answered/float(question_amount))*100))

    def get_test_grade(
                self,
                user_answers=None):
        '''
            Args:
                user_answers - test answers in format:
                 {
                     lecture_id - lecture id (int),
                     questions - questions and answers in format:
                        'questions': [
                            {
                                'question': 'question text',
                                'answers': [
                                    {
                                        'answer': 'answer text',
                                        'correct': 'True/False',
                                    },
                                    ...
                                ]
                            },
                            ...
                        ]                 
                 }
            Return:
                False - if false
                qrade - if OK (int) [0:100]
        '''
        if not user_answers:
            return False

        correct_answers = list(self.collection.find( {
            'lecture_id': int(user_answers['lecture_id'])
            }, 
            {
            '_id': 0,
            'description': 0,
            'name': 0,
            'lecture_id': 0,
            'final': 0,
            } ))[0]
        quiz_active = correct_answers['active']
        del(correct_answers['active'])
        del(user_answers['lecture_id'])
        grade = self._check_test_answers(
            user_answers=self._questions_to_dol(answers=user_answers['questions']),
            correct_answers=self._questions_to_dol(answers=correct_answers['questions']),
            )
        return grade

    def update(
            self,
            lecture_id=None,
            name=None,
            description=None,
            active=None,
            final=None,
            questions=None,
    ):
        """This method requires following parameters:
            - lecture_id @type str
            - original_test_name @type str
            - name @type str
            - description @type str
            - active @type boolean
            - final @type boolean
            - questions (list containing dictionaries in format:
                {question: @type string, answers: [{answer: @type string, correct: @type boolean}, {etc}]}
        """
        try:
            self.delete(lecture_id=lecture_id)
            self.create(
                lecture_id=lecture_id,
                name=name,
                description=description,
                active=active,
                final=final,
                questions=questions,
            )
            return True
        except Exception, e:
            print 'Failed to EDIT test: %s' % e
            return False

    def delete(self, lecture_id=None):
        """This method requires following parameters:
            - lecture_id (str)
            - name (str)
        It removes test from DB and returns True on success and False on failure
        """
        try:
            self.collection.remove({
                "lecture_id": int(lecture_id),
            })
            #print "Test with, Lecture ID: {}, and Name: {}, Successfully removed!".format(lecture_id, name)
            return True
        except Exception, e:
            print 'Failed to DELETE test: %s' % e
            return False

    def delete_collection(self):
        """This method requires no parameters
        Use only for developing purposes
        It will remove collection
        """
        try:
            self.collection.remove()
            return True
        except Exception, e:
            print 'Failed to DELETE collection: %s' % e
            return False

    def search(self, search_key=None, search_value=None):
        """This method requires two parameters:
            - search_key (str)
            - search_value (str)
        It will search for search_value in the given search_key
        and return list with tests
        """
        if search_key == 'question':
            results = self.collection.find(
                {'questions':
                    {'$elemMatch':
                        {'question':
                            {'$regex': search_value}
                         }
                     }
                 },
                {'_id': 0}
            )
        elif search_key == 'answer':
            results = self.collection.find(
                {'questions':
                    {'$elemMatch':
                        {'answers':
                            {'$elemMatch':
                                {'answer':
                                    {'$regex': search_value}
                                 }
                             }
                         }
                     }
                 },
                {'_id': 0}
            )
        else:
            results = self.collection.find({search_key: {'$regex': search_value}}, {'_id': 0})

        #print list(results)
        return list(results)

    def check(self, answer_to_check=None):
        """This model receives one parameter:
            - answer_to_check @type(dictionary) in the following format:
                - {
                lecture_id: id @type(str or int),
                name: name @type(str),
                questions: {
                    question_name: {answer: None],
                    question_name: {answer: None, answer None, answer None]
                    }
                }
        Return of this function will be same dictionary,
        but answer values will be replaced with True/False booleans
        """
        #answer_to_check = {
        #    'lecture_id': 1,
        #    'name': 'test 3',
        #    'questions': {
        #        'Nihilne te nocturnum praesidium Palati, nihil urbis vigiliae.':
        #            {'Pellentesque habitant morbi tristique senectus et netus.': None, },
        #    }
        #}
        #answer_to_check = {
        #    'lecture_id': 1,
        #    'name': 'test 2',
        #    'questions': {
        #        'Ullamco laboris nisi ut aliquid ex ea commodi consequat.':
        #            {'Paullum deliquit, ponderibus modulisque suis ratio utitur.': None, },
        #        'Fictum,  deserunt mollit anim laborum astutumque!':
        #            {
        #                'Quam temere in vitiis, legem sancimus haerentia.': None,
        #                'Ut enim ad minim veniam, quis nostrud exercitation.': None
        #            },
        #    }
        #}
        query_copy = dict(answer_to_check)

        db_result = list(self.collection.find(
            {
                'lecture_id': query_copy['lecture_id'],
                'name': query_copy['name'],
            },
            {
                '_id': 0,
                'questions': 1
            }
        )
        )
        if db_result:
            for question in query_copy['questions']:
                for item in db_result[0]['questions'][0]:
                    if item['question'] == question:
                        for saved_answer in item['answers']:
                            for sent_answer in query_copy['questions'][question]:
                                if saved_answer['answer'] == sent_answer:
                                    answer_to_check['questions'][question][sent_answer] = saved_answer['correct']
        #print([{k: v} for k, v in answer_to_check['questions'].items()])
        return answer_to_check

    def check_for_lecture(self, lecture_id=None):
        if lecture_id:
            if self.collection.find_one({'lecture_id': int(lecture_id)}):
                return True
            else:
                return False

    def has_final(self, course_id=None):
        if course_id:
            lectures = Lecture().get(course_id=course_id)
            for lecture in lectures:
                test = self.collection.find_one({'lecture_id': int(lecture['id'])}, {'_id': 0, 'final': 1})
                if test:
                    if test['final']:
                        return True
        return False
