#import functools
from datetime import datetime
import MySQLdb as mdb
from MySQLdb.constants import FIELD_TYPE
import ConfigParser

from learningtoday.loggingconfig import db_logger


conv_type = {FIELD_TYPE.LONG: int,
             FIELD_TYPE.TINY: int,
             FIELD_TYPE.SHORT: int}
        
        

def esc(string):
    return mdb.escape_string(string)
    
class DbConf(object):
    _CONFIG_PATH = "etc/db-config.cfg"
    _courses_section = "CoursesDBConfiguration"
    _library_section = "LibraryDBConfiguration"
    _conf = {"host": "", "user": "", "pswd": "", "db": ""}
    
    @staticmethod
    def _get_config_instance():
        config = ConfigParser.SafeConfigParser()
        return config
    
    @staticmethod
    def _get_configuration(section):
        try:
            config = DbConf._get_config_instance()
            config.read(DbConf._CONFIG_PATH)
            DbConf._conf["host"] = config.get(section, "host")
            DbConf._conf["user"] = config.get(section, "user")
            DbConf._conf["pswd"] = config.get(section, "pswd")
            DbConf._conf["db"] = config.get(section, "db")
            return DbConf._conf
        except ConfigParser.Error as e:
            db_logger.critical('can not get MySQL configuration: %s' % (e,))
            return False
            
    @staticmethod    
    def get_courses_conf():
        return DbConf._get_configuration(DbConf._courses_section)
    
    @staticmethod
    def get_libraty_conf():
        return DbConf._get_configuration(DbConf._library_section)
    
class ConnectConf(object):
    
    def __init__(self, dictionary):
        for key in dictionary:
            setattr(self, key, dictionary[key])
    
    def __str__(self):
        s = ""
        for attr in self.__dict__:
            s = "".join([s, "%s " % (self.__dict__[attr])])
        return s


class DbOps(object):
    """select conn_target between 'library' and 'courses'"""
    
    def __init__(self, target="courses"):
        if target == "courses":
            self.conf = ConnectConf(DbConf.get_courses_conf())
            #print hex(id(self.conf.host))
            #print self.conf
        elif target == "library":
            self.conf = ConnectConf(DbConf.get_libraty_conf())
            #print hex(id(self.conf.host))
            #print self.conf
        else:
            return False
    
    def __call__(self, func):
        def wrapper(this=None, *args, **kwargs):
            try:
                #print hex(id(self.conf.host))
                #print self.conf
                con = mdb.connect(host=self.conf.host, user=self.conf.user, passwd=self.conf.pswd, db=self.conf.db, conv=conv_type)
                #pay attention! acces rows not as row[0], row[1] but row["id"], row["name"]
                cur = con.cursor(mdb.cursors.DictCursor)
                cur.execute("set names utf8")
                cur.execute("start transaction")
                if this is None:
                    ret = func(cur, *args, **kwargs)
                else:
                    ret = func(this, cur, *args, **kwargs)
                cur.execute("commit")
            except mdb.Error as e:
                db_logger.critical('can not execute MySQL query: %s' % (e,))
                cur.execute("rollback")
                #return False
            finally:
                if con:
                    con.close()
            return ret
        return wrapper
    
class DbObject(object):
    
    def _check_foreign_id(self, cur, table, value):
        cur.execute("select id from %s where id=%s" % (table, value))
        if cur.rowcount != 1:
            return False
        else:
            return True
    
    def _check_duplicate(self, cur, table, field, value):
        cur.execute("select id from %s where %s='%s'" % (table, field, value))
        if cur.rowcount != 0:
            return False
        else:
            return True
        
    def _check_duplicate_on_update(self, cur, table, field, value):
        cur.execute("select id from %s where %s='%s' and id not in (%s)" % (table, field, value, self.id))
        if cur.rowcount != 0:
            return False
        else:
            return True
        
    def _last_insert_id(self, cur):
        cur.execute("select last_insert_id() as last_id")
        return int(cur.fetchone()["last_id"])
    
    def _update(self, cur, table, check_id = True):
        """all fields but 'id' are optional"""
        sql = "update %s set " % (table)
        if self.id is not None and type(self.id) is int:
            for attr in self.__dict__:
                if not attr.startswith("__"):
                    if attr != "id" or not check_id:
                        if self.__dict__[attr] is not None:
                            #replace(microsecond = 0).isoformat(" ")
                            if type(self.__dict__[attr]) is datetime:
                                sql = "".join([sql, "%s='%s', " % (attr, self.__dict__[attr].replace(microsecond = 0).isoformat(" "))])
                            elif type(self.__dict__[attr]) is str or type(self.__dict__[attr]) is unicode:
                                sql = "".join([sql, "%s='%s', " % (attr, esc(self.__dict__[attr]))])
                            else:
                                sql = "".join([sql, "%s=%s, " % (attr, self.__dict__[attr])])
            sql = sql[:-2]
            sql = "".join([sql, " where id=%s" % (self.id)])
            if cur.execute(sql) == 1:
                return True
            else:
                return False
        else:
            return False
        
    def _get(self, cur, one=False, **kwargs):
        """ """
        limit = ""
        if kwargs.get("limit") is not None:
            limit = "limit %s" % kwargs.get("limit")
            if kwargs.get("limit_to") is not None:
                limit = "".join([limit, ", %s" % (kwargs["limit_to"])])
        order_by = ""
        if kwargs.get("order_by") is not None:
            order_by = "order by %s" % (kwargs.get("order_by"))
            if kwargs.get("asc_desc") is not None:
                order_by = "".join([order_by, " %s" % (kwargs["asc_desc"])])
        sql = "select * from %s" % (kwargs["table"])
        params = [False, False] #where_is, gtth_first_iter
        if kwargs.get("where") is not None and len(kwargs["where"]) != 0:
            sql = "".join([sql, " %s " % (kwargs["where"])])
            params[0] = True
        for key in kwargs:
            if kwargs[key] is not None and key in self.__dict__:
                if not params[0] and not params[1]:
                    sql = "".join([sql, " where "])
                    params[1] = True
                elif params[0] and not params[1]:
                    sql = "".join([sql, " and "])
                    params[1] = True
                if type(kwargs[key]) is datetime:
                    sql = "".join([sql, "%s='%s' and " % (key, kwargs[key].replace(microsecond = 0).isoformat(" "))])
                elif type(kwargs[key]) is str or type(kwargs[key]) is unicode:
                    if not one:
                        sql = "".join([sql, "%s like '%%%s%%' and " % (key, esc(kwargs[key]))])
                    else:
                        sql = "".join([sql, "%s='%s' and " % (key, esc(kwargs[key]))])
                else:
                    sql = "".join([sql, "%s=%s and " % (key, kwargs[key])])
        if params[1]:
            sql = sql[:-5]
        sql = "".join([sql, " %s %s" % (order_by, limit)])
        cur.execute(sql)
        if one and cur.rowcount == 1:
            row = cur.fetchone()
            result = {}
            for field in row:
                result[field] = row[field]
            return result
        elif one and cur.rowcount != 1:
            return False
        if not one:
            result = []
            for row in cur.fetchall():
                item = {}
                for field in row:
                    item[field] = row[field]
                result.append(item)
            return result    
    
    def _add(self, cur, table, check_id=True):
        sql = "insert into %s(" % (table)
        fields = ''
        values = 'values('
        for attr in self.__dict__:
            if self.__dict__[attr] is not None:
                if attr != "id" or not check_id:
                    fields = "".join([fields, "%s, " % (attr)])
                    if type(self.__dict__[attr]) is datetime:
                        values = "".join([values, "'%s', " % (self.__dict__[attr].replace(microsecond = 0).isoformat(" "))])
                    elif type(self.__dict__[attr]) is str or type(self.__dict__[attr]) is unicode:
                        values = "".join([values, "'%s', " % (esc(self.__dict__[attr]))])
                    else:
                        values = "".join([values, "%s, " % (self.__dict__[attr])])
        fields = "".join([fields[:-2], ")"])
        values = "".join([values[:-2], ")"])
        sql = "".join([sql, "%s %s" % (fields, values)])
        ret = cur.execute(sql)
        if ret == 1:
            if check_id:
                self.id = self._last_insert_id(cur)
            return True
        else:
            return False



    

