"""
    This module consist MySQL model for worker role
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class LibRole(DbObject):
    """
        This is MySQL model for worker role
    """

    def __init__(self, **kwargs):
        """
            Args:
                id - worker role id (int)
                name - worker role name (str)
        """
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        
    @DbOps("library")
    def add(self, cur):
        """
            This method add new worker role
            id not required
            Constraints:
                name - unique
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_duplicate(cur, "roles", "name", self.name)
        if not ret:
            return ret
        return self._add(cur, "roles")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        """
            This method get list of workers roles
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "roles"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        """
            This method get one worker role
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "roles"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        """
            This method update worker role
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "roles", "name", self.name)
            if not ret: 
                return ret
        return self._update(cur, "roles")

"""def check_role():
    rl = LibRole(id=5, name="Test role 1")
    print "adding role"
    print rl.add()
    print "get many"
    print rl.get(ordered_by="name", asc_desc="desc")
    print rl.get_one(id=2)
    print "update"
    rl.name = "".join([rl.name, "22"])
    print rl.update()

check_role()"""