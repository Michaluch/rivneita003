"""
    This module consist MySQL model for user
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)
from pyramid.security import (
    Allow,
)


class User(DbObject):
    """
        This is MySQL model for user
    """
    @property
    def __acl__(self):
        return [
            (Allow, self.login, 'view'),
        ]

    def __init__(self, **kwargs):
        """
            Args:
                id - user id (int)
                role_id - user role id (int)
                f_name - user first name (str)
                l_name - user last name (str)
                login - user login (str)
                password - user password (str)
                email - user email (str)
                photo - path to users photo (str)
                status_id - user's status id (int)
        """
        self.id = kwargs.get("id")
        self.role_id = kwargs.get("role_id")
        self.f_name = kwargs.get("f_name")
        self.l_name = kwargs.get("l_name")
        self.login = kwargs.get("login")
        self.password = kwargs.get("password")
        self.email = kwargs.get("email")
        self.photo = kwargs.get("photo")
        self.status_id = kwargs.get("status_id")
        
    @DbOps("courses")
    def add(self, cur):
        """
            This method add new user
            id not required
            Constraints:
                login - unique
                email - unique
                status_id
                role_id
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_duplicate(cur, "users", "login", self.login)
        if not ret: 
            return ret
        ret = self._check_duplicate(cur, "users", "email", self.email)
        if not ret: 
            return ret
        ret = self._check_foreign_id(cur, "status", self.status_id)
        if not ret: 
            return ret        
        ret = self._check_foreign_id(cur, "roles", self.role_id)
        if not ret: 
            return ret        
        return self._add(cur, "users")
        
    @DbOps("courses")
    def check_exist(self, cur):
        """ This mehtod check user existanse
            Requirements:
                login
                password
            Returns:
                False - if user not found
                True - if user found
            """
        cur.execute("select id from users where login='%s' and password='%s'" % 
                    (self.login, self.password))
        if cur.rowcount !=1:
            return False
        else:
            return True
        
    @DbOps("courses")
    def get_info(self, cur):
        """ This mehtod get user info
            Requirements:
                login
                password
            Returns:
                False - if information not found
                Dict - if information found
            """
        cur.execute("select * from users where login='%s' and password='%s'" % 
                    (self.login, self.password))
        if cur.rowcount !=1:
            return False
        else:
            row = cur.fetchone()
            self.id = row["id"]
            self.role_id = row["role_id"]
            self.f_name = row["f_name"]
            self.l_name = row["l_name"]
            self.email = row["email"]
            self.photo = row["photo"]
            self.status_id = row["status_id"]
            return row
        
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """
            This method get list of users
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "users"
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        """
            This method get one user
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "users"
        return self._get(cur, True, **kwargs)
        
    @DbOps("courses")
    def update(self, cur):
        """
            This method update user
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.login is not None:
            ret = self._check_duplicate_on_update(cur, "users", "login", self.login)
            if not ret: 
                return ret
        if self.email is not None:
            ret = self._check_duplicate_on_update(cur, "users", "email", self.email)
            if not ret: 
                return ret
        if self.status_id != None:
            ret = self._check_foreign_id(cur, "status", self.status_id)
            if not ret: 
                return ret
        if self.role_id is not None:
            ret = self._check_foreign_id(cur, "roles", self.role_id)
            if not ret: 
                return ret
        return self._update(cur, "users")

"""def user_check():
    us = User(id=0, role_id=3, f_name="f5", l_name="l'loop5", login="log", password="test_pass", email="e5@e.com", photo="p/t/ph", status_id=1)
    print "adding user:"
    print type(us.id)
    print us.add()
    print "last id = ", us.id, type(us.id)
    print "checking existanse:"
    print us.check_exist()
    print "get one"
    print us.get_one(id=2)
    print "get many users"
    print us.get(l_name="Hell", order_by="email", asc_desc="asc", limit=100):
    print "updating user:"
    us.f_name = "".join([us.f_name, "88"])
    us.login = "".join([us.login, "88"])
    us.status_id = 2
    print us.update()
    
user_check()"""
