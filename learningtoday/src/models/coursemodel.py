"""
    This module consist MySQL model for course
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)
from pyramid.security import (
    Allow,
)
from learningtoday.src.models.usermodel import User


class Course(DbObject):
    """
        This is MySQL model for course
    """
    @property
    def __acl__(self):
        author = User().get_one(id=self.author_id).get('login')
        return [
            (Allow, author, 'edit'),
        ]
    
    def __init__(self, **kwargs):
        """
            Args:
                id - course id (int)
                category_id - course category id (int)
                author_id - course author id (int)
                name - course name (str)
                description - course description (str)
                image - path to course logo (str)
                deleted - course deletion status (int)
        """
        self.__name__ = None
        self.__parent__ = None

        self.id = kwargs.get("id")
        self.category_id = kwargs.get("category_id")
        self.author_id = kwargs.get("author_id")
        self.name = kwargs.get("name")
        self.description = kwargs.get("description")
        self.image = kwargs.get("image")
        self.deleted = kwargs.get("deleted")
        
    @DbOps("courses")
    def add(self, cur):
        """
            This method add new course
            id not required
            Constraints:
                category_id
                author_id
                name - unique
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_foreign_id(cur, "category", self.category_id)
        if not ret: 
            return ret        
        ret = self._check_foreign_id(cur, "users", self.author_id)
        if not ret: 
            return ret        
        ret = self._check_duplicate(cur, "courses", "name", self.name)
        if not ret: 
            return ret        
        return self._add(cur, "courses")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """
            This method get list of courses
            Args:
                arguments with attribute names,
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
                where - have_lectures = True/False (bool)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        where = ""
        if kwargs.get("have_lectures") is not None:
            subquery = "select course_id from lectures where deleted=0"
            if kwargs["have_lectures"]:
                where = "where id in (%s)" % (subquery)
            else:
                where = "where id not in (%s)" % (subquery)
        kwargs["table"] = "courses"
        kwargs["where"] = where
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        """
            This method get one course
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "courses"
        return self._get(cur, True, **kwargs)
    
    @DbOps("courses")
    def update(self, cur):
        """
            This method update course
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.category_id is not None:
            ret = self._check_foreign_id(cur, "category", self.category_id)
            if not ret: 
                return ret
        if self.author_id is not None:
            ret = self._check_foreign_id(cur, "users", self.author_id)
            if not ret: 
                return ret            
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "courses", "name", self.name)
            if not ret: 
                return ret            
        return self._update(cur, "courses")

"""def cource_check():
    cs = Course(id=5, category_id=1, author_id=2, name="test_course_1", description="description_1", image="path/to/im", deleted=1)
    print "adding course:"
    print cs.add()
    print "getting many:"
    print cs.get(order_by="name", limit=10)
    print "get one"
    print cs.get_one(id=2)
    print "updating:"
    cs.name = "".join([cs.name, "999"])
    cs.description = "".join([cs.description, "999"])
    print cs.update()

cource_check()"""
