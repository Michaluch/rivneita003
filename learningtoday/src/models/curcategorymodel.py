"""
    This module consist MySQL model for course category
"""
import re
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class CurCategory(DbObject):
    """
        This is MySQL model for course category
    """

    def __init__(self, **kwargs):
        """
            Args:
                id - course category id (int)
                name - course category name (str)
                deleted - course category deletion ststus (int)
        """
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        if kwargs.get("color") is not None and self._check_color(kwargs["color"]):
            self.color = kwargs["color"]
        else:
            self.color = "#aaaaaa"
        self.deleted = kwargs.get("deleted")

    def _check_color(self, color):
        """
            Private
            This method validate course color
        """
        rexp = r'^#[0123456789abcdef]{3}$|^#[0123456789abcdef]{6}$'
        if re.match(rexp, color) is None:
            return False
        else:
            return True
        
    @DbOps("courses")
    def add(self, cur):
        """
            This method add new course category
            Constraints:
                id
                name - unique
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_duplicate(cur, "category", "name", self.name)
        if not ret: 
            return ret
        ret = self._check_foreign_id(cur, "library_db.category", self.id)
        if not ret: 
            return ret
        return self._add(cur, "category", False)
        
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """
            This method get list of courses categories
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "category"
        return self._get(cur, **kwargs)

    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        """
            This method get one course category
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "category"
        return self._get(cur, True, **kwargs)
    
    @DbOps("courses")
    def update(self, cur):
        """
            This method update course category
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.color is not None and not self._check_color(self.color):
            return False            
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "category", "name", self.name)
            if not ret: 
                return ret
        if self.id is not None:
            ret = self._check_foreign_id(cur, "library_db.category", self.id)
            if not ret: 
                return ret
        return self._update(cur, "category")

"""def check_category():
    ct = CurCategory(id=3, name="Scare category 1")
    print "adding category"
    print ct.add()
    print type(ct.id)
    print "getting many categories"
    print ct.get(order_by="name", asc_desc="desc")
    print "get one"
    print ct.get_one(id=3)
    print "updating category"
    ct.name = "".join([ct.name, "66"])
    print Category(id=ct.id, name="Scare category 166").update()

check_category()"""