from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)


class User(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.role_id = kwargs.get("role_id")
        self.f_name = kwargs.get("f_name")
        self.l_name = kwargs.get("l_name")
        self.login = kwargs.get("login")
        self.password = kwargs.get("password")
        self.email = kwargs.get("email")
        self.photo = kwargs.get("photo")
        self.status_id = kwargs.get("status_id")
        
    @DbOps("courses")
    def add(self, cur):
        ret = self._check_duplicate(cur, "users", "login", self.login)
        if not ret: return ret
        ret = self._check_duplicate(cur, "users", "email", self.email)
        if not ret: return ret
        ret = self._check_foreign_id(cur, "status", self.status_id)
        if not ret: return ret        
        ret = self._check_foreign_id(cur, "roles", self.role_id)
        if not ret: return ret        
        return self._add(cur, "users")
        
    @DbOps("courses")
    def check_exist(self, cur):
        """fields:
            login:
            password:
            """
        cur.execute("select id from users where login='%s' and password='%s'" % 
                    (self.login, self.password))
        if cur.rowcount !=1:
            return False
        else:
            return True
        
    @DbOps("courses")
    def get_info(self, cur):
        """fields:
            login:
            password:
            """
        cur.execute("select * from users where login='%s' and password='%s'" % 
                    (self.login, self.password))
        if cur.rowcount !=1:
            return False
        else:
            row = cur.fetchone()
            self.id = row["id"]
            self.role_id = row["role_id"]
            self.f_name = row["f_name"]
            self.l_name = row["l_name"]
            self.email = row["email"]
            self.photo = row["photo"]
            self.status_id = row["status_id"]
            return row
        
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """kwargs:
            order_by: <field>
            asc_desc: <asc, desc>
            limit:
            -- if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to'
            limit_to:"""
        kwargs["table"] = "users"
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "users"
        return self._get(cur, True, **kwargs)
        
    @DbOps("courses")
    def update(self, cur):
        """ all fields but 'id' are optional"""
        if self.login is not None:
            ret = self._check_duplicate_on_update(cur, "users", "login", self.login)
            if not ret: return ret
        if self.email is not None:
            ret = self._check_duplicate_on_update(cur, "users", "email", self.email)
            if not ret: return ret
        if self.status_id != None:
            ret = self._check_foreign_id(cur, "status", self.status_id)
            if not ret: return ret
        if self.role_id is not None:
            ret = self._check_foreign_id(cur, "roles", self.role_id)
            if not ret: return ret
        return self._update(cur, "users")

"""def user_check():
    us = User(id=0, role_id=3, f_name="f5", l_name="l'loop5", login="log", password="test_pass", email="e5@e.com", photo="p/t/ph", status_id=1)
    print "adding user:"
    print type(us.id)
    print us.add()
    print "last id = ", us.id, type(us.id)
    print "checking existanse:"
    print us.check_exist()
    print "get one"
    print us.get_one(id=2)
    print "get many users"
    print us.get(l_name="Hell", order_by="email", asc_desc="asc", limit=100):
    print "updating user:"
    us.f_name = "".join([us.f_name, "88"])
    us.login = "".join([us.login, "88"])
    us.status_id = 2
    print us.update()
    
user_check()"""


class Course(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.category_id = kwargs.get("category_id")
        self.author_id = kwargs.get("author_id")
        self.name = kwargs.get("name")
        self.description = kwargs.get("description")
        self.image = kwargs.get("image")
        self.deleted = kwargs.get("deleted")
        
    @DbOps("courses")
    def add(self, cur):
        ret = self._check_foreign_id(cur, "category", self.category_id)
        if not ret: return ret        
        ret = self._check_foreign_id(cur, "users", self.author_id)
        if not ret: return ret        
        ret = self._check_duplicate(cur, "courses", "name", self.name)
        if not ret: return ret        
        return self._add(cur, "courses")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """kwargs:
            where: <have_lectures = True/False>
            order_by: <field>
            asc_desc: <asc, desc>
            limit:
            -- if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to'
            limit_to:"""
        where = ""
        if kwargs.get("have_lectures") is not None:
            subquery = "select course_id from lectures where deleted=0"
            if kwargs["have_lectures"]:
                where = "where id in (%s)" % (subquery)
            else:
                where = "where id not in (%s)" % (subquery)
        kwargs["table"] = "courses"
        kwargs["where"] = where
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "courses"
        return self._get(cur, True, **kwargs)
    
    @DbOps("courses")
    def update(self, cur):
        """all fields but 'id' are optional"""
        if self.category_id is not None:
            ret = self._check_foreign_id(cur, "category", self.category_id)
            if not ret: return ret
        if self.author_id is not None:
            ret = self._check_foreign_id(cur, "users", self.author_id)
            if not ret: return ret            
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "courses", "name", self.name)
            if not ret: return ret            
        return self._update(cur, "courses")

"""def cource_check():
    cs = Course(id=5, category_id=1, author_id=2, name="test_course_1", description="description_1", image="path/to/im", deleted=1)
    print "adding course:"
    print cs.add()
    print "getting many:"
    print cs.get(order_by="name", limit=10)
    print "get one"
    print cs.get_one(id=2)
    print "updating:"
    cs.name = "".join([cs.name, "999"])
    cs.description = "".join([cs.description, "999"])
    print cs.update()

cource_check()"""

    
class Lecture(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.course_id = kwargs.get("course_id")
        self.test_id = kwargs.get("test_id")
        self.title = kwargs.get("title")
        self.body = kwargs.get("body")
        self.raite = kwargs.get("raite")
        self.deleted = kwargs.get("deleted")
    
    @DbOps("courses")
    def add(self, cur):
        ret = self._check_foreign_id(cur, "courses", self.course_id)
        if not ret: return ret
        return self._add(cur, "lectures")
        
    @DbOps("courses")
    def update(self, cur):
        if self.course_id is not None:
            ret = self._check_foreign_id(cur, "courses", self.course_id)
            if not ret: return ret
        return self._update(cur, "lectures")
        
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """kwargs:
            where: <have_test=True/False>
            order_by: <field>
            asc_desc: <asc, desc>
            limit:
            -- if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to'
            limit_to:"""
        where = ""
        if kwargs.get("have_test") is not None:
            if kwargs["have_test"]:
                where = "where test_id is not NULL"
            else:
                where = "where test_id is NULL"
        kwargs["table"] = "lectures"
        kwargs["where"] = where
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "lectures"
        return self._get(cur, True, **kwargs)
    
"""def check_lecture():
    l = Lecture(id=7, course_id=1, test_id=0, title="test lecture 1", body="lecture body", raite=1, deleted=0)
    print "adding lecture:"
    print l.add()
    print "getting many:"
    print l.get(order_by="title", asc_desc="desc", limit=100)
    print "get one"
    print l.get_one(id=3)
    print "updating:"
    l.title = "".join([l.title, "11"])
    l.body = "".join([l.body, "11"])
    l.deleted = 1
    print l.update()

check_lecture()"""
    

class Role(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        
    @DbOps("courses")
    def add(self, cur):
        ret = self._check_duplicate(cur, "roles", "name", self.name)
        if not ret: return ret
        return self._add(cur, "roles")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        kwargs["table"] = "roles"
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "roles"
        return self._get(cur, True, **kwargs)    
    
    @DbOps("courses")
    def update(self, cur):
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "roles", "name", self.name)
            if not ret: return ret
        return self._update(cur, "roles")
    
    
"""def check_role():
    rl = Role(id=0, name="test role 1")
    print "adding role:"
    print rl.add()
    print "id = ", rl.id
    print "getting roles:"
    print rl.get(order_by="name", asc_desc="desc", limit=10)
    print rl.get_one(id=2)
    print "getting one rol:"
    rl2 = Role(id=2)
    print rl2.id, rl2.name
    print "updating role:"
    rl.name = "".join([rl.name, "22"])
    print rl.update()

check_role()"""

class Status(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.status = kwargs.get("status")
        
    @DbOps("courses")
    def add(self, cur):
        ret = self._check_duplicate(cur, "status", "status", self.status)
        if not ret: return ret
        return self._add(cur, "status")
        
    @DbOps("courses")
    def get(self, cur, **kwargs):
        kwargs["table"] = "status"
        return self._get(cur, **kwargs)

    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "status"
        return self._get(cur, True, **kwargs)
    
    @DbOps("courses")
    def update(self, cur):
        if self.status is not None:
            ret = self._check_duplicate_on_update(cur, "status", "status", self.status)
            if not ret: return ret
        return self._update(cur, "status")

"""def check_status():
    st = Status(id=0, status="test_status 1")
    print "adding status"
    print st.add()
    print "getting many"
    print st.get(order_by="status", asc_desc="desc")
    print st.get_one(id=3)
    print "updating status"
    st.status = "".join([st.status, "66"])
    print st.update()

check_status()"""
 
 
class Category(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        self.deleted = kwargs.get("deleted")
        
    @DbOps("courses")
    def add(self, cur):
        ret = self._check_duplicate(cur, "category", "name", self.name)
        if not ret: return ret
        ret = self._check_foreign_id(cur, "library_db.category", self.id)
        if not ret: return ret
        return self._add(cur, "category", False)
        
    @DbOps("courses")
    def get(self, cur, **kwargs):
        kwargs["table"] = "category"
        return self._get(cur, **kwargs)

    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "category"
        return self._get(cur, True, **kwargs)
    
    @DbOps("courses")
    def update(self, cur):
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "category", "name", self.name)
            if not ret: return ret
        if self.id is not None:
            ret = self._check_foreign_id(cur, "library_db.category", self.id)
            if not ret: return ret
        return self._update(cur, "category")

"""def check_category():
    ct = Category(id=3, name="Scare category 1")
    print "adding category"
    print ct.add()
    print type(ct.id)
    print "getting many categories"
    print ct.get(order_by="name", asc_desc="desc")
    print "get one"
    print ct.get_one(id=3)
    print "updating category"
    ct.name = "".join([ct.name, "66"])
    print Category(id=ct.id, name="Scare category 166").update()

check_category()"""


class Comment(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.user_id = kwargs.get("user_id")
        self.lecture_id = kwargs.get("lecture_id")
        self.creation_date = kwargs.get("creation_date")
        self.title = kwargs.get("title")
        self.body = kwargs.get("body")
        self.active = kwargs.get("active")
        
    
    @DbOps("courses")
    def add(self, cur):
        ret = self._check_foreign_id(cur, "users", self.user_id)
        if not ret: return ret
        ret = self._check_foreign_id(cur, "lectures", self.lecture_id)
        if not ret: return ret
        return self._add(cur, "comments")
    
    @DbOps("courses")
    def update(self, cur):
        if self.user_id is not None:
            ret = self._check_foreign_id(cur, "users", self.user_id)
            if not ret: return ret
        if self.lecture_id is not None:
            ret = self._check_foreign_id(cur, "lectures", self.lecture_id)
            if not ret: return ret
        return self._update(cur, "comments")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        kwargs["table"] = "comments"
        return self._get(cur, **kwargs)

    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "comments"
        return self._get(cur, True, **kwargs)

"""def check_comment():
    cm = Comment(id=200, user_id=4, lecture_id=2, creation_date=datetime(2001, 9, 11, 9, 8, 1), title="comment title 1", body="body", active=1)
    print "adding coment"
    print cm.add()
    print "getting many"
    print cm.get(order_by="title", asc_desc="desc", creation_date=datetime(2012, 6, 1))
    print "get one"
    print cm.get_one(id=2)
    print "updating"
    cm.title = "".join([cm.title, "66"])
    cm.active = 0
    cm.creation_date = datetime(2020, 11, 8, 0, 1, 1)
    print cm.update()

check_comment()"""


class Score(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.course_id = kwargs.get("course_id")
        self.user_id = kwargs.get("user_id")
        self.progress_id = kwargs.get("progress_id")
        
    @DbOps("courses")
    def add(self, cur):
        ret = self._check_foreign_id(cur, "courses", self.course_id)
        if not ret: return ret
        ret = self._check_foreign_id(cur, "users", self.user_id)
        if not ret: return ret
        ret = self._check_foreign_id(cur, "progress", self.progress_id)
        if not ret: return ret
        ret = cur.execute("select * from score where course_id=%s and user_id=%s and progress_id=%s\
                          " % (self.course_id, self.user_id, self.progress_id))
        if ret > 0: return False
        return self._add(cur, "score")
    
    @DbOps("courses")
    def update(self, cur):
        if self.course_id is not None:
            ret = self._check_foreign_id(cur, "courses", self.course_id)
            if not ret: return ret
        if self.user_id is not None:
            ret = self._check_foreign_id(cur, "users", self.user_id)
            if not ret: return ret
        if self.progress_id is not None:
            ret = self._check_foreign_id(cur, "progress", self.progress_id)
            if not ret: return ret
        if self.course_id is not None and self.user_id is not None and self.progress_id is not None:
            ret = cur.execute("select * from score where course_id=%s and user_id=%s and progress_id=%s \
                              and id not in (%s)" % (self.course_id, self.user_id, self.id, self.progress_id))
            if ret > 0: return False
        return self._update(cur, "score")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        kwargs["table"] = "score"
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "score"
        return self._get(cur, True, **kwargs)    
    
"""def check_score():
    sc = Score(id=0, course_id=2, user_id=10, progress_id=3)
    print "adding score"
    print sc.add()
    print "get many"
    print sc.get()
    print "get one"
    print sc.get_one(id=2)
    print "updating"
    sc.course_id = 1
    print sc.update()

check_score()"""
            

class Progress(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.lecture_id = kwargs.get("lecture_id")
        self.passtest = kwargs.get("passtest")
        self.attempts = kwargs.get("attempts")
        self.finalexam = kwargs.get("finalexam")
        
    @DbOps("courses")
    def add(self, cur):
        ret = self._check_foreign_id(cur, "lectures", self.lecture_id)
        if not ret: return ret
        return self._add(cur, "progress")
    
    @DbOps("courses")
    def update(self, cur):
        if self.lecture_id is not None:
            ret = self._check_foreign_id(cur, "lectures", self.lecture_id)
            if not ret: return ret
        return self._update(cur, "progress")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        kwargs["table"] = "progress"
        return self._get(cur, **kwargs)

    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "progress"
        return self._get(cur, True, **kwargs)
    
"""def check_progress():
    pg = Progress(id=0, lecture_id=2, passtest=10, attempts=2, finalexam=1)
    print "adding progress"
    print pg.add()
    print "get many"
    print pg.get(finalexam=0)
    print pg.get_one(id=4)
    print "updating"
    pg.passtest = 200
    print pg.update()
    
check_progress()"""

