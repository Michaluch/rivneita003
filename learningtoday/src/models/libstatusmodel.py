"""
    This module consist MySQL model for worker status
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class LibStatus(DbObject):
    """
        This is MySQL model for worker status
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - worker status id (int)
                status - worker status (str)
        """
        self.id = kwargs.get("id")
        self.status = kwargs.get("status")
        
    @DbOps("library")
    def add(self, cur):
        """
            This method add new worker status
            id not required
            Constraints:
                status - unique
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_duplicate(cur, "status", "status", self.status)
        if not ret:
            return ret
        return self._add(cur, "status")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        """
            This method get list of workers statuses
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "status"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        """
            This method get one worker status
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "status"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        """
            This method update worker status
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.status is not None:
            ret = self._check_duplicate_on_update(cur, "status", "status", self.status)
            if not ret: 
                return ret
        return self._update(cur, "status")
    
"""def check_status():
    st = Status(id=5, status="Test status 1")
    print "adding status"
    print st.add()
    print "get many"
    print st.get(ordered_by="name", asc_desc="desc")
    print st.get_one(id=2)
    print "update"
    st.status = "".join([st.status, "22"])
    print st.update()

check_status()"""