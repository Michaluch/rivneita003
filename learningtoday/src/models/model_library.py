from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class Category(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        self.deleted = kwargs.get("deleted")
        
    @DbOps("library")
    def add(self, cur):
        ret = self._check_duplicate(cur, "category", "name", self.name)
        if not ret:
            return ret
        return self._add(cur, "category")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        kwargs["table"] = "category"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "category"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "category", "name", self.name)
            if not ret: return ret
        return self._update(cur, "category")
    
"""def check_category():
    ct = Category(id=5, name="Test category 2")
    print "adding category"
    print ct.add()
    print "get many"
    print ct.get(ordered_by="name", asc_desc="desc")
    print ct.get_one(id=2)
    print "update"
    ct.name = "".join([ct.name, "22"])
    print ct.update()
    
check_category()"""

class BookAuthor(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        
    @DbOps("library")
    def add(self, cur):
        ret = self._check_duplicate(cur, "books_authors", "name", self.name)
        if not ret:
            return ret
        return self._add(cur, "books_authors")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        kwargs["table"] = "books_authors"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "books_authors"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "books_authors", "name", self.name)
            if not ret: return ret
        return self._update(cur, "books_authors")
    
"""def check_author():
    ba = BookAuthor(id=5, name="Test Author 1")
    print "adding author"
    print ba.add()
    print "get many"
    print ba.get(ordered_by="name", asc_desc="desc")
    print ba.get_one(id=2)
    print "update"
    ba.name = "".join([ba.name, "22"])
    print ba.update()

check_author()"""


class Role(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        
    @DbOps("library")
    def add(self, cur):
        ret = self._check_duplicate(cur, "roles", "name", self.name)
        if not ret:
            return ret
        return self._add(cur, "roles")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        kwargs["table"] = "roles"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "roles"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "roles", "name", self.name)
            if not ret: return ret
        return self._update(cur, "roles")

"""def check_role():
    rl = Role(id=5, name="Test role 1")
    print "adding role"
    print rl.add()
    print "get many"
    print rl.get(ordered_by="name", asc_desc="desc")
    print rl.get_one(id=2)
    print "update"
    rl.name = "".join([rl.name, "22"])
    print rl.update()

check_role()"""


class Status(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.status = kwargs.get("status")
        
    @DbOps("library")
    def add(self, cur):
        ret = self._check_duplicate(cur, "status", "status", self.status)
        if not ret:
            return ret
        return self._add(cur, "status")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        kwargs["table"] = "status"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "status"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        if self.status is not None:
            ret = self._check_duplicate_on_update(cur, "status", "status", self.status)
            if not ret: return ret
        return self._update(cur, "status")
    
"""def check_status():
    st = Status(id=5, status="Test status 1")
    print "adding status"
    print st.add()
    print "get many"
    print st.get(ordered_by="name", asc_desc="desc")
    print st.get_one(id=2)
    print "update"
    st.status = "".join([st.status, "22"])
    print st.update()

check_status()"""


class Book(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.category_id = kwargs.get("category_id")
        self.author_id = kwargs.get("author_id")
        self.title = kwargs.get("title")
        self.amount = kwargs.get("amount")
        self.deleted = kwargs.get("deleted")
        
    @DbOps("library")
    def add(self, cur):
        ret = self._check_foreign_id(cur, "category", self.category_id)
        if not ret: return ret
        ret = self._check_foreign_id(cur, "books_authors", self.author_id)
        if not ret: return ret
        return self._add(cur, "library_books")
        
    @DbOps("library")
    def get(self, cur, **kwargs):
        kwargs["table"] = "library_books"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "library_books"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        if self.category_id is not None:
            ret = self._check_foreign_id(cur, "category", self.category_id)
            if not ret: return ret
        if self.author_id is not None:
            ret = self._check_foreign_id(cur, "books_authors", self.author_id)
            if not ret: return ret
        return self._update(cur, "library_books")

"""def check_book():
    bk = Book(id=0, category_id=2, author_id=3, title="Test title 1", amount=1, deleted=0)
    print "adding book"
    print bk.add()
    print "get many"
    print bk.get(ordered_by="name", asc_desc="desc")
    print bk.get_one(id=2)
    print "update"
    bk.title = "".join([bk.title, "22"])
    print bk.update()

check_book()"""

class Worker(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.role_id = kwargs.get("role_id")
        self.f_name = kwargs.get("f_name")
        self.l_name = kwargs.get("l_name")
        self.login = kwargs.get("login")
        self.password = kwargs.get("password")
        self.status_id = kwargs.get("status_id")
        
    @DbOps("library")
    def add(self, cur):
        ret = self._check_foreign_id(cur, "roles", self.role_id)
        if not ret: return ret
        ret = self._check_foreign_id(cur, "status", self.status_id)
        if not ret: return ret
        ret = self._check_duplicate(cur, "workers", "login", self.login)
        if not ret: return ret
        return self._add(cur, "workers")
        
    @DbOps("library")
    def get(self, cur, **kwargs):
        kwargs["table"] = "workers"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "workers"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        if self.role_id is not None:
            ret = self._check_foreign_id(cur, "roles", self.role_id)
            if not ret: return ret
        if self.status_id is not None:
            ret = self._check_foreign_id(cur, "status", self.status_id)
            if not ret: return ret
        if self.login is not None:
            ret = self._check_duplicate_on_update(cur, "workers", "login", self.login)
            if not ret: return ret
        return self._update(cur, "workers")


"""def check_worker():
    w = Worker(id=0, role_id=2, f_name="name 1", l_name="lname 2", login="log 1", password="ps1", status_id=1)
    print "adding new"
    print w.add()
    print "get many"
    print w.get(role_id=2)
    print "get one"
    print w.get_one(login="login2")
    w.f_name = "".join([w.f_name, "77"])
    print "updating"
    print w.update()

check_worker()"""
    
class Reader(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.f_name = kwargs.get("f_name")
        self.l_name = kwargs.get("l_name")
        self.reg_begin = kwargs.get("reg_begin")
        self.reg_end = kwargs.get("reg_end")
        self.status_id = kwargs.get("status_id")
        
    @DbOps("library")
    def add(self, cur):
        ret = self._check_foreign_id(cur, "status", self.status_id)
        if not ret: return ret
        return self._add(cur, "readers")
        
    @DbOps("library")
    def get(self, cur, **kwargs):
        kwargs["table"] = "readers"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "readers"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        if self.status_id is not None:
            ret = self._check_foreign_id(cur, "status", self.status_id)
            if not ret: return ret
        return self._update(cur, "readers")
    
"""def check_reader():
    w = Reader(id=0, f_name="name 1", l_name="lname 2", reg_begin=datetime(2011, 10, 1), reg_end=datetime(2011, 12, 1), status_id=1)
    print "adding new"
    print w.add()
    print "get many"
    print w.get()
    print "get one"
    print w.get_one(login="login2")
    w.f_name = "".join([w.f_name, "77"])
    print "updating"
    print w.update()

check_reader()"""
    
class Monitor(DbObject):
    
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.book_id = kwargs.get("book_id")
        self.worker_id = kwargs.get("worker_id")
        self.reader_id = kwargs.get("reader_id")
        self.issue_date = kwargs.get("issue_date")
        self.return_date = kwargs.get("return_date")
        
    @DbOps("library")
    def add(self, cur):
        ret = self._check_foreign_id(cur, "library_books", self.book_id)
        if not ret: return ret
        ret = self._check_foreign_id(cur, "workers", self.worker_id)
        if not ret: return ret
        ret = self._check_foreign_id(cur, "readers", self.reader_id)
        if not ret: return ret
        return self._add(cur, "books_monitor")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        kwargs["table"] = "books_monitor"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        kwargs["table"] = "books_monitor"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        if self.book_id is not None:
            ret = self._check_foreign_id(cur, "library_books", self.book_id)
            if not ret: return ret
        if self.worker_id is not None:
            ret = self._check_foreign_id(cur, "workers", self.worker_id)
            if not ret: return ret
        if self.reader_id is not None:
            ret = self._check_foreign_id(cur, "readers", self.reader_id)
            if not ret: return ret
        return self._update(cur, "books_monitor")
    
"""def check_monitor():
    w = Monitor(id=0, book_id=1, worker_id=2, reader_id=4, issue_date=datetime(2010, 8, 10), return_date=datetime(2011, 8, 10))
    print "adding new"
    print w.add()
    print "get many"
    print w.get()
    print "get one"
    print w.get_one(id=2)
    w.book_id = 3
    print "updating"
    print w.update()

check_monitor()"""

        