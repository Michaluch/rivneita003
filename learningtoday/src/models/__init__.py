from learningtoday.src.models.usermodel import User
from learningtoday.src.models.coursemodel import Course
from learningtoday.src.models.lecturemodel import Lecture
from learningtoday.src.models.currolemodel import CurRole
from learningtoday.src.models.curcategorymodel import CurCategory
from learningtoday.src.models.curstatusmodel import CurStatus
from learningtoday.src.models.scoremodel import Score
from learningtoday.src.models.commentmodel import Comment
from learningtoday.src.models.progressmodel import Progress


from learningtoday.src.models.libstatusmodel import LibStatus
from learningtoday.src.models.librolemodel import LibRole
from learningtoday.src.models.libcategorymodel import LibCategory
from learningtoday.src.models.bookauthormodel import BookAuthor
from learningtoday.src.models.bookmodel import Book
from learningtoday.src.models.monitormodel import Monitor
from learningtoday.src.models.readermodel import Reader
from learningtoday.src.models.workermodel import Worker

from testsmodel import Tests
from learningtoday.src.models.recommendationsmodel import Recommendations
