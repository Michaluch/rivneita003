"""
    This module consist MySQL model for lecture
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class Lecture(DbObject):
    """
        This is MySQL model for lecture
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - lecture id (int)
                course_id - lecture course id (int)
                test_id - lecture test id (int)
                title - lecture title (str)
                body - lecture body (str)
                raite - lecture raite (int)
                deleted - ledture deletion status (int)
        """
        self.id = kwargs.get("id")
        self.course_id = kwargs.get("course_id")
        self.test_id = kwargs.get("test_id")
        self.title = kwargs.get("title")
        self.body = kwargs.get("body")
        self.raite = kwargs.get("raite")
        self.number_in_course = kwargs.get("number_in_course")
        self.deleted = kwargs.get("deleted")
    
    @DbOps("courses")
    def add(self, cur):
        """
            This method add new lecture
            id not required
            Constraints:
                course_id
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_foreign_id(cur, "courses", self.course_id)
        if not ret: 
            return ret
        return self._add(cur, "lectures")
        
    @DbOps("courses")
    def update(self, cur):
        """
            This method update lecture
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.course_id is not None:
            ret = self._check_foreign_id(cur, "courses", self.course_id)
            if not ret: 
                return ret
        return self._update(cur, "lectures")
        
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """
            This method get list of lectures
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
                where - have_test=True/False, get lectures what have (or not) lectures (bool)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        where = ""
        if kwargs.get("have_test") is not None:
            if kwargs["have_test"]:
                where = "where test_id is not NULL"
            else:
                where = "where test_id is NULL"
        kwargs["table"] = "lectures"
        kwargs["where"] = where
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        """
            This method get one lecture
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "lectures"
        return self._get(cur, True, **kwargs)
    
"""def check_lecture():
    l = Lecture(id=7, course_id=1, test_id=0, title="test lecture 1", body="lecture body", raite=1, deleted=0)
    print "adding lecture:"
    print l.add()
    print "getting many:"
    print l.get(order_by="title", asc_desc="desc", limit=100)
    print "get one"
    print l.get_one(id=3)
    print "updating:"
    l.title = "".join([l.title, "11"])
    l.body = "".join([l.body, "11"])
    l.deleted = 1
    print l.update()

check_lecture()"""
