"""
    This module consist MySQL model for user score
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class Score(DbObject):
    """
        This is MySQL model for user score
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - user score id (int)
                course_id - id of course, user subscribed on (int)
                user_id - id scores user (int)
        """
        self.id = kwargs.get("id")
        self.course_id = kwargs.get("course_id")
        self.user_id = kwargs.get("user_id")
        self.finished = kwargs.get("finished")
        self.finish_date = kwargs.get("finish_date")
        
    @DbOps("courses")
    def add(self, cur):
        """
            This method add new user score
            id not required
            Constraints:
                course_id
                user_id
                combination of course_id, user_id must be unique 
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_foreign_id(cur, "courses", self.course_id)
        if not ret: 
            return ret
        ret = self._check_foreign_id(cur, "users", self.user_id)
        if not ret: 
            return ret
        return self._add(cur, "score")
    
    @DbOps("courses")
    def update(self, cur):
        """
            This method update user score
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.course_id is not None:
            ret = self._check_foreign_id(cur, "courses", self.course_id)
            if not ret: 
                return ret
        if self.user_id is not None:
            ret = self._check_foreign_id(cur, "users", self.user_id)
            if not ret: 
                return ret
        return self._update(cur, "score")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """
            This method get list of users scores
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "score"
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        """
            This method get one user score
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "score"
        return self._get(cur, True, **kwargs)    
    
"""def check_score():
    sc = Score(id=0, course_id=2, user_id=10, progress_id=3)
    print "adding score"
    print sc.add()
    print "get many"
    print sc.get()
    print "get one"
    print sc.get_one(id=2)
    print "updating"
    sc.course_id = 1
    print sc.update()

check_score()"""