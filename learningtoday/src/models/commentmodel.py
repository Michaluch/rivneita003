"""
    This module consist MySQL model for comment
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class Comment(DbObject):
    """
        This is MySQL model for books comment
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - comment id (int)
                user_id - id of user who created comment (int)
                lecture_id - id of lecture of comment (int)
                creation_date - comment creation date (datetime)
                title - comment title (str)
                body - comment body (str)
                active - comment activeness status (int)
        """
        self.id = kwargs.get("id")
        self.user_id = kwargs.get("user_id")
        self.lecture_id = kwargs.get("lecture_id")
        self.creation_date = kwargs.get("creation_date")
        self.title = kwargs.get("title")
        self.body = kwargs.get("body")
        self.active = kwargs.get("active")
        
    
    @DbOps("courses")
    def add(self, cur):
        """
            This method add new comment
            id not required
            Constraints:
                user_id
                lecture_id
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_foreign_id(cur, "users", self.user_id)
        if not ret: 
            return ret
        ret = self._check_foreign_id(cur, "lectures", self.lecture_id)
        if not ret: 
            return ret
        return self._add(cur, "comments")
    
    @DbOps("courses")
    def update(self, cur):
        """
            This method update comment
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.user_id is not None:
            ret = self._check_foreign_id(cur, "users", self.user_id)
            if not ret: 
                return ret
        if self.lecture_id is not None:
            ret = self._check_foreign_id(cur, "lectures", self.lecture_id)
            if not ret: 
                return ret
        return self._update(cur, "comments")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """
            This method get list of comments
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "comments"
        return self._get(cur, **kwargs)

    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        """
            This method get one comment
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "comments"
        return self._get(cur, True, **kwargs)

"""def check_comment():
    cm = Comment(id=200, user_id=4, lecture_id=2, creation_date=datetime(2001, 9, 11, 9, 8, 1), title="comment title 1", body="body", active=1)
    print "adding coment"
    print cm.add()
    print "getting many"
    print cm.get(order_by="title", asc_desc="desc", creation_date=datetime(2012, 6, 1))
    print "get one"
    print cm.get_one(id=2)
    print "updating"
    cm.title = "".join([cm.title, "66"])
    cm.active = 0
    cm.creation_date = datetime(2020, 11, 8, 0, 1, 1)
    print cm.update()

check_comment()"""