"""
    This module consist MySQL model for book author
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class BookAuthor(DbObject):
    """
        This is MySQL model for book author
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - book author id (int)
                name - book author first and last name (str)
        """
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        
    @DbOps("library")
    def add(self, cur):
        """
            This method add new book author
            id not required
            Constraints:
                name - unique
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_duplicate(cur, "books_authors", "name", self.name)
        if not ret:
            return ret
        return self._add(cur, "books_authors")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        """
            This method get list of books authors
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "books_authors"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        """
            This method get one book author
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "books_authors"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        """
            This method update book author
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "books_authors", "name", self.name)
            if not ret: 
                return ret
        return self._update(cur, "books_authors")
    
"""def check_author():
    ba = BookAuthor(id=5, name="Test Author 1")
    print "adding author"
    print ba.add()
    print "get many"
    print ba.get(ordered_by="name", asc_desc="desc")
    print ba.get_one(id=2)
    print "update"
    ba.name = "".join([ba.name, "22"])
    print ba.update()

check_author()"""