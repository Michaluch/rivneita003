"""
    This module consist MySQL model for book monitor
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class Monitor(DbObject):
    """
        This is MySQL model for book monitor
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - book monitor id (int)
                book_id - id of monitoring book(int)
                worker_id - id of worker who give book (int)
                reader_id - id of reader who recieve book (int)
                issue_date - date when worker gave book to reader (datetime)
                return_date - date when book must be returned by reader (datetime)
        """
        self.id = kwargs.get("id")
        self.book_id = kwargs.get("book_id")
        self.worker_id = kwargs.get("worker_id")
        self.reader_id = kwargs.get("reader_id")
        self.issue_date = kwargs.get("issue_date")
        self.return_date = kwargs.get("return_date")
        
    @DbOps("library")
    def add(self, cur):
        """
            This method add new book monitor
            id not required
            Constraints:
                book_id
                worker_id
                reader_id
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_foreign_id(cur, "library_books", self.book_id)
        if not ret: 
            return ret
        ret = self._check_foreign_id(cur, "workers", self.worker_id)
        if not ret: 
            return ret
        ret = self._check_foreign_id(cur, "readers", self.reader_id)
        if not ret: 
            return ret
        return self._add(cur, "books_monitor")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        """
            This method get list of books bonitors
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "books_monitor"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        """
            This method get one book monitor
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "books_monitor"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        """
            This method update book monitor
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.book_id is not None:
            ret = self._check_foreign_id(cur, "library_books", self.book_id)
            if not ret: 
                return ret
        if self.worker_id is not None:
            ret = self._check_foreign_id(cur, "workers", self.worker_id)
            if not ret: 
                return ret
        if self.reader_id is not None:
            ret = self._check_foreign_id(cur, "readers", self.reader_id)
            if not ret: 
                return ret
        return self._update(cur, "books_monitor")
    
"""def check_monitor():
    w = Monitor(id=0, book_id=1, worker_id=2, reader_id=4, issue_date=datetime(2010, 8, 10), return_date=datetime(2011, 8, 10))
    print "adding new"
    print w.add()
    print "get many"
    print w.get()
    print "get one"
    print w.get_one(id=2)
    w.book_id = 3
    print "updating"
    print w.update()

check_monitor()"""