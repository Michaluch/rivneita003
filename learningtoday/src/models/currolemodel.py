"""
    This module consist MySQL model for user role
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class CurRole(DbObject):
    """
        This is MySQL model for user role
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - user role id (int)
                name - user role name (str)
        """
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        
    @DbOps("courses")
    def add(self, cur):
        """
            This method add new user role
            id not required
            Constraints:
                name - unique
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_duplicate(cur, "roles", "name", self.name)
        if not ret: 
            return ret
        return self._add(cur, "roles")
    
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """
            This method get list of user roles
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "roles"
        return self._get(cur, **kwargs)
    
    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        """
            This method get one user role
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "roles"
        return self._get(cur, True, **kwargs)    
    
    @DbOps("courses")
    def update(self, cur):
        """
            This method update user role
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "roles", "name", self.name)
            if not ret: 
                return ret
        return self._update(cur, "roles")
    
    
"""def check_role():
    rl = CurRole(id=0, name="test role 1")
    print "adding role:"
    print rl.add()
    print "id = ", rl.id
    print "getting roles:"
    print rl.get(order_by="name", asc_desc="desc", limit=10)
    print rl.get_one(id=2)
    print "getting one rol:"
    rl2 = Role(id=2)
    print rl2.id, rl2.name
    print "updating role:"
    rl.name = "".join([rl.name, "22"])
    print rl.update()

check_role()"""
