"""
    This module consist MySQL model for worker (librarian)
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class Worker(DbObject):
    """
        This is MySQL model for worker (librarian)
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - worker id (int)
                role_id - worker role id (int)
                f_name - worker first name (str)
                l_name - worker last name (str)
                loign - worker login (str)
                password - worker password (str)
                status_id - worker status id (int)
        """
        self.id = kwargs.get("id")
        self.role_id = kwargs.get("role_id")
        self.f_name = kwargs.get("f_name")
        self.l_name = kwargs.get("l_name")
        self.login = kwargs.get("login")
        self.password = kwargs.get("password")
        self.status_id = kwargs.get("status_id")
        
    @DbOps("library")
    def add(self, cur):
        """
            This method add new worker
            id not required
            Constraints:
                login - unique
                role_id
                status_id
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_foreign_id(cur, "roles", self.role_id)
        if not ret: 
            return ret
        ret = self._check_foreign_id(cur, "status", self.status_id)
        if not ret: 
            return ret
        ret = self._check_duplicate(cur, "workers", "login", self.login)
        if not ret: 
            return ret
        return self._add(cur, "workers")
        
    @DbOps("library")
    def get(self, cur, **kwargs):
        """
            This method get list of workers
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "workers"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        """
            This method get one worker
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "workers"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        """
            This method update worker
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.role_id is not None:
            ret = self._check_foreign_id(cur, "roles", self.role_id)
            if not ret: 
                return ret
        if self.status_id is not None:
            ret = self._check_foreign_id(cur, "status", self.status_id)
            if not ret: 
                return ret
        if self.login is not None:
            ret = self._check_duplicate_on_update(cur, "workers", "login", self.login)
            if not ret: 
                return ret
        return self._update(cur, "workers")


"""def check_worker():
    w = Worker(id=0, role_id=2, f_name="name 1", l_name="lname 2", login="log 1", password="ps1", status_id=1)
    print "adding new"
    print w.add()
    print "get many"
    print w.get(role_id=2)
    print "get one"
    print w.get_one(login="login2")
    w.f_name = "".join([w.f_name, "77"])
    print "updating"
    print w.update()

check_worker()"""