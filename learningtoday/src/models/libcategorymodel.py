"""
    This module consist MySQL model for library category
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class LibCategory(DbObject):
    """
        This is MySQL model for library category
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - library category id (int)
                name - library category name (str)
                deleted - library category deleiton status (int)
        """
        self.id = kwargs.get("id")
        self.name = kwargs.get("name")
        self.deleted = kwargs.get("deleted")
        
    @DbOps("library")
    def add(self, cur):
        """
            This method add new library category
            id not required
            Constraints:
                name - unique
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_duplicate(cur, "category", "name", self.name)
        if not ret:
            return ret
        return self._add(cur, "category")
    
    @DbOps("library")
    def get(self, cur, **kwargs):
        """
            This method get list of libraries categories
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "category"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        """
            This method get one library category
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "category"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        """
            This method update library category
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.name is not None:
            ret = self._check_duplicate_on_update(cur, "category", "name", self.name)
            if not ret: 
                return ret
        return self._update(cur, "category")
    
"""def check_category():
    ct = LibCategory(id=5, name="Test category 2")
    print "adding category"
    print ct.add()
    print "get many"
    print ct.get(ordered_by="name", asc_desc="desc")
    print ct.get_one(id=2)
    print "update"
    ct.name = "".join([ct.name, "22"])
    print ct.update()
    
check_category()"""