"""
    This module consist MySQL model for book
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class Book(DbObject):
    """
        This is MySQL model for book
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - book author id (int)
                category_id - book category id (int)
                author_id - book author id (int)
                title - book title (str)
                amount - amount of added books (int)
                deleted - book deletion status (int)
        """
        self.id = kwargs.get("id")
        self.category_id = kwargs.get("category_id")
        self.author_id = kwargs.get("author_id")
        self.title = kwargs.get("title")
        self.amount = kwargs.get("amount")
        self.deleted = kwargs.get("deleted")
        
    @DbOps("library")
    def add(self, cur):
        """
            This method add new book
            id not required
            Constraints:
                category_id
                author_id
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_foreign_id(cur, "category", self.category_id)
        if not ret: 
            return ret
        ret = self._check_foreign_id(cur, "books_authors", self.author_id)
        if not ret: 
            return ret
        return self._add(cur, "library_books")
        
    @DbOps("library")
    def get(self, cur, **kwargs):
        """
            This method get list of books
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "library_books"
        return self._get(cur, **kwargs)
    
    @DbOps("library")
    def get_one(self, cur, **kwargs):
        """
            This method get one book
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "library_books"
        return self._get(cur, True, **kwargs)
    
    @DbOps("library")
    def update(self, cur):
        """
            This method update book
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.category_id is not None:
            ret = self._check_foreign_id(cur, "category", self.category_id)
            if not ret: 
                return ret
        if self.author_id is not None:
            ret = self._check_foreign_id(cur, "books_authors", self.author_id)
            if not ret: 
                return ret
        return self._update(cur, "library_books")

"""def check_book():
    bk = Book(id=0, category_id=2, author_id=3, title="Test title 1", amount=1, deleted=0)
    print "adding book"
    print bk.add()
    print "get many"
    print bk.get(ordered_by="name", asc_desc="desc")
    print bk.get_one(id=2)
    print "update"
    bk.title = "".join([bk.title, "22"])
    print bk.update()

check_book()"""