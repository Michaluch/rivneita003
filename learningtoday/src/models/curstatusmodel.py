"""
    This module consist MySQL model for user status
"""
from learningtoday.src.models.mysqlapi import (
    DbOps,
    DbObject
)

class CurStatus(DbObject):
    """
        This is MySQL model for user status
    """
    
    def __init__(self, **kwargs):
        """
            Args:
                id - user status id (int)
                status - user status (str)
        """
        self.id = kwargs.get("id")
        self.status = kwargs.get("status")
        
    @DbOps("courses")
    def add(self, cur):
        """
            This method add new user status
            id not required
            Constraints:
                status - unique
            Returns:
                True - if adding successful
                False - if adding not successful
        """
        ret = self._check_duplicate(cur, "status", "status", self.status)
        if not ret: 
            return ret
        return self._add(cur, "status")
        
    @DbOps("courses")
    def get(self, cur, **kwargs):
        """
            This method get list of users ststuses
            Args:
                arguments with attribute names
                order_by - order by field name (str)
                asc_desc - asc/desc (str)
                limit - if no limit_to, limit acts as 'LIMIT limit' else as 'LIMIT limit, limit_to' (int)
                limit_to - limit to (int)
            Returns:
                List of dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "status"
        return self._get(cur, **kwargs)

    @DbOps("courses")
    def get_one(self, cur, **kwargs):
        """
            This method get one user status
            Args:
                arguments with attribute names
            Returns:
                Dict - if getting successful
                False - if getting not successful
        """
        kwargs["table"] = "status"
        return self._get(cur, True, **kwargs)
    
    @DbOps("courses")
    def update(self, cur):
        """
            This method update user status
            Requirements:
                id
            Returns:
                True - if updating successful
                False - if updatting not successful
        """
        if self.status is not None:
            ret = self._check_duplicate_on_update(cur, "status", "status", self.status)
            if not ret: 
                return ret
        return self._update(cur, "status")

"""def check_status():
    st = CurStatus(id=0, status="test_status 1")
    print "adding status"
    print st.add()
    print "getting many"
    print st.get(order_by="status", asc_desc="desc")
    print st.get_one(id=3)
    print "updating status"
    st.status = "".join([st.status, "66"])
    print st.update()

check_status()"""
 