from pyramid.view import view_config

from .src.models.model_courses import (
    Course,
)


@view_config(route_name='course',
             renderer='course.jinja2')
def course_test(request):
    return {'courses': Course().get()}


