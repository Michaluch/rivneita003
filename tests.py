import unittest
from mock import Mock
from pyramid import testing


class MyTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_view_signup_forbidden(self):
        from learningtoday.src.views.signupview import SignUp
        self.config.testing_securitypolicy(permissive=False)
        request = testing.DummyRequest()
        request.context = testing.DummyResource()
        response = SignUp.signup_get(SignUp(request))
        self.assertEqual(response, {'message':''})

class MyHomeViewTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_view_search(self):
        from learningtoday.src.views.baseview import Base
        from learningtoday.src.models.coursemodel import Course
        Course.get = Mock(return_value=list([{'description': 'Nullam id dolor id nibh ultricies vehicula ut id elit.', 'deleted': 0, 'image': 'Nullam id dolor id nibh ultricies vehicula ut id elit.', 'author_id': 1, 'category_id': 1, 'id': 1, 'name': 'PyPro'}, {'description': 'Wow-wow', 'deleted': 0, 'image': None, 'author_id': 1, 'category_id': 1, 'id': 3, 'name': 'Py for Puppies'}]))
        request = testing.DummyRequest()
        request.context = testing.DummyResource()
        request.request_method = 'GET'
        request.matchdict['phrase'] = 'py'
        response = Base.search_view(Base(request))
        self.assertEqual(response, {'boxes': [{'description': 'Nullam id dolor id nibh ultricies vehicula ut id elit.', 'deleted': 0, 'image': 'Nullam id dolor id nibh ultricies vehicula ut id elit.', 'author_id': 1, 'category_id': 1, 'id': 1, 'name': 'PyPro'}, {'description': 'Wow-wow', 'deleted': 0, 'image': None, 'author_id': 1, 'category_id': 1, 'id': 3, 'name': 'Py for Puppies'}]})
        
    def test_view_search_not_found(self):
        from learningtoday.src.views.baseview import Base
        from learningtoday.src.models.coursemodel import Course
        Course.get = Mock(return_value=list(''))
        request = testing.DummyRequest()
        request.context = testing.DummyResource()
        request.matchdict['phrase'] = 'pyram'
        response = Base.search_view(Base(request))
        self.assertEqual(response, {'result': 'Search have no result'})
        
    def test_category_view(self):
        from learningtoday.src.views.baseview import Base
        from learningtoday.src.models.coursemodel import Course
        request = testing.DummyRequest()
        request.context = testing.DummyResource()
        Course.get = Mock(return_value=list([{'description': 'Nullam id dolor id nibh ultricies vehicula ut id elit.', 'deleted': 0, 'image': 'Nullam id dolor id nibh ultricies vehicula ut id elit.', 'author_id': 1, 'category_id': 1, 'id': 1, 'name': 'PyPro'}, {'description': 'Wow-wow', 'deleted': 0, 'image': None, 'author_id': 1, 'category_id': 1, 'id': 3, 'name': 'Py for Puppies'}]))
        request.matchdict['id'] = 1
        response = Base.category_view(Base(request))
        self.assertEqual(response, {'boxes': [{'description': 'Nullam id dolor id nibh ultricies vehicula ut id elit.', 'deleted': 0, 'image': 'Nullam id dolor id nibh ultricies vehicula ut id elit.', 'author_id': 1, 'category_id': 1, 'id': 1, 'name': 'PyPro'}, {'description': 'Wow-wow', 'deleted': 0, 'image': None, 'author_id': 1, 'category_id': 1, 'id': 3, 'name': 'Py for Puppies'}]})
    
   
    def test_category_view_not_found(self):
        from learningtoday.src.views.baseview import Base
        from learningtoday.src.models.coursemodel import Course
        Course.get = Mock(return_value=list(''))
        request = testing.DummyRequest()
        request.context = testing.DummyResource()
        request.matchdict['id'] = 5
        response = Base.category_view(Base(request))
        self.assertEqual(response, None)
    
   
